---
title: " 14 coisas sobre o livro que comecei a escrever"
date: 2013-05-31 19:56:19
---

por um ponto de partida:

1.  é o primeiro livro de uma trilogia (ou sabe-se lá quantos livros mais vou emendar nessa brincadeira) que gira de alguma forma em torno do futebol (quer dizer, se for pensar no TRÉGUA, esse vai ser o segundo de uma quadrilogia, então enfim);
2.  não tenho título (nunca tenho título);
3.  são dois amigos jogadores de futebol: um goleiro e um zagueiro, do mesmo time, ambos convocados para jogar na seleção brasileira na copa de 2014;
4.  o goleiro tem mais de dois metros de altura (a vida é meu tanque de areia);
5.  o goleiro queria enlouquecer, de uma vez;
6.  o goleiro se chama Tarciso;
7.  o zagueiro é um tipo meio cínico meio agressivo que toma o tempo todo uma mistura de uísque (acho) e codeína (acho) desde que precisou ficar afastado por causa de uma lesão no tornozelo, e meio metido a literato por causa de uma irmã professora de literatura;
8.  o zagueiro é o narrador;
9.  o zagueiro se chama Bernardo;
10.  junto deles está Emília, trainee de marketing na empresa que é a principal patrocinadora do time;
11.  Emília pensa em largar o emprego e o marketing e desaparecer;
12.  a história começa quando os três viajam para Fortaleza para filmar um comercial de uma promoção para a copa do mundo, greves nos aeroportos os obrigam a ficar mais um tempo e resolvem alugar um carro para conhecer as praias no entorno da capital;
13.  nunca enlouquecer pareceu tão tentador e nunca desaparecer pareceu tão fácil;
14.  claro que o Bernardo vai fazer alguma merda.

claro que eu ainda não descobri o que o Bernardo vai fazer de merda, mas enfim. já estou descobrindo que ele é bem capaz de fazer muita merda. acho que não preciso me preocupar. detalhes. pensando agora no TRÉGUA e no fato de que o último livro planejado para essa série de livros vai ter como título GUIA (olha, já tenho um título!), talvez eu deva procurar títulos de uma palavra só. não sei se é mais fácil ou mais difícil ou impossível.

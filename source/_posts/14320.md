---
title: " something else"
date: 2014-08-17 19:59:03
---

> There’s a sense of anger and grief and despair that causes Lewis to want to discard the entire war, set it aside in the favor of something better. You can feel him telling you —I know it’s awful, truly terrible, but that’s not all there is. There’s another option. Lucy, as she enters the wardrobe, takes the other option. I remember feeling this way as a child, too. I remember thinking, “Yes, of course there is. Of course this isn’t all there is. There must be something else.” How powerful it was to have Lewis come along and say, Yes, I feel that way, too.

em [Confronting Reality by Reading Fantasy](http://www.theatlantic.com/entertainment/archive/2014/08/going-home-with-cs-lewis/375560/), sobre Narnia, a guerra e um mundo melhor. ou: um resumo de por que eu leio e escrevo.

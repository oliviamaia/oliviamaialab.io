---
title: "aspiração capitalista"
date: "2019-07-22 16:46"
---

> There are many people who, having amassed fortunes, are seem under the grip of a kind of compulsion. They must turn ten million into a hundred, a hundred into a billion, and so on. This is what Marx called “fetishization.” Later thinkers, like Adorno and Fromm, would have said that such a person is still trying to escape capitalism — only they don’t know how, the poor things. They are trying to buy love, affection, belonging, meaning, and purpose, they are trying to win the very same self-discovery and self-realization our genteel bourgeois is after, devoting his life to literature or art once he has made his money, with a bigger yacht, mansion, and bank account. But you cannot really buy those things — in this world, or the next. So it’s true to say that mega-capitalists aren’t exactly monks — but I don’t think that means they’re not also trying to escape capitalism, too. They’re just trying to buy their way out.

daqui: _[If the Point of Capitalism is to Escape Capitalism, Then What’s the Point of Capitalism?](https://eand.co/if-the-point-of-capitalism-is-to-escape-capitalism-then-whats-the-point-of-capitalism-bedd1b2447d?gi=3e87e7bb7c89)_

um raciocínio interessante, apesar da conclusão genérica e _vamos todos dar as mãos e fazer arte_. ou: mais uma contradição pra conta do capitalismo? a velha história de trabalhar mais pra pagar alguém pra cuidar dos filhos porque você trabalha mais e não tem tempo pra cuidar dos filhos etc.

a que viemos?

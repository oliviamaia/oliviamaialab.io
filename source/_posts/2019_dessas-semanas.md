---
title: "dessas semanas"
date: "2019-07-10 15:23"
---

dessas semanas de dor de garganta, crise de enxaqueca e crise de ansiedade, irritação ou tpm, gatinha desaparecida, inverno, gatos mal-humorados porque abandonaram três filhotes no terreno vizinho e eles moram todos agora na sua casa. dessas semanas que não tem terapia porque um feriado estadual discutivelmente incompreensível, e na semana seguinte o terapeuta tenta dizer algo que era talvez para que você se sentisse melhor e tudo bem não querer fazer nada e deitar na cama e esperar passar mas na verdade só serve pra irritar mais ainda; o mundo, esse mundo, as notícias que você nem leu porque eliminou do twitter todas as fontes possíveis de notícia.

acreditar que sim, passa, vai passar. sempre passa. como tudo sempre passa, e a vida passa, e passa junto o sistema solar e todo o universo, e bem provável que nem faça diferença. certamente não deveria fazer nenhuma diferença.

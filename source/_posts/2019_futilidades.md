---
title: "futilidades"
date: "2019-06-19 15:09"
---

se a futilidade "feminina" é a preocupação com a aparência;

a futilidade "masculina" é o poder.

a mulher que busca o poder seria tão fútil quanto o homem que passa o dia em frente ao espelho.

mas isso ninguém diz.

diz-se: a mulher que busca o poder é forte.

enquanto o homem que se preocupa com a aparência é corajoso por não se apegar talvez aos velhos estereótipos de masculinidade.

a futilidade afinal é só questão de hierarquia de gênero.

---
layout: "post"
title: "liberdade de expressão na internet dos novos anos 20"
date: "2019-12-14 09:39"
---

Peter Pomerantsev em [evento da London School of Economics](http://www.lse.ac.uk/Events/2019/11/20191105t1830vOT/this-is-not-propaganda), "This Is Not Propaganda", disponível para [download como podcast](http://www.lse.ac.uk/lse-player?category=public+lectures+and+events) sobre a liberdade e facilidade de expressão nesses tempos de internet "when it's all turned upside-down":

porque não é mais necessário censurar: em vez disso a direita pode atacar com hordas de _fake news_, desinformação, trolls e bots diversos, e logo hastear a bandeira da liberdade de expressão porque afinal não era isso que a esquerda queria?

mais: a livre expressão também se transforma em _big data_. quanto mais você se expressa livremente na internet, mais você é vigiado, mais o seu comportamento é explorado por empresas que podem direcionar de forma cada vez mais precisa a propaganda de produtos ou ideias, alterar comportamentos e influenciar pontos de vista.

"freedom os expression gets kind of hacked."

quando mais você se expressa, mais vulnerável você fica, menos poder você tem.

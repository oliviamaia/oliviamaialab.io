---
title: "resmungo breve sobre história e violência"
date: "2019-07-21 15:40"
---

fiz aqui uma pesquisa rápida e algumas contas apressadas e queria informar aos desinformados que, por incrível que pareça, **Hitler matou mais do que o _comunismo_**, e a segunda guerra mundial matou mais ainda, muito mais, se a gente contar as mortes indiretas por fome ou frio ou doença -- essas que não aparecem nas contagens dentro da minha pesquisa relâmpago sobre a segunda guerra mas que sim, aparecem na conta de Stalin e Mao por todos os lados.

(nem vamos falar de Cuba porque Cuba matou menos do que certamente morrem nas favelas brasileiras a cada ano.)

não era por dizer que Stalin e Mao são santos, longe disso. mais por medir esse descompasso de pesos e medidas que se aplica quando alguém aponta o dedo e grita _comunista_. e nem quero discutir sobre o que significa de fato _comunismo_ ou se ele existiu etc. eu diria que existiram talvez intenções em direção ao comunismo mas que ele nunca aconteceu, nunca ninguém viu. ao contrário do que se poderia dizer de um _capitalismo ideal_ em que --

quê?

se todos são capitalistas, de onde a gente tira o lucro? o sistema de pirâmide afinal funciona porque a maioria continua embaixo, sustentando o topo da pirâmide e não, você não vai chegar no topo ou, pior, se chegar, vai pisar em muita gente e essa gente não vai chegar nunca. porque se chegarem, não tem pirâmide. não tem capitalismo. e, convenhamos, não tem planeta.

fiz essas contas todas nem tanto pra defender um comunismo utópico que nunca aconteceu (embora quem me conhece saiba que é exatamente isso que eu costumo fazer quando me oferecem um caixotinho). mas porque continuei com as contagens e cheguei num número de quase 10 milhões de pessoas mortas pelo _capitalismo_ nos últimos quarenta anos, principalmente na África e no Oriente Médio. ou: só contando as vítimas das grandes crises de fome ocasionadas por bloqueios comerciais, crises políticas, má distribuição de alimentos. o número é sim menor do que as contas de Stalin -- mas não era pra ser agora em um sistema de _abundância_?

a pergunta que a gente não pode parar de fazer: _abundância pra quem, cara pálida?_

também porque ao capitalismo nem interessa que as pessoas morram: melhor que continuem vivas, com fome, para trabalhar e produzir e fazer girar as engrenagens do sistema.

fiz essas contas porque ouvi esses dias de um filho de judeus fugidos da segunda guerra que Stalin matou mais do que Hitler e meu estômago se revirou um pouquinho logo antes do jantar. porque aos vencedores se permite escrever a história, e disso ninguém fala, nunca. porque precisamos aprender a desconfiar do tipo de narrativa que faz voltas para defender a exploração sistemática e intencional daqueles mais vulneráveis e continua matando, silenciosamente, atrás das câmeras e por baixo dos panos da história, em nome da democracia e da liberdade.

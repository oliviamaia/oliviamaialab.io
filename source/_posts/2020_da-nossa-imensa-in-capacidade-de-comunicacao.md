---
layout: "post"
title: "da nossa imensa (in)capacidade de comunicação"
date: "2020-01-09 15:21"
---

claro que a gente sabe articular palavras e emitir sons coerentes e compreensíveis (quase sempre) e, na maior parte do tempo, fingir que nos comunicamos, pedir água numa lanchonete ou informar que temos fome. mas cada vez mais me parece que a falta da arte e da literatura vai matando aos poucos um pedacinho das pessoas; essa capacidade de olhar pra si e refletir um pouquinho, e de tentar se comunicar pra além das necessidades básicas e imediatas.

a arte não promove empatia nem ensina a se colocar no lugar dos outros. a arte nos lembra de que somos humanos, mais que nada. a arte grita humanidade. sem ela a gente se comunica como meus gatos quando estão aborrecidos com a chuva e começam a bater uns nos outros.

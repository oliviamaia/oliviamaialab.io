---
layout: "post"
title: "formas talvez não muito eficientes de se lidar com depressão e ansiedade"
date: "2020-01-20 15:53"
---

1. assistir a todas as temporadas de THE MENTALIST de uma vez
2. montar quebra-cabeças no tablet
3. configurar o openbox com rofi, compton, polybar, tint2 etc como manda a cartilha #unixporn
4. instalar, testar e configurar aplicativos de linha de comando pra e-mail, agenda, música, gerenciador de arquivos, rss, twitter, mastodon, irc, reddit etc etc etc
5. descobrir formas de tornar esses aplicativos cada vez mais eficientes
6. corrigir erros causados por edições desavisadas em arquivos de configuração

wash, rinse, repeat

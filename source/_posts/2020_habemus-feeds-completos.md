---
layout: "post"
title: "habemus feeds completos"
date: "2020-01-17 10:53"
---

finalmente. pra comemorar essa **conquista**, uma foto do processo de construção do novo _keyhole garden_ (ou canteiro/jardim fechadura) aqui em casa:

![imagem: canteiro suspenso em forma de fechadura.](/img/posts/26ecf215ab7ac6e0.png)

---
title: " a cultura depois da guerra"
date: 2017-04-02 10:37:10
---

a reação com os absurdos da humanidade se alternam entre a surpresa constante e a ideia de que nada mudou: continuamos nos surpreendendo com as mesmas coisas que espantavam os cidadãos europeus na segunda metade do século XIX. ou: critica-se a sociedade que se mantém imersa em seu mundo particular com os smartphones, mas uma fotografia do começo do século XX revela um vagão de trem em que todos estão imersos em seus mundos particulares com as caras no jornal aberto. [![](/img/2017/03/SoG7Yqh-660x450.jpg)](http:/img/2017/03/SoG7Yqh.jpg) algum esperto vai dizer: vê! nada mudou. (tem também aquele que vai dizer [_não é bem assim_](https://medium.com/alt-ledes/stop-sharing-this-photo-of-antisocial-newspaper-readers-533200ffb40f).) Antonio Candido em seu incansável otimismo disse que a prova de que humanidade progride é que hoje, embora ainda existam os genocídios, ninguém se _orgulha_ publicamente por ser responsável por um genocídio. a consciência humana estaria em processo de transformação. (ou seria mero efeito de _superego_ coletivo exagerado pela expansão do alcance da mídia?)

> It may well be that it is a mere fatuity, an indecency to debate of the definition of culture in the age of the gas oven, of the arctic camps, of napalm. The topic may belong solely to the past history of hope. But we should not take this contingency to be a natural fact of life, a platitude. We must keep in sharp focus its hideous novelty or renovation. We must keep vital in ourselves a sense of scandal so overwhelming that it affects every significant aspect of our position in history and society. We have, as Emily Dickinson would have said, to keep the soul terribly surprised.

George Steiner, _In Bluebeard's Castle_

o _superego_ vigilante como o elemento de constante surpresa. mas se esse horror não é um fato natural da vida, se precisamos seguir adiante tomando-o sempre como absurdo e escandaloso -- que se pode fazer contra ele? basta manter-se surpreso e dizer que não, isso não é normal? impedir enfim que o horror seja motivo de orgulho. enquanto isso a história (envergonhada) segue se repetindo.

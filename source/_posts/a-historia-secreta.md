---
title: " a história secreta"
date: 2010-08-30 05:07:56
---

> Você deve estar se perguntando o que é a história secreta, disse meu amigo. Pois bem, a história secreta é aquela que nunca conheceremos, a que vivemos dia a dia, pensando que vivemos, pensando que temos tudo sob controle, pensando que o que passa batido por nós não tem importância.

"Dentista", Roberto Bolaño, no livro _Putas Assassinas_.

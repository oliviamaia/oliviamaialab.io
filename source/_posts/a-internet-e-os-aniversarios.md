---
title: " a internet e os aniversários"
date: 2015-12-06 22:44:28
---

> It was my birthday recently. Perhaps you heard? Sorry about that! Google Plus, the zombie social network I have barely used since its launch in 2011, alerted my contacts that have Android phones. And anyone with iCal synced to Google Calendar had it marked in their iPhones. “The internet is trying to tell me that it’s your birthday,” someone wrote in an email to me. Throughout the day several others would send similarly kind but bewildered messages.
>
> \-\- ["The Internet of Things Will Ruin Birthdays"](https://medium.com/message/the-internet-of-things-will-ruin-birthdays-8a5b781abb6b)

porque tão prático que o facebook avise dos aniversários; sem contar a praticidade de uma mensagenzinha de duas linhas no mural do aniversariante pra dizer que _pensei em você!_; ou sentir-se mui amado pelas dezenas de mensagens no fim do dia; etc. é que há de se pensar que os aniversários dos amigos mais próximos a gente devia anotar na nossa agenda, colar no calendário, fazer uma listinha na parede da escada (como sempre fez minha avó-postiça esposa de meu avô única pessoa da família a saber o aniversário de todo mundo) revista e atualizada todo fim de ano. ou talvez eu seja mesmo muito cínica porque nunca consegui dar crédito pras mensagens de feliz aniversário que me mandavam no facebook (quando tinha facebook), e nunca tive coragem de escrever mensagens no mural quando era isso que o facebook me sugeria fazer. (e se me calhava o aviso eu dava um jeito de mandar mensagem por outro meio, que era pra pelo menos tentar disfarçar.)

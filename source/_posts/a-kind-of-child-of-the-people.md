---
title: " a kind of child of the people"
date: 2010-06-04 21:53:00
---

de ler Enrique Vila-Matas, inevitável ir atrás de Robert Walser. topei primeiro com o ensaio do Walter Benjamin, em que se fala do pudor em escrever presente no texto de Walser. como se cada frase fosse um pedido de desculpas por se estar escrevendo.

quando comecei a ler o romance _O ajudante_ (parece que o único traduzido pro português), pensei que havia algo de verdade na afirmação do Benjamin.  

> Certa manhã, às oito horas, um jovem estava diante da porta de uma casa isolada e de bela aparência. Chovia. "Para mim", pensou o rapaz ali postado, "é quase um milagre ter nas mãos um guarda-chuva". O certo é que jamais tivera um guarda-chuvas em sua infância. Numa das mãos, estendida verticalmente ao longo do corpo, segurava a mala marrom, dessas bem baratas. Diante dos olhos desse que aparentemente chegava de viagem, havia uma placa esmaltada em que se lia: K. Tobler, Escritório Técnico. Esperou ainda um instante, como para refletir sobre qualquer coisa certamente sem nenhuma importância, depois apertou o botão da campainha elétrica, a qual atendeu uma pessoa, que tudo levava a crer ser a criada, para deixá-lo entrar.  
>
> começo de _O ajudante_  

que se pode dizer?

parece que no texto sempre um esforço enorme pra agarrar o mundo, entender o mundo ou, mais ainda, explicar-se, pedir licença, veja bem, senhor mundo. não resisti comprei um _Selected stories_, com tradução do alemão pro inglês.

me apaixonei.

as descrições são intermináveis, e se são às vezes até mesmo cansativas, viram de repente o texto do avesso numa pergunta -- que segundo a Susan Sontag, que escreveu o prefácio, é uma cortesia do Walser -- como se nem mesmo ele estivesse tão certo de tudo aquilo, da beleza do mundo, da alegria ou da tristeza ou de qualquer outra certeza que se poderia ter sobre qualquer coisa.  

> Is it really the case that I'm a kind of child of the people, who doesn't yet understand even himself? That would be terrible.  
>
> _Am I demanding?_  

ele devia saber o quão terrível.

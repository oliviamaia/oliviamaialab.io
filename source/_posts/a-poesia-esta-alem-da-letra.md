---
title: " a poesia está além da letra"
date: 2008-11-30 02:28:32
---

se já ameaçava, José Paulo Paes entra de vez na minha lista de escritores que falam comigo. escritores que me salvam, quando tudo o mais parece perdido. Guimarães, Mário, Cortázar e Joyce te saúdam, Zé. não quero mais nada dessa vida.

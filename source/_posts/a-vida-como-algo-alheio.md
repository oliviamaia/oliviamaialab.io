---
title: " a vida como algo alheio"
date: 2010-11-06 13:31:00
---

> O que nos salva a todos é uma vida tácita que pouco tem a ver com o cotidiano ou o astronômico, uma influência espessa que luta contra a fácil dispersão em qualquer conformismo ou qualquer rebeldia mais ou menos gregários, uma catarata de tartarugas que não acaba nunca de dar pé porque desce com um movimento retardado que mal guarda relação com nossas identidades de foto três quartos sobre fundo branco e impressão dígito-polegar direito, a vida como algo alheio mas que ao mesmo tempo é preciso cuidar, o menino que deixam com a gente enquanto a mãe vai tomar alguma providência, o vaso com a begônia que regaremos duas vezes por semana e por favor não jogue mais do que uma jarrinha de água, porque a coitada morre.

Julio Cortázar _62  Modelo para Armar_

esse eu já tinha começado a ler, comprado em sebo. minha estante vai se demitir se eu empilhar mais um livro por cima dos outros livros.

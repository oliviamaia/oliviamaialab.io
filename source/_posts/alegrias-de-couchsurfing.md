---
title: " alegrias de couchsurfing"
date: 2014-05-22 09:29:34
---

couchsurfing é um troço muito massa. eu sempre acho meio temeroso, porque você não conhece a pessoa e ela pode não ter nada a ver com você, e você fica lá meio sem jeito numa casa estranha etc. até agora nunca. digo: sempre acaba dando certo. em Foz por exemplo eu acabei prolongando minha estadia pra passar uns dias com a Eva, uma uruguaia do couchsurfing que me convidou pra ficar na casa dela. a gente se divertiu horrores, mesmo com ela precisando terminar um trabalho pra faculdade e não podendo me acompanhar até Puerto Iguazu pra visitar o marco das três fronteiras e a feirinha.

claro que teoricamente a gente escolhe pra quem mandar o pedido, e imagina que quem te aceita tem algum interesse em te conhecer. mas albergue é mais fácil: você está lá e não precisa falar com ninguém se não tiver vontade. mas aí cheguei em Posadas e fui recebida pela Mia, que é quietinha e quase séria mas uma pessoa muito massa. cheguei, almoçamos e conversamos um pouco. depois ela tinha que estudar e eu fiquei desenhando e escrevendo. à noite a gente caminhou pela costaneira e fez pizza com queijo de verdade (queijo argentino é bom demais). um amigo dela que mora por ali veio pra bater papo e me perguntar mil coisas sobre o português, que ele está aprendendo.

pena que por causa do horário de ônibus pro Brasil, que só sai duas vezes por semana, não deu pra ficar mais um pouco. acordamos cedo e fomos até o rio Paraná: sentamos numas pedras e ficamos tomando sol e mate, até dar a hora de voltar, fechar a mochila e rumar à rodoviária. 

---
title: "angastaco pueblo-surpresa"
date: 2014-11-07 12:58:01
---

o povoado já me pareceu mui simpático à noite; uma praça grande, uma igrejinha, um cinema, meia dúzia de ruas. pela manhã todo verdinho ensolarado claro que ainda mais lindo.

trabalhei um pouco de manhã e caminhei à tarde; essas ideias tortas de turista que não sabe que a hora da siesta é pra se meter debaixo de uma sombra e esquecer do mundo. segui a indicação do rapaz da hostería e alcancei o rio Angastaco, praticamente seco, e segui na direção da rodovia. queria subir ao hotel abandonado, um pedaço de construção inacabada que já se vê de qualquer ponto do rio por ali.

foi caminhada interminável mais pelo sol e pelas pedras e areia, mas enfim cheguei ao lado do morro em que está o hotel, e perguntei a um pastor de cabras sentadinho num pedaço do morro como subir. ele me indicou o caminho e depois todo encabulado (era um senhor mais velho) disse que pensou que eu era um rapaz. fui subindo até a plataforma onde começaram a construir o elefante branco (e laranja e vermelho). a vista mui linda e nenhum fantasma. ao menos não àquela hora, que obviamente fantasmas também devem dormir a siesta.

dali se vê bem o caminho do rio e o começo do que é a _quebrada de las flechas_, no caminho a Cafayate pela _ruta 40_. um morros cinzento e cheio de pontas inclinadas como setas. à esquerda mais morro atrás do povoado, metido e escondido entre árvores depois de um terreno de vinhedos. voltei pela rodovia e no meio do caminho consegui uma carona com uma senhora numa caminhoeta que me deixou na porta da hostería.

no dia seguinte fui mais esperta e fiz a caminhada pela manhã: segui à igreja velha, às costas do povoado, e subi a um mirante atrás dela. uma subidinha mei interminável mas uma vista que valeu a pena. apesar do sol o vento estava forte a agradável e eu fiquei por ali sentada e lamentei um pouco que o sol estava contra e essa poeira de clima seco não deixava fotografar a vista em todo seu esplendor etc. venham vocês a Angastaco, enfim.

porque também dali se vê uns pedaços do que vai ser a _quebrada de las flechas_ mas àquela hora tudo mui cinzento e pouco nítido. ainda à direita outros morros mais próximos todos com esse mesmo _look alien_ lunar. aliás, todo o povoado está metido entre esses morrinhos arenosos com formas estranhas. é um povoadinho minúsculo e simpático, com vistas lindas pra todos os lados que os olhos alcançam. infelizmente um lugar de passagem, um pouco pelas condições da estrada e também porque de fato estrutura de turismo não tem muita além da hostería, duas hospedagens do tipo casa de família e uns quatro restaurantes. tinha comprado a passagem pras quatro da tarde porque queria um horário em que pudesse ver o caminho. deu tempo de voltar da caminhada, tomar um banho e me acomodar na varanda da hostería usando a internet _del pueblo_ pra continuar atualizando a vida online.

---
title: " ao professor Pedro, de quem fui aluna, muito me orgulho e faço alarde!"
date: 2011-09-27 00:06:00
---

todos que passam pela vida da gente fazem alguma diferença, de algum jeito. dá-lhe a pensar em bifurcações e os motivos que levaram a essa ou aquela escolha. as oportunidades. o lugar certo na hora certa. a forma como certas coisas parecem todas bem amarradas.

e aí, então.

vê: retomei meu gosto pela leitura por causa dele. aprendi a gostar de Guimarães Rosa, me encantei com Mário de Andrade. voltei a escrever. decidi que não queria fazer artes plásticas, e sim letras. "mas não vai parar de escrever, Olivia. conheço muita gente que parou de escrever depois que fez letras." deixa comigo, professor.

eu voltava sempre para assistir às aulas dele, depois de formada no colégio. voltava tanto que acabava me intrometendo em outros assuntos. então a oportunidade de trabalhar com correção de redação. por causa dele? diretamente, não. mas era por causa dele, das aulas dele, que eu voltava, sempre. "você foi uma dessas alunas que ficou", ele me disse uma vez.

e eu disse pra ele: "fiz letras por sua causa, professor." verdadíssima. não me arrependo. ele fazia que era exagero. mal sabia ele.

então tudo muito bem amarrado. minhas escolhas, minhas, sem dúvida. mas todas elas as mais importantes foram porque tive literatura com ele. porque o ouvi falar de Guimarães Rosa e Mário de Andrade e meu deus, que troço incrível pode ser a literatura. nunca vou esquecer as aulas sobre o conto "Nenhum, nenhuma", do _Primeiras estórias_. e de repente aquele texto enigmático ia fazendo sentido, ganhando sentidos.

dá-lhe a pensar em bifurcações e ele está em todas elas, querendo ou não. se sou escritora e professora é porque ele estava lá no meu terceiro ano do colégio discorrendo sobre as madeleines de Proust no meio de uma aula sobre Guimarães Rosa. de repente me dou conta disso. não sei pensar o que eu seria se não o tivesse conhecido. o que estaria fazendo. não fosse ele, minha vida seria outra.

e ele repetia, naquele sotaque mui limeirense, o dedo em riste: "como diria o professor Antonio Candido, de quem fui aluno, muito me orgulho e faço alarde!"

como pode? porra. cinquenta e seis anos. que vou dizer?

obrigada, Pedro.

e só posso esperar que ele tenha ido pro inferno, que era pra onde sempre dizia que queria ir, porque "o céu deve ser um tédio, aquele monte de anjinho tocando harpa o dia inteiro, que saco!"

_...mas era primavera e até o leão lambeu o rosto glabro da leoa._

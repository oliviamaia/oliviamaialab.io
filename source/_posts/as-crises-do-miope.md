---
title: " as crises do míope"
date: 2008-05-12 05:49:25
---

você sabe que sua miopia já começa a ser inconveniente quando não consegue mais cortar as unhas dos pés sem óculos.

ou, ainda, quando começa a surgir aquela crise na hora de pentear os cabelos: com óculos você não consegue pentear, porque os óculos atrapalham. sem óculos você não se enxerga no espelho.

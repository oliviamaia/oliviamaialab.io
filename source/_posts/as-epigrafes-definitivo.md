---
title: " as epígrafes (definitivo)"
date: 2011-04-10 21:56:00
---

Cortázar por todos os lados. justo, se pensar que a culpa por esse meu último livro é toda dele. uma overdose de Cortázar. dizer que _sim, mestre_, estou vendo se aprendo e faço do meu jeito. não acho que aprendi, ainda. mas estou fazendo do meu jeito, o que é um começo. daí que precisei me segurar muito para não ter cinquenta mil epígrafes. tudo era ponto de partida. mas consegui reduzir o repertório a três. a primeira numa página sozinha, que é ponto de partida MASTER. as outras duas na página seguinte, porque são os ecos.


_Tudo seria como uma inquietação, uma falta de sossego, um desarranjo contínuo, um território onde a causalidade psicológica cederia, desconcertada, e esses fantoches se destruiriam ou se amariam ou se reconheceriam sem suspeitar demasiado de que a vida procura trocar a clave deles e através deles e por eles, de que uma tentativa ainda pouco concebível nasce no homem da mesma forma como outrora foram nascendo a clave-razão, a clave-sentimento, a clave-pragmatismo. Que a cada sucessiva derrota há uma aproximação da mutação final, e que o homem não é, mas procura ser, projeta ser, algemado entre palavras e comportamento e alegria salpicada de sangue e outras retóricas como esta._

Julio Cortázar  
**Jogo da amarelinha**, capítulo 62


_Quem é que tinha a perfeita consciência de si mesmo, da solidão absoluta que significa ter de entrar num cinema ou num bordel, ou em casa de amigos ou numa profissão absorvente ou no matrimônio para estar pelo menos só-entre-os-demais._

Julio Cortázar  
**Jogo da amarelinha**


_Assim, profissionalmente, assim em todos os planos; aquela que teme a violação profunda de sua vida, a invasão na ordem obstinada de seu abecedário, Hélène que só entregou seu corpo quando tinha a certeza de que não a amavam, e só por isso, para deslindar o presente e o futuro, para que ninguém subisse depois para bater à sua porta em nome dos sentimentos._

Julio Cortázar  
**62  Modelo para armar**

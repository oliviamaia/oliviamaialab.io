---
title: " as leituras de janeiro"
date: 2017-01-31 20:14:00
---

pois desde setembro do ano passado que engatei nas leituras de vez, de volta ao mundo da literatura depois de um tempo ausente por motivos diversos. agora acho que li mais neste mês de janeiro do que em todo o ano de 2015, por exemplo. com certeza li mais do que em todo o ano de 2012, quando ainda fazia listas. inclusive voltei a fazer listas. também [estou no goodreads](https://www.goodreads.com/oliviamaia), como já disse por aqui. enfim, as leituras de janeiro:

1. _In Bluebeard's Castle_, George Steiner 
2. _Heretics_, G. K. Chesterton 
3. _Men Explain Things to Me_, Rebecca Solnit 
4. _The Actuality of Communism_, Bruno Bosteels 
5. _As aventuras da dialética_, Maurice Merleau-Ponty 
6. _The Communist Horizon_, Jodi Dean 
7. _Azul é a cor mais quente_, Julie Maroh 
8. _Sobre el arte contemporáneo & En la Habana_, César Aira 
9. _As artes da palavra: Elementos para uma poética marxista_, Leandro Konder 
10. _A estrela da manhã: surrealismo e marxismo_, Michael Löwy 
11. _As cinco dificuldades de escrever a verdade_, Bertolt Brecht 
12. _Os irredutíveis: teoremas da resistência para o tempo presente_, Daniel Bensaïd 
13. _Para você não se perder no bairro_, Patrick Modiano 
14. _O mal-estar da pós-modernidade_, Zygmunt Bauman 
15. _Quem matou Roland Barthes_, Laurent Binet 
16. _Dialética do esclarecimento_, Theodor Adorno & Max Horkheimer 
17. _The Enemies of Books_, William Blades 18. _Manifesto contra o trabalho_, Grupo Krisis

só duas leituras de ficção (13 e 15), mais uma de quadrinhos (7). muitos barbudos e/ou comunistas, evidentemente. umas coisas engraçadinhas pelo caminho, mas não muitas. acho que preciso mesmo uma pausa nas leituras cabeçudas e voltar um pouco à ficção. vamos ver o que vem pela frente neste mês de fevereiro (não faço mais planos; vou abrindo livros e empilhando, e lendo cinco ao mesmo tempo, conforme o momento e a vontade).

---
title: " as much as death is inevitable life is inevitable"
date: 2016-11-20 20:41:51
---

> The most solid advice, though, for a writer is this, I think: Try to learn to breathe deeply, really to taste food when you eat, and when you sleep, really to sleep. Try as much as possible to be wholly alive, with all your might, and when you laugh, laugh like hell, and when you get angry, get good and angry. Try to be alive. You will be dead soon enough.

William Saroyan, _The Daring Young Man on the Flying Trapeze_ (prefácio)

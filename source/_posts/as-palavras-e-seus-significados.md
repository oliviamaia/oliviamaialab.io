---
title: " as palavras e seus significados"
date: 2014-01-11 13:09:14
---

> Já foi dito que o autor pressupõe a existência de palavras que, segundo estabelecem os dicionários, têm certos significados, e não outros. Como se sabe, as figuras de retórica foram inventadas porque todos sabiam que não era verdade, que na realidade as palavras tinham muitos significados, simultâneos e inconciliáveis. Mas talvez se possa ir mais além: afirmar que as palavras têm todos os significados, e não apenas os do dicionário, mas sobretudo os significados vagos e flutuantes que nenhum dicionário é capaz de captar e catalogar, significados que estão propriamente entre uma e outra palavra.
>
> \- Giorgio Manganelli, Pinóquio: um livro paralelo

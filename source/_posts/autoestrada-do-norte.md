---
title: " autoestrada do norte"
date: 2012-12-04 09:26:15
---

[engarrafamento de 200 km na Rússia já dura cinco dias](http://g1.globo.com/mundo/noticia/2012/12/engarrafamento-de-200-km-na-russia-ja-dura-cinco-dias.html).

"o governo russo admitiu que as estradas do país precisam de melhorias." [alguém no twitter](https://twitter.com/RobertaSimoni/status/275913112165683202) comentou o quanto isso fazia lembrar um certo conto do Cortázar. desses contos que a gente lê e diz que o _realismo fantástico_ e o _absurdo_ etc. mas não custa pensar em Pirandello, que bem lembrou o quanto a ficção muitas vezes inventa a realidade, e não o contrário.

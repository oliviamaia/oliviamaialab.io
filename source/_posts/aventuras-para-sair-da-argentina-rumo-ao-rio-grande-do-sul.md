---
title: "aventuras para sair da Argentina rumo ao Rio Grande do Sul"
date: 2014-05-23 09:36:29
---

ônibus de Posadas pra Santa Rosa sai sexta-feira e segunda-feira, meio-dia. eu tinha comprado logo que cheguei a passagem pra sexta-feira. pois no dia o vendedor da empresa de ônibus me diz que não vai ter ônibus, que o que vinha ficou na aduana brasileira e não conseguiu entrar na Argentina. solução: tomar um ônibus até San Javier, na fronteira. do terminal de lá, um táxi até o porto (35 pesos), aí passar pela aduana e cruzar o rio Uruguai de lancha ou balsa, junto dos caminhões de cebola (mil caminhões de cebola na fila pra cruzar o rio).

enfim Brasil: a cidade se chama Porto Xavier e pra chegar na rodoviária era só seguir em frente na avenida (cidade de uma avenida só). a entrada pra rodoviária ficava ali na altura da praça. cheguei passava das três da tarde e o ônibus dali pra Santa Rosa saía 19h. tudo bem, tudo bem. logo ao lado tinha uma lanchonete pra matar o tempo. de Porto Xavier até Santa Rosa foram só mais duas horas, e 21h quase em ponto fui recebida por lá pela Maiara, do couchsurfing. ufa ufa. missão cumprida.

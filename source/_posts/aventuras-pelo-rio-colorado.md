---
title: " aventuras pelo rio colorado"
date: 2014-11-08 10:47:05
---

nos juntamos numa manhã de domingo que por acaso era também o dia das mães: Pablo o portenho, Sílvia a espanhola, Charlie a francesa, e eu. tomamos um remis até o camping do rio Colorado (são 6 km em subida e o plano era voltar caminhando) e topamos ali com um dos guias querendo cobrar 30 pila por _cascada_ pra nos levar pelas cinco primeiras cachoeiras. já sabíamos que as primeiras são uns chorinhos de água e ele disse que sim, isso incluía duas grandes. ainda assim era muito caro e ele disse que 125 por pessoa. sei que ao final Charlie disse que pagava 60 e o guia propôs 70. aceitamos. ele seguiu na frente feito um cabrito saltando as pedras e subindo os morros. paramos num miradorzinho e começamos a seguir o rio. de fato as primeiras cachoeiras são mei sem graça, mas o rio é uma lindezinha: uma água cristalina, às vezes esverdeada, e GELADA. o dia anterior tinha feito um calor monstro mas aquela manhã ia ameaçando nublar e os planos de se jogar na água foram minguando conforme seguíamos, e principalmente porque a água vai ficando cada vez mais fria quando mais se sobe. o caminho a princípio parecia tranquilo, mas logo precisamos passar por umas pedras meio inclinadas e a Sílvia foi ficando mei nervosa porque vertigem e tudo mais. o guia garantia: agora o caminho começa a ficar um pouco mais complicado.

pois. era uma de escala pedra, se agarra na pedra, se mete pela pedra. quando chegamos na última cachoeira do dia eu estava com fome e queria aproveitar um pedacinho de pedra plana pra fazer um piquenique ali mesmo, mas a perspectiva de precisar escalar umas pedras pra continuar o caminho estava deixando a turma mei ansiosa

vencemos um trechinho estreito e inclinado até o ponto da escalada e mandamos o Pablo na frente porque era o mais gordinho e se passasse no meio daquelas pedras era porque nós também passávamos

enfim alguns minutos depois estávamos todos ali em cima pro esperado piquenique.

o caminho de volta foi mais tranquilo, por trás do morro, em descida. nem deu muito pra cansar. só pra forçar o joelho mesmo. a turma costuma ir atrás das cachoeiras sem guia e talvez seja mesmo possível, mas deve ser bem complicado. mais complicado do que conseguir contornar os guias insistentes na entrada, porque eles ficam lá de cão de guarda botando medo na turma. vez ou outra alguém se perde. mas no fundo o grande lance é seguir o rio (e seguir em frente quando o caminho olha pra você e você pensa: não deve ser por aqui). (ou seja: mei complicado.) (e mui divertido.) voltamos num remis que nos parou no meio da caminhada. Charlie tinha que pegar o ônibus das 18h pra Salta e na verdade acabamos chegando de volta ao centro de Cafayate antes das 16h.

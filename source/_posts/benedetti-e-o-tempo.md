---
title: " Benedetti e o tempo"
date: 2010-05-20 23:07:00
---

eu tinha lido _A trégua_, do Mario Benedetti, e me encantado. um pouco mais do cotidiano, e na forma de diário também uma despretenção que tira da narrativa o peso de um objetivo literário maior. a história se faz sozinha pelos caminhos do personagem-narrador que escreve o diário-livro. não se busca tão imediatamente qualquer transcendência nos acontecimentos. e está ali a história, um pouco triste, desse sujeito que se sente velho.

aí que numa promoção de uma livraria comprei um livro de contos do Benedetti: _Correio do tempo_. uns contos curtinhos, fui lendo aos poucos sem perceber que o livro chegava ao fim. mas tão terrível esse transformar em palavras a noção abstrata do tempo, de um inevitável. a percepção da pequeneza de tudo aquilo que é maior que nós.

> E o adeus dizia: "Não suporto o mundo. Quero me encontrar. Desta vez a solidão é imprescindível. Não estou louco. Não estou delirando. Hoje quando você enfrentar o noticiário na televisão e vir mais negrinhos esqueléticos do Sudão, jangadas com marroquinos naufragando em Gilbraltar, índios do Amazonas empurrados para o próprio fim, cursos básicos de violência juvenil, além da desenfrada e programada destruição da natureza, e depois, no mesmo canal ou no seguinte, a arrogância dos governantes, demo ou autocráticos, dá quase na mesma, exibindo sem pudor sua fome de poder; sua indiferença pelo próximo, singular ou plural, e também os grandes salões da Bolsa, com a histeria milionária dos apostadores; quando vir tudo isso, talvez você entenda por que não suporto mais o mundo. (...)"
> 
> \[Primavera dos outros\]             

me encanta principalmente essa linguagem que não exagera demais numa busca por precisão, como se acompanhando as incertezas e desvios de raciocínio que são nossos, que são inevitáveis, e que talvez não mereçam sumir de todo do texto escrito. é uma linguagem sem vergonha das banalidades do cotidiano, mas que não por isso faz delas uma bandeira.

> Sabia que a morte é uma interminável planice cinzenta? Garanto que não voltarei a te incomodar. Isso mesmo, a morte é uma interminável planice cinzenta. Sem aleluias. Cinzenta.
> 
> \[Secretária eletrônica\]

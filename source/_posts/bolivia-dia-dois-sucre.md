---
title: " bolívia, dia dois: sucre"
date: 2011-07-18 03:49:58
---

altitude so far so good, mesmo com a caminhada até o mirante no topo da cidade -- um dos topos da cidade -- com essas ladeiras infinitas. o coração dispara porque um pique escada acima pra buscar qualquer coisa na cozinha aqui no alojamiento, mas ok.

curioso como se acostuma cumprimentar a todos com 'hola', mesmo os gringos com quem depois logo se vai conversar em inglês.

andamos um monte e que coisa incrível essa cidade, os cantos escondidos e perdidos no tempo, parados no tempo, as placas de comércio feito vila de filme de velho oeste. o mercado é também casarão antigo, a porta de madeira fechada por um cadeado enorme quando cedo demais e ainda não estavam abertos -- e outras voltas até achar uma vendinha pra um bolinho de laranja mui safado e iogurte de potinho pra fazer o café da manhã.

e depois mais futebol e apostas. não acertei nada, mas o rogério levou no zero a zero dos primeiros 90 minutos do jogo do brasil.

o ar uma secura dos piores dias de inverno em são paulo. frio suportável ainda que agora de noite não me arrisque a sair na rua. fora do quarto e do lounge já me parece frio o suficiente. durante o dia foi um sol forte e até quente, de ficar sem blusa e se esconder atrás dos óculos escuros.

mais rinosoro pra mim antes de dormir que essa cerveja judas me deu um soninho legal. bueno.

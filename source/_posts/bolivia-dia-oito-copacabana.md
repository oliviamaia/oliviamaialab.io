---
title: " bolívia, dia oito: copacabana"
date: 2011-07-24 02:39:52
---

altitude não acaba nunca.

saímos pra andar por aí. ver pedras de tribunal inca, observatório astronômico inca. subir dez metros numa ladeira íngreme aqui é um troço terrível. e subimos os dois morros que emulduram a cidade. o lado pagão de manhã, pra ver ruínas incas, e à tarde o lado cristão, o cerro calvario, que termina numa vista monstro do lago titicaca.

quer dizer, esse lago titicaca é um monstro por si. parece praia, parece mar, não se vê o fim. e azul azul. quando nubla fica todo prateado.

o que mais tem por aqui é peruano e brasileiro. tem também os gringos fedidos de sempre. conversei com um tipo hippie brasileiro que vende colar e pulseira na calçada e ele disse que vai por aí caçando turismo, e viajando. tem um monte desses por aqui também, de nacionalidades variadas.

meu pé fez até uma bolhazinha. chega o fim do dia e vinte metros em linha reta já fica cansativo. deve ter sido o dia que mais andamos. meus músculos estão com preguiça do mundo.

compramos os tickets pra isla del sol. amanhã, buena, duas horas de barco. com o titicaca gelado aos nossos pés. que medo.

---
title: " bolívia, dia sete: copacabana"
date: 2011-07-23 02:19:04
---

ufas ufas. enfim banho e descanso. até as roupas sujas foram à lavanderia. a vontade era tirar a pele e deixar na lavanderia também.

o lago titicaca é monstruoso. não tem fim. a estrada pra copacabana vai rodeando o lago, (às vezes temerosamente) e num momento a van foi de balsa e tivemos que descer pra cruzar de lancha. a água refletindo o céu azul azul; azul escuro brilhando com o sol forte.

a cidade é miúda e cheia desses turistas com calça listrada e blusa de lhama. muitos tipos ciganos pouco bolivianos vendendo artesanato na calçada. e as lojinhas que vendem as calças listradas e blusas de lhama, todas iguais. andamos pouco; mais ficar um tanto à toa pra aguentar as caminhadas dos próximos dias.

friozinho mais ameno que uyuni, mas ainda assim bom pra ficar debaixo de cobertores. a água pra beber aqui sempre gelada; geladeira pra quê? nem o iogurte precisa refrigeração. minhas luvas de alpaca estão todas maltratadas, pobrezitas. agora dentro do quarto blusa de lã e fleece e corta-vento e meias de neve. nem sei quantos graus, mas nada como o frio com as roupas certas.

fui ver a máquina e já tirei mais de 700 fotos. rogério tirou mais de 400. ficamos aqui imaginando como seria se só tivéssemos filme de 36 poses. deos.

ainda estou tentando entender qual é a dos bloqueios nas estradas. passamos a la paz por pouco, parece, e escapamos de ficar presos no caminho também por muito pouco, que hoje à tarde os arredores de potosí e oruro ficaram bem conturbados. o telejornal no restaurante agora à noite mais mostrava imagens que explicava.

(nem reparem os erros de ortografia e gramática nesse e nos posts anteriores, que escrevo do celular e, né, perdoa.)

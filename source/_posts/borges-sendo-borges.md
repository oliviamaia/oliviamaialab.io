---
title: borges sobre a divina comédia
date: 2022-03-26 09:10:52
---

> Um gran libro como la _Divina Comedia_ no es el aislado o azaroso capricho de un individuo; muchos hombres y muchas generaciones tendieron hacia él. Investigar sus precursores no es incurrir en una miserable tarea de carácter jurídico o policial; es indagar los movimientos, los tanteos, las aventuras, las vislumbres y las premoniciones del espíritu humano.

do livro _Nueve ensayos dantescos_.

que seja também dizer que tudo que já estava dito e escrito não precisa talvez sequer ser ouvido e lido, porque um pouco o vento também carrega as ideias, através das gerações, conversando em silêncio com o espírito humano.

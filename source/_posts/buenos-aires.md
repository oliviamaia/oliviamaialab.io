---
title: " buenos aires"
date: 2010-11-13 15:08:14
---

> Não entendi por que os argentinos preferiam escrever histórias fantásticas ou inverossímeis sobre civilizações perdidas, ou clones humanos, ou hologramas em ilhas desertas, quando a realidade estava viva e a gente a sentia queimar e se queimar, e doer na pele das pessoas.

Tomás Eloy Martínez, _O cantor de tango_

---
title: " cada um apeia do cavalo, etc\t\t"
date: 2008-12-17 20:36:41
---

qualquer coisa no meu relógio biológico que me dá ímpetos de escrever só quando passa das seis da tarde. mesmo quando quero escrever antes disso. lutar contra ou adaptar adaptar-se.

> "Um tipo é a ansiedade de aniquilante estreiteza, da impossibilidade de escapar e o horror de estar sendo agarrado. O outro é a ansiedade de aniquilante vastidão, de espaço infinito e sem forma, no qual se cai sem um lugar sobre o qual tombar."
> 
> Paul Tillish, "A coragem de ser"

claro que também estou sempre escrevendo outra coisa que não aquilo que resolvi que deveria seguir escrevendo.

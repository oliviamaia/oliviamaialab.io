---
title: camping de aniversário
date: 2022-03-05 11:18:56
photos:
  - /img/posts/camping/IMG_3787.JPG
  - /img/posts/camping/IMG_3788.JPG
  - /img/posts/camping/IMG_3789.JPG
  - /img/posts/camping/IMG_3791.JPG
  - /img/posts/camping/IMG_3794.JPG
  - /img/posts/camping/IMG_3799.JPG
  - /img/posts/camping/IMG_3800.JPG
  - /img/posts/camping/IMG_3801.JPG
  - /img/posts/camping/IMG_3806.JPG
  - /img/posts/camping/IMG_3808.JPG
  - /img/posts/camping/IMG_3809.JPG
  - /img/posts/camping/IMG_3811.JPG
  - /img/posts/camping/IMG_3817.JPG
  - /img/posts/camping/IMG_3825.JPG
  - /img/posts/camping/IMG_3827.JPG
  - /img/posts/camping/IMG_3828.JPG
  - /img/posts/camping/IMG_3829.JPG
  - /img/posts/camping/IMG_3836.JPG
  - /img/posts/camping/IMG_3839.JPG
  - /img/posts/camping/IMG_3840.JPG
  - /img/posts/camping/IMG_3844.JPG
  - /img/posts/camping/IMG_3846.JPG
  - /img/posts/camping/IMG_3847.JPG
  - /img/posts/camping/IMG_3848.JPG
  - /img/posts/camping/IMG_3855.JPG
  - /img/posts/camping/IMG_3857.JPG
  - /img/posts/camping/IMG_3859.JPG
  - /img/posts/camping/IMG_3873.JPG
  - /img/posts/camping/IMG_3874.JPG
  - /img/posts/camping/IMG_3875.JPG
  - /img/posts/camping/IMG_3877.JPG
  - /img/posts/camping/IMG_3880.JPG
  - /img/posts/camping/IMG_3881.JPG
  - /img/posts/camping/IMG_3885.JPG
  - /img/posts/camping/IMG_3887.JPG
  - /img/posts/camping/IMG_3890.JPG
  - /img/posts/camping/IMG_3892.JPG
  - /img/posts/camping/IMG_3893.JPG
  - /img/posts/camping/IMG_3895.JPG
  - /img/posts/camping/IMG_3897.JPG
  - /img/posts/camping/IMG_3904.JPG
  - /img/posts/camping/IMG_3908.JPG
  - /img/posts/camping/IMG_3911.JPG
---

porque era carnaval e melhor mesmo era se esconder no meio do mato por três dias.

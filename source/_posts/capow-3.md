---
title: "capow"
date: 2016-06-27 17:50:04
---

aqui em Lençóis está frio. frio: calça e blusa e meia pra dormir. ventanias. chuveiro nunca esquenta o suficiente. estou com livro começado e pilha pra escrever porém o cérebro não para de dar voltas. estava lendo Herbert Read e ele tem ideias tão interessantes porém às vezes tão chato. voltei a ler Robert Walser. cuido da ansiedade trabalhando com caneta nanquim num caderno de desenho.

também planto sementinhas e elas nascem e crescem e não tem mais nem espaço pra todas elas.

o suíço que cuida de mim quando a cabeça parece desses pacotes de pipoca de micro-ondas. encontrei um plugin do calibre (o programa que cuida da minha coleção de e-books) que conta a quantidade aproximada de páginas de um livro digital. a ansiedade pede leituras mais curtas.

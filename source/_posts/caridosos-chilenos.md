---
title: " caridosos chilenos"
date: 2014-11-24 16:26:14
---

o plano era se meter um pouco no shopping de La Serena e comprar umas botas novas, já que no Chile essas coisas são mais baratas e minhas botas já não têm mais sola. saímos até que cedo do hostel (tipo umas nove e meia) e seguimos o caminho da roça; digo, da praça. aí que Fernando é o maluco dos mapas e nos metemos no escritório de informação turística (que havia estado fechado no domingo), onde a mocinha nos deu um mapa, nos deu informações de hospedagem no Valle de Elqui (não usamos), nos falou sobre Coquimbo e comentou que sim, é interessante, vale a visita etc. eu não estava assim tão convencida porque vista da praia de La Serena a cidade de Coquimbo parece um amontoado de casas velhas. de qualquer forma saímos e... bueno, vamos a Coquimbo? (que era mais ou menos a forma que aparentemente tínhamos de decidir as coisas.) caminhamos até a parada de ônibus e Fernando cruzou a rua pra cobiçar qualquer coisa numa padaria.

eu fui atrás e _já tá com fome? ou é angústia?_ no que ele respondeu que era angústia e voltamos à outra calçada pra esperar o ônibus. aí que não passou muito tempo aparece do nada uma senhora que a alguns passos de distância me chama com um gesto mui suspeito, em voz baixa, como quem quer me contar um segredo. me pareceu um pouco... suspeito. perguntei o que era e ela insistindo pra que eu me aproximasse; dei um passo adiante e ela me pergunta se estamos com fome, que pode nos dar algo de dinheiro pra que compremos comida. 

... 

vinha o ônibus. agradeci (que ia fazer?) e disse que não, não se preocupe, está tudo bem, _muchas gracias_ etc. subimos no ônibus e enquanto Fernando exclamava sobre a caridosa senhora que _génia, es una génia!_ me restou dar risada e pensar que estamos bem, estamos mui bem (e eu nem estava com minhas calças com o bolso descosturando, embora não vou dizer que minhas calças estavam em seu momento mais limpo). a que ponto chegamos.

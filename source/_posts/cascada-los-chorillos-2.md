---
title: " cascada Los Chorillos"
date: 2014-09-08 18:06:56
---

(não confundir com los _chorizos_, que no caso é de comer.)

ontem: turma de umas 40 pessoas do couchsurfing, a maioria de Córdoba e arredores, e os gringos eventuais (eu ali como mais-ou-menos-gringa). encontrei a turma na rodoviária de Tanti, depois de tomar o ônibus em Carlos Paz com a Pamela, que tinha me levado antes pra conhecer Icho Cruz. nos dividimos nos carros disponíveis e tomamos o rumo pra fazer a trilha. do ponto onde se deixa o carro (la casa de Jorgito) são mais ou menos sete quilômetros até a cachoeira. nos metemos pelo caminho antigo e no meio do percurso veio o guarda-parques dizer que não podia, que tinha outra entrada e que tinha que pagar. ops. de fato informação clara não é bem o forte dessa turma do turismo em Tanti. o organizador do encontro resolveu o problema e seguimos pela trilha numa dessas descidas que te faz pensar (sofrendo) na volta, até chegar na cascada. queda de uns 100 metros de altura, água gelada, sol. deu pra ficar um tempo de bermuda. saudade de ficar de bermuda. parece que já chegou a primavera por aqui. 

ficamos até umas quatro da tarde lagarteando e batendo papo.

na volta consegui uma carona direto a Carlos Paz. e ainda consegui carona com eles até Córdoba (e deu tempo de tomar banho e arrumar as coisas). agora um pouco de internet rápida enquanto organizo o que falta organizar pra seguir pra La Rioja na terça-feira.

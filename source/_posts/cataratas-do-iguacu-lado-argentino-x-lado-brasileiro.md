---
title: "Cataratas do Iguaçu: lado argentino x lado brasileiro"
date: 2014-05-12 12:12:38
---

agora que já vi os dois lados vou deixar aqui meus pitacos sobre essa complexa discussão patriótica. essa história de que o lado argentino é melhor porque de lá se vê o lado brasileiro é mor papo furado. do lado argentino a gente vê mesmo, e muito mais, o lado argentino mesmo. do lado brasileiro a gente vê também o lado argentino, bastante dele, em panorâmica; também se vê um meio que eu não sei de que lado está, então vamos deixar por isso mesmo. os dois lados têm quatis. 

 no lado argentino se tem uma ótima visão da Garganta do Diabo, que é a primeira das quedas, e aparentemente das mais altas e potentes. isso porque, claro, é uma garganta e um monte de água se concentra ali. do mirante não se consegue ver o ponto de contato da queda d'água com a base, tanta a água em gotículas que sobem loucamente, inclusive te molhando todo.

 os outros circuitos também dão uma visão sensacional das outras quedas, menores, e não por isso menos interessantes. em alguns lugares a visão parece coisa de filme feito no computador. com sol, tem arco-íris pra todo lado. no circuito inferior, um dos pontos de mirante fica bem próximo à base de uma das quedas, e é hora de tirar a capa de chuva da mochila e se meter ali pra ficar pertinho da água (e todo molhado). 

 o lado brasileiro começa com uma panorâmica do que se vê mais de perto no lado argentino (nos circuitos superior e inferior). é incrível, claro, mas a sensação inicial é de que tudo está longe demais: o impacto inicial, sem dúvida, é menor. aos poucos, isso vai mudando: a trilha do parque no lado brasileiros vai fazendo crescer a proximidade com as quedas. desde o começo, uma "garoa" fina se faz onipresente. não é chuva não: são as cataratas. 

 em seguida se tem uma visão boa do rio depois das primeiras quedas, e de umas quedas laterais que não se podem ver no lado argentino. não estamos tão próximos, é verdade, mas já mais próximos do que no primeiro mirante, e um pouco mais molhados. com sol, arco-íris pra todos os lados! 

 í chega a vez do mirante da Garganta do Diabo. ao contrário do lado argentino, ele se faz mais por baixo, e não se chega tão perto: ainda assim, tem-se uma visão mais completa da garganta. e dá pra se molhar com a água que vem das quedas mais próximas, numa ventania sem fim.

 dito isso:

*   não sei qual foi MELHOR. o lado argentino é mais IMPACTANTE, e no brasileiro você vê mais coisa, mas mais de longe (por isso também o circuito é bem mais curto).
*   o preço (pra brasileiros) é praticamente o mesmo nos dois lados.
*   se quer levar de vez o CHOQUE das cataratas, vá primeiro ao lado argentino, direto à Garganta del Diablo; se quer fazer crescer a empolgação aos poucos até praticamente dar um abraço nas quedas d'água, melhor é começar pelo lado brasileiro.
*   vale ir nos dois lados e decidir por você mesmo qual lado é mais legal.
*   o parque argentino é maior, e você caminha bem mais; tem mais coisa pra ver, talvez porque a maior parte das quedas propriamente ditas estão do lado de lá. considerando o transporte de Foz até Puerto Iguazu, é passeio pra um dia inteiro e é melhor sair cedo (eu fui com um monte de gringo e por isso a gente teve que parar na imigração brasileira, o que fez demorar mais).
*   o parque brasileiro se faz em uma manhã; dá tempo de sair de lá e ir visitar o parque das aves do outro lado da rodovia.
*   não faça como eu que esqueci de levar o cartão de memória da câmera fotográfica (tirei e não devolvi) e só pude tirar fotos com o celular no lado brasileiro. ops.

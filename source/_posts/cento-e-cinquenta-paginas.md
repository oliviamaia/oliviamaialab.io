---
title: " cento e cinquenta páginas"
date: 2011-01-16 01:09:56
---

aí que você tem uma hora e meia pra matar numa livraria, e lê de uma sentada (literalmente, embora aqueles pufes da livraria cultura não sejam a coisa mais confortável do mundo) um livro de cento e cinquenta páginas (_O túnel_, do Ernesto Sabato), inteirinho, sem nem piscar, mesmo com aquele doido-que-lê-quadrinhos-em-voz-alta fazendo _ptzui_ e _chuuff_ ali no pufe do lado.

no que se começa a meditar sobre os efeitos negativos da POLTRONA PRÓPRIA na literatura.

---
title: "chegar na puna outra vez"
date: 2014-11-15 19:46:45
---

com o sol aparecendo pelo caminho e iluminando o topo dos morros nessa paisagem lunar de areia, cinzas vulcânicas, rochas que já estavam ali antes de se formar a cordilheira. e subindo. subimos um monte em pouco tempo, e de repente passávamos de 3 mil metros de altitude e lá fora fazia 3 graus. 

foi Néstor que nos levou ao refúgio Las Grutas numa caminhoneta e mui simpático nos foi contando sobre as paisagens e as montanhas que iam surgindo por trás dos morros mais próximos, exemplos dos _seismiles_ que são esses vulcões de mais de seis mil metros de altitude que povoam a cordilheira dos Andes nessa região entre Catamarca e o norte chileno. 

paramos em alguns pontos gelados pra tirar fotos. a temperatura subia um pouco com o sol, mas o vento era matador. depois de uma última parada e uma curva chegamos por volta das oito da manhã na aduana argentina. cem metros adiante estava o refúgio. 

precisamos esperar abrir a aduana e saímos oficialmente da Argentina, embora o Chile estivesse a mais ou menos 20 km (e a aduana chilena a uns 80), e embora na verdade continuaríamos em solo argentino e quem sabe nem sairíamos. o refúgio fica a uns 100 metros da aduana, ao lado de uma construção da _Vialidad Nacional_ (que administra o refúgio). pensar que olá estou na cordilheira dos Antes e montanhas pra todo lado que se olhava perdidas atrás de uma imensidão de puna: um solo arenoso e rochoso com essas plantinhas amarelas que comem as vicuñas, o vulcão Incahuasi gigante atrás do refúgio ao lado do San Francisco, menos imponente mas igualmente gigantesco, e entre eles na distância um pedacinho do El Muerto. a estrada seguia pela borda do San Francisco no ponto mais baixo entre as montanhas. 

estávamos a 4 mil metros de altitude. o Fernando foi só levar a mochila ao quarto que lhe mordeu a puna; desabou com dor de cabeça pra dormir quase o dia inteiro. tínhamos comida pra uns dois dias e nenhuma ideia do que ia acontecer depois disso.

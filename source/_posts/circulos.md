---
title: " círculos"
date: 2009-07-29 03:36:28
---

primeiro ano da faculdade, primeira aula de introdução aos estudos literários, primeira leitura do curso: Antonio Candido, "O direito à literatura". e agora, enfim, numa última aula de reposição do meu último semestre na graduação da Letras: Antonio Candido, "O direito à literatura". e eu não estou reclamando. na verdade me veio um sorriso quando o professor anunciou sua sugestão de texto para a última aula de literatura comparada, e eu achei que era um jeito bem redondinho de se terminar.

---
title: " coisas com as quais eu não me importo\t\t"
date: 2009-04-19 21:01:08
---

- o acordo ortográfico 
- o futuro da língua portuguesa no Brasil sem o trema 
- o futuro do livro impresso 
- o futuro da internet 
- o futuro do linux nos computadores pessoais 
- pagerank do google e viadagens semelhantes 
- a utilidade do twitter 
- quantos seguidores você ou eu ou o Edney tem no twitter 
- o que minha professora de metodologia do ensino de português diz 
- o Lúdico, o Mesmo e o Este, que devem ser todos primos

                                       ADENDO: o Lúdico deve ser um senhor grisalho com bigode, suspensórios e um nariz de palhaço, que entra nos lugares dando piruetas. não há dúvidas. abriga em sua casa o Mesmo, depois que Este -- quer dizer, o Este e o Mesmo -- foram banidos de todos os elevadores do país.

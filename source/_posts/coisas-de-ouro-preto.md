---
title: " coisas de Ouro Preto"
date: 2014-02-05 16:05:18
---

escrevi um texto no autonautas com minhas impressões sobre Ouro Preto. completo aqui que é muito estranho voltar a uma cidade que se conheceu há tanto tempo, numa excursão com a escola, quando não era necessário nenhum esforço de localização e a cidade parecia um enfileiramento de igrejas e museus. nem sempre minha memória bate com a realidade. fico me perguntando onde foi parar esse e aquele cenário. procuro vistas e não encontro. alguém andou trocando as igrejas de lugar. ou encontro algum cenário pra constatar que me lembro mais da foto tirada do que de estar ali. lembro das fachadas das igrejas porque passávamos horas desenhando aquele barroquismo sobre as portas e janelas. e lembro que era mesmo das mais barrocas que eu gostava (por isso na época Ouro Preto me pareceu tão mais interessante do que Mariana). 

a surpresa grande foi encontrar o Kevin Johansen. isso postei no autonautas (inclusive uma foto com melhor resolução do que essa do link), mas repito aqui: meu deos que inesperado. uma equipe de filmagem nos degraus da escadaria na frente da igreja (essa mesma da foto acima) e de repente perceber que aquele tipo engraçadinho de óculos escuros me parecia terrivelmente familiar. 

paf. amanhã vou dar uma volta em Mariana, de trem (porque sim), para procurar mais cenários distorcidos na memória. depois acho que no dia seguinte tomo o rumo de Tiradentes.

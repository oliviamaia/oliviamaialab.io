---
title: "coisas que eu não consigo mais fazer sem óculos"
date: 2017-02-18 10:14:03
---

- usar o computador; 
- cortar as unhas do pé; 
- pentear o cabelo; 
- raspar as pernas; 
- lavar louça; 
- regar as plantas; 
- varrer o chão; 
- andar na rua; 
- encontrar a Faísca no quintal.

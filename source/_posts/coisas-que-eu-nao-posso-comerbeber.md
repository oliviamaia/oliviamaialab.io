---
title: " coisas que eu não posso comer/beber"
date: 2009-12-03 00:47:25
---

situação temporária tratamento de gastrite:
- morango, abacaxi, laranja e frutas ácidas em geral
- bebidas alcoólicas
- mostarda ketchup molhos esquisitos similares
- café puro (socorro)
- refrigerante (não tomo)
- molho de tomate

intolerância à lactose + enxaqueca forever
- leite
- queijo
- chocolate (enxaqueca + leite)
- leite condensado doce de leite e derivados (leite!)
- pizza (queijo)
- cheeseburger (cheese)
- café (cafeína = enxaqueca)
- capuccino (café + leite + chocolate, shoot me)
- bolachas (leite, chocolate)
- pão de queijo (queijo)
- porcarias em geral (sempre leite)
- cebola crua (enxaqueca)

feliz natal pra você também.

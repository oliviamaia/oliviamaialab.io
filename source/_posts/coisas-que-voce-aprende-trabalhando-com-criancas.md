---
title: " coisas que você aprende trabalhando com crianças"
date: 2016-05-19 10:42:41
---

você jamais diz "vamos fazer uma roda". você diz "todo mundo dá as mãos, vamos ver se vocês conseguem fazer uma roda bem redondinha". 

você não diz "vamos agora até o pátio sem correr nem empurrar o colega". você diz "todo mundo faz uma fila aqui na minha frente e dá as mãos". 

você tampouco diz "formem aqui duas fileiras". você bota dois alunos um do lado do outro e manda o resto ficar atrás com a mão no ombro do coleguinha da frente.

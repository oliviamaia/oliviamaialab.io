---
title: " começo da volta"
date: 2014-11-27 11:07:51
---

opa opa. me distraí aqui em San Marcos Sierra e pulei uns dias de atualização. mas voltemos ao Chile. porque já eram quase dez dias de estar por ali, Fernando estava preocupado com a mochila que tinha deixado em Tucumán etc etc. eu tinha uma ideia de passar um dia em algum povoado do Valle de Elqui, quem sabe ir ao parque nacional dos pinguins. pensamos em fazer o checkout no dia seguinte, deixar as mochilas e passar o dia em Pisco Elqui, depois voltar e... não tinha jeito de pegar carona pra cruzar o paso de Água Negra porque, bueno, o paso estava fechado e só ia abrir no dia 26 de novembro. era dia 13 e a gente não tinha pressa mas também não era pra tanto. nos restava voltar e tentar cruzar pelo paso de San Francisco ou fazer o caminho mais óbvio que era por Santiago-Mendoza, onde tem ônibus e um monte de caminhão. mandei mensagem pro Luís. 

ele explicou que em dois ou três dias ia cruzar pelo paso del Libertador (esse que vai a Mendoza) e seguia a La Rioja e Catamarca, e que nos levava; que fôssemos à cidade de Los Andes, um pouco ao norte de Santiago, e dali nos buscava. feito. como não havia passagem direta decidimos que parávamos uma noite em Viña del Mar porque, enfim, já que estamos. dia seguinte tomamos o ônibus rumo ao começo da volta.

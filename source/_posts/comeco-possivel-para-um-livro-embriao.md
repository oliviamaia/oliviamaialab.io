---
title: " começo possível para um livro embrião"
date: 2011-10-17 20:59:29
---

>> Às vezes penso que eu podia enlouquecer. Num ímpeto de desprendimento deixar de me importar com as coisas, com as pessoas, com as tabelas e as matemáticas e deixar o mundo acontecer. Me livrar dessa tensão, essa atenção, todo tempo, desamarrado de um eterno estado de alerta. Sim: que enlouquecer fosse a solução para todos os meus problemas, porque afinal só temos problemas quando nos importamos com eles. Era; isso: enlouquecer. Ensaio alguma coisa de frente para o espelho e imagino-me falando sozinho pelas calçadas. Mas é difícil enlouquecer quando se é uma pessoa pública. Quando qualquer desvio da norma já é motivo de questionamento e especulação e deve ser o estresse da temporada etc. Fica difícil enlouquecer aos poucos quando se é uma pessoa pública; fica fácil acreditar que, sim, é o estresse da temporada, por que não? A única saída é enlouquecer de vez, sem escala, sem alguns dias de falar sozinho e esquecer quem sou onde estou quem é você para depois voltar ao normal depois de algum tempo de afastamento e repouso. Sem dar tempo de ser salvo; não quero ser salvo. Quero enlouquecer de uma vez por todas, sem volta.  

  
tão embrião que só o começo, rabisco.

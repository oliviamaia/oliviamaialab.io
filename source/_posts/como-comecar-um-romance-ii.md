---
title: " como começar um romance II"
date: 2017-02-04 09:02:03
---

> La vida es un pequeño espacio de luz entre dos nostalgias: la de lo que aún no has vivido y la de lo que ya no vas a poder vivir. Y el momento justo de la acción es tan confuso, tan resbaladizo y tan efímero que lo desperdicias mirando con aturdimiento alrededor.

Rosa Montero, _La carne_

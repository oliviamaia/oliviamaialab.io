---
title: " como começar um romance"
date: 2017-01-27 13:47:22
---

> A vida não é um romance. Pelo menos é o que você gostaria de acreditar. Roland Barthes sobe a Rue de Bièvre. O maior crítico literário do século XX tem todas as razões para estar no auge da angústia. Sua mãe, com quem mantinha relações muito proustianas, morreu. E seu curso no Collège de France, intitulado “A preparação do romance”, resultou num fracasso que dificilmente ele pode disfarçar: o ano inteiro ele terá falado para seus estudantes de haikus japoneses, de fotografia, de significantes e significados, de divertimentos pascalianos, de garçons de bar, de robes de chambre ou de lugares no auditório — de tudo, menos do romance. E vai fazer três anos que isso dura. Ele sabe, necessariamente, que o próprio curso não passa de uma manobra dilatória para adiar o momento de começar uma obra realmente literária, isto é, que faça justiça ao escritor hipersensível que cochila dentro dele e que, segundo a opinião de todos, começou a brotar em seus Fragmentos de um discurso amoroso, já então a bíblia dos menores de vinte e cinco anos. De Sainte-Beuve para Proust, está na hora de mudar e assumir o lugar que lhe cabe no panteão dos escritores. Mamãe morreu: desde O grau zero da escrita fechou-se um ciclo. Chegou a hora.

_Quem matou Roland Barthes_, Laurent Binet

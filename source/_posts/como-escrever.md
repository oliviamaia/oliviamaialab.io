---
title: " como escrever"
date: 2012-02-01 18:31:10
---

**nível 3 - intermediário**

compreendo a angústia que nos traz o ato de escrever uma obra literária. por isso, trago aos meus iluminados leitores um guia prático para escrever literatura. sigam passo a passo cada uma das instruções e tenho certeza de que em menos de cinco anos vocês terão produzido a grande obra prima da sua vida (não garanto a vida dos outros).  

1.  abra aquele arquivo de texto em que foi iniciada sua grande obra.
2.  passe alguns minutos (horas) tentando compreender o que se passava pela sua cabeça ao escrever aquele último pedaço.
3.  dê uma volta pela internet.
4.  olhe outra vez as últimas linhas do arquivo de texto.
5.  lembre-se de que precisa ligar na operadora de cartão de crédito.
6.  lembre se de que precisa ir aos correios.
7.  não faça nenhuma das duas coisas.  
    
8.  dê outra volta pela internet.
9.  volte ao arquivo.
10.  odeie-se um pouco.
11.  continue encarando o texto e odeie-se um pouco mais.
12.  mande tudo à merda.
13.  conclua que não sabe escrever.  
    
14.  escreva mais dez linhas.
15.  tenha uma ideia brilhante no momento em que precisa parar de escrever.
16.  pare de escrever.
17.  no dia seguinte, esqueça qual foi a ideia brilhante.
18.  lather, rinse, repeat.

etc.

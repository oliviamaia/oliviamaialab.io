---
title: " como se nada houvesse acontecido"
date: 2016-07-07 17:50:04
---

> People’s ability to forget what they do not want to know, to overlook what is before their eyes, was seldom put to the test better than in Germany at that time. The population decided—out of sheer panic at first—to carry on as if nothing had happened.

W. G. Sebald, _On the Natural History of Destruction_

eu queria escrever qualquer coisa sobre minhas impressões quando estive na Alemanha, sobre o peso do silêncio em algumas partes de Berlim e o peso da história esquecida nas partes mais antigas de Hamburgo. mas esse trecho já está em minha pasta de rascunhos do blog há muito tempo e eu percebi que Sebald não precisa nenhum complemento.

---
title: "como um abismo"
date: 2010-08-30 05:11:29
---

> Quando a gente morre o mundo real se _move_ um pouquinho, e isso contribui para o enjoo. É como se de repente você colocasse uns óculos de outro grau, não muito diferentes dos seus, mas distintos. E o pior é que você sabe que são seus os óculos que colocou, e não óculos errados. O mundo real se _move_um pouquinho para a direita, um pouquinho para baixo, a distância que separa você de determinado objeto muda imperceptivelmente, e a gente percebe essa mudança como um abismo, e o abismo contribui para o seu enjoo, mas também não importa.

"O retorno", Roberto Bolaño, no livro _Putas Assassinas_.

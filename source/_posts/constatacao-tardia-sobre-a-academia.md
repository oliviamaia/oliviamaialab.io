---
title: " constatação tardia sobre a academia"
date: 2016-02-27 12:44:11
---

a academia é o cabresto do pensamento. é eterna e insistente reescrita do que já foi escrito e 100 páginas analisando a análise de um texto alheio (também conhecido como dissertação de mestrado). é citar crítico citando outros autores e por aí adiante. a academia é uma biblioteca gigantesca em que todos os livros fazem referências a outros livros que fazem referências a outros livros; os originais estão -- devem estar -- guardados em algum andar subterrâneo. de qualquer forma, poucos se atrevem a lê-los. isso seria -- tremei! -- uma tese de doutorado; e ainda assim é preciso mesmo muita coragem pra escrever qualquer coisa nas humanidades que já não tenha antes sido escrita por outro nome _de peso_, sem argumento de autoridade, sem Roberto Schwarz em seu trono de livros dizendo que pode.

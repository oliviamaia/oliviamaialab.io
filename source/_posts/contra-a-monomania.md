---
title: " contra a monomania"
date: 2013-02-12 11:45:39
---

> Writing a book consists largely of avoiding distractions. (...) But utter devotion to the principle that distraction is Satan and writing is paramount can be just as poisonous as an excess of diversion. I tried to make writing my only god, and it sickened my work, for a while. The condition endemic to my generation, attention deficit disorder, gave way to its insidious Victorian foil: monomania.

via [Upside of Distraction - NYTimes.com](http://opinionator.blogs.nytimes.com/2013/02/02/upside-of-distraction/). também porque já faz um tempo comecei a desconfiar que escrever só faz sentido quando a gente para de escrever e vai dar uma volta no quarteirão.

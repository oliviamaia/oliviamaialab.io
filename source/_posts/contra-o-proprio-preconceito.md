---
title: " contra o próprio preconceito"
date: 2017-05-03 18:50:22
---

> Para mí el famoso compromiso del escritor no consiste en poner sus obras a favor de una causa (el utilitarismo panfletario es la máxima traición del oficio; la literatura es un camino de conocimiento que uno debe emprender cargado de preguntas, no de respuestas), sino en mantenerse siempre alerta contra el tópico general, contra el prejuicio propio, contra todas esas ideas heredadas y no contrastadas que se nos meten insidiosamente en la cabeza, venenosas como el cianuro, inertes como el plomo, malas ideas malas que inducen a la pereza intelectual. Para mí, escribir es una manera de pensar; y ha de ser un pensamiento lo más limpio, lo más libre, lo más riguroso posible.

Rosa Montero, _La loca de la casa_

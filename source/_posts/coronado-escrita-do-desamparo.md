---
title: " Coronado: escrita do desamparo"
date: 2010-05-12 16:24:33
---

lendo agora **Coronado**, livro de contos do Dennis Lehane, me lembrei de porque eu quero escrever como ele quando crescer. à parte dos problemas que existem nos romances -- os temas que se repetem, a estrutura esquisita, repetida, e por vezes um pouco frouxa --, o texto é sempre devastador. o que ele faz com a escrita é um exercício de abandono e desamparo: as personagens ali, abandonadas, esquecidas.  

> Half an hour later, a black Suburban pulls onto the roof and parks. Troy gets out of it. He crosss to the other men. There is talking, gesticulating, hands that point vaguely in the direction of the exit ramp, the door, the sky. In the ponting, Daniel can see their humanity, their frustrated ineffectuality, and it comforts him to realize that these are men who do, inf act, sleep in beds. Have children, possibly, mothers who still harry them, dry-cleaning tickets in their wallets.
> 
> _ICU_          

o texto passa por cima das personagens. o que é maior fica nos detalhes, na significância possível de um detalhe.  

> I see my daddy in the backyard, his face leaking, and I see my mother with that hand over her eyes, and I see the red sky I chased in that shitty truck. I see John Wayne in the jungle and LBJ in that picture and Lurlene standing up on the fridge, ballerina-like, and I see Lyle dropping that ball on the one, and those stands gone empty of fans under the black sky, and I think how it would have been nice for someone besides my dumb, drunk daddy to have told me that this is it. This is the whole deal.
> 
> _Gone Down to Corpus_            

tanto melhor quem consegue escrever o mundo com poucas palavras. de certa forma, é essa a diferença maior entre a ficção e a não-ficção. a poesia e a história, como já explicou Aristóteles: a poesia enuncia verdades gerais, enquanto a história relata fatos particulares. os fatos particulares se esgotam neles mesmos, com o peso da realidade. a ficção pode concentrar nos menores detalhes o maior de um porvir. e isso Dennis Lehane sabe fazer muito bem.

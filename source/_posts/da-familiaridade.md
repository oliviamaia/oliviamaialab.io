---
title: "da familiaridade"
date: 2014-03-22 10:08:30
---

existem pessoas que a gente conhece antes de conhecer; pessoas com quem a conversa vem fácil, carregada de uma familiaridade antiga; como se não fosse aquela a primeira conversa, o primeiro oi, tudo bem, prazer etc. gente que se adivinha os pensamentos; uma lógica de raciocínio, uma forma de estar no mundo. gente que parece conhecemos antes, em qualquer tempo cuja medida já se perdeu. que é também encontrar-se um pouco, um pedaço perdido e esquecido, e a alegria por identificar outro membro dessa família a que pertencemos.

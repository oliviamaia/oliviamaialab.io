---
title: " da importância da literatura"
date: 2013-09-10 18:30:34
---

> Smiley’s argument extends beyond the common lament that people are leading less and less and she focuses on statistics that point to only adult males reading less fiction while women and children are, in many studies, reading more than in the past. She then argues that the basic power structure of our society has not greatly changed—we are still governed, by and large, by men of affluence. Smiley makes the point that if we continued to be governed by people who are losing, increasingly, the ability to see the world from someone else’s point of view (fictions’ great gift as an art form), we are only on track to become a more selfish and less empathetic culture.

via [The Sunday Rumpus Essay: Literary Fiction’s Dilemma](http://therumpus.net/2013/08/the-sunday-rumpus-essay-literary-fictions-dilemma/) eu pensei em grifar as partes mais importantes do trecho mas logo me dei conta de que ia acabar grifando o trecho todo. ou, ainda, como bem escreveu antonio candido (vou citar de cabeça): "a literatura humaniza porque faz viver." ![old_book_library_ladder_bookshelf_books_desktop_1920x1200_wallpaper-7274](/img/2013/09/old_book_library_ladder_bookshelf_books_desktop_1920x1200_wallpaper-7274-470x293.jpg) (desculpem o título babaca, mas é o que tinha para o momento.)

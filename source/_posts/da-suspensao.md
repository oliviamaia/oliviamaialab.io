---
title: " da suspensão"
date: 2015-11-26 13:49:20
---

às vezes eu fico bem monotarefa, principalmente quando qualquer coisa grande pra além do meu controle me ocupa a cabeça, e eu estou esperando resposta alheia pra resolver questão minha. aí que essas últimas duas semanas duraram aproximadamente três décadas mas tudo indica que até segunda-feira acaba esse estado de suspensão e não saber; então terei novidades e tranquilidade, mando newsletter e fotos, essa coisa toda. (aliás: estou em São Paulo por duas semanas mais ou menos.) até lá, paciência e boas vibrações.

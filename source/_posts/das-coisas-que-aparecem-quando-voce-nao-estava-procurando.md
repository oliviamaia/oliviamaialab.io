---
title: " das coisas que aparecem quando você não estava procurando"
date: 2016-08-28 11:47:44
---

porque apareceu trabalho; mais ou menos quando eu tinha parado um pouco de procurar. talvez porque as coisas acontecem no ritmo que querem acontecer, e não no ritmo que você espera que elas aconteçam. aí que passei as últimas semanas trabalhando num curso de redação do SENAC aqui de Lençóis. cinco semanas de curso, três dias por semana. tive uma semana antes de começar o curso pra começar a preparar o material. mas valeu. entra agora a última semana de curso e respiro, escrevo no blog, termino a newsletter, cuido do jardim. também porque crises de ansiedade e outras loucuras estão também amenizadas e né, tudo isso ajuda. em algumas semanas começa tudo de novo: vou dar aulas de inglês para turismo e depois com uma semana de intervalo começa a de gramática, também em cursos do SENAC. vamos ver como segue o ritmo para enfim retomar o filhote blog, programar uns posts e torcer para que o tempo esquente porque faz muito tempo que não vou no rio.

---
title: " das coisas que (não) possuímos"
date: 2016-01-30 15:21:25
---

> It strikes me, as I look at it, that the table is way beyond my control. Up until this moment, I mean, I believed I owned that table. Now, looking at it out in the open air, I know that I don’t. I know for the first time that I maybe don’t own anything.

_The First Person_, Ali Smith.

---
title: " das leituras infinitas"
date: 2014-01-22 18:13:22
---

sabe o que é isso de sentar-se para ler, um pouco sem ânimo porque o livro parece longo e talvez um pouco aborrecido, e passar da página 20 à 90 e à 140 sem nem perceber passar o tempo, sem perceber o desconforto das pernas ou das costas; mergulhar numa leitura e parar só quando ela suscita a possibilidade de ainda outra leitura, anotar nomes e títulos para não esquecer e seguir adiante; terminar um livro e começar outro, e desse outro vislumbrar ainda outras leituras possíveis, outras bifurcações que são como uma página na internet com muitos links e a gente vai abrindo todos eles em abas paralelas para ler depois; acumular abas e abas livros e livros possibilidades infinitas de leitura para até o fim da vida; e a súbita certeza (essa certeza que já se intuía mas que se confundia com uma angústia e uma sensação terrível de pequenez, mas que então reforçada parece de repente um alento, um conforto quentinho numa tarde de inverno) enfim; a súbita certeza de nunca se estará sozinho, jamais.

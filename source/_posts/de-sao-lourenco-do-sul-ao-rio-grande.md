---
title: " de São Lourenço do Sul ao Rio Grande"
date: 2014-06-02 14:53:00
---

fiquei mais tempo do que o previsto em São Lourenço do Sul porque tomei uma mordida de cachorro (longa história resumida: cão da dona da pousada foi atropelado, já está bem apesar de uns ossos quebrados; minha mão também se recupera) e fui acolhida pela dona da pousada e do cachorro até encaminhar a recuperação (curativo, duas doses de antirrábica etc). cinco dias depois, minha mão está praticamente boa. 

no sábado peguei carona com a dona da pousada até Pelotas. ela ia ver o cãozinho na clínica e me deixou na rodoviária. peguei o ônibus pra Rio Grande que saía em menos de dez minutos (tem ônibus a cada meia hora) e rapidinho estava no aparentemente famoso Bar do Beto, onde minha anfitriã de couchsurfing (que mora no Cassino, uma espécie de bairro/distrito de Rio Grande que fica na praia) me buscou. demos uma volta pelo centro, almoçamos e voltamos pela praia, passando pela região portuária e o estaleiro; a partir do começo do que é a maior praia do mundo (e também uma estrada).

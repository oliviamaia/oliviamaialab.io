---
title: " découragez!"
date: 2008-04-30 04:22:18
---

[you're an author? me too!](http://www.nytimes.com/2008/04/27/books/review/Donadio-t.html?_r=2&amp;oref=slogin&amp;oref=slogin) pra ler, do NY Times. em inglês (bã). que devemos desencorajar novos escritores, sempre quando possível. sim?

_‘Découragez! Découragez!’_

não duvido. ninguém jamais conseguiria me desencorajar (se nem mesmo uma faculdade de letras), mas isso talvez não queira dizer nada.

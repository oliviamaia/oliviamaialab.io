---
title: " estranhas constatações"
date: 2013-07-15 15:04:59
---

eu jamais compraria um relógio amarelo, mas desde que comprei esse modelo com cinco cores de pulseira, entre elas uma amarela (gema), a pulseira que mais uso é justamente a amarela (seguida por uma de um azul [quase cian](http://pt.wikipedia.org/wiki/Azure_%28cor%29), que é para ter certeza de que às favas com os pudores cromáticos; devo ter usado a pulseira preta uma só vez).

---
title: " desses posts que eu certamente não postaria"
date: 2008-04-29 01:43:04
---

não há mais o que de novo se possa fazer com a palavra que se possa fazer sem pretencionismos. não há quem chocar (e pra que chocar?). a palavra perde, aos poucos, seu poder de dizer o oculto e alardear as consciências. não há o que ser descoberto e não há (há?) o que ser inventado. e a palavra...? que força resta à palavra, agora? que malabarismos se fazem? o que se pode dobrar, retorcer, virar do avesso, como última alternativa para conseguir do mundo algum resto de reflexão? por onde se pega um leitor que não se move (comove) ao belo ou ao feio e recebe tudo filtrado pelo que já foi, que achata, comprime, equaliza...?

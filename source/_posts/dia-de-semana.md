---
title: " dia de semana"
date: 2016-09-08 11:29:11
---

um dia como qualquer outro; de olhar na agenda e organizar as obrigações. olhar a lista com as pendências e decidir em que dia vai cada coisa -- porque essa sensação de que há muito a se fazer, e o tempo. saber que tudo isso e organizar e fazer e saber mas a vontade é buscar um pedaço de sol e ler mais um pouco esse livro que já vai pela metade. botar o andu na panela com uns temperos e deixar cozinhar. perceber que não: não há nada urgente e a semana está nos trilhos e já é quase meio-dia. perceber que o dia está livre adiante e então sentar num pedaço de sol e avançar pela outra metade do livro começado enquanto o andu continua dançando dentro d'água na panela até a hora de almoçar.

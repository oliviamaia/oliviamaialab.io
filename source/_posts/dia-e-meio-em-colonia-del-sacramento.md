---
title: " dia e meio em Colonia del Sacramento"
date: 2014-08-27 14:16:47
---

entre Montevidéu e Buenos Aires uma noite em Colonia del Sacramento, pra conhecer a cidade portuguesa-espanhola com calma. e porque portuguesa aquela familiaridade das cidades históricas brasileiras. é uma cidade pequerrucha, com um centro histórico mui lindinho e que vale a pena ficar mais de um dia, mais de uma noite. não tanto pra conhecer, que conhecer sim se conhece em um dia sem pressa, mas por curtir o lugar, sentar à beira do rio e ficar lendo. 

comer por lá é caro, mas mais uma vez eu comprava legumes no mercado e fazia qualquer coisa no hostel (que também é mais caro que Montevidéu) ou ia atrás de rotisseria. pelo centro histórico tem um monte de museu, e se pode pagar uma taxa única e visitar (quase) todos eles. também se pode subir no farol por algo como cinco reais e sim, vale a pena, é uma vista gracinha. as fotos estão publicadas no álbum! pelas ruas um turbilhão de turistas brasileiros seguindo guias em um portunhol engraçado; e eu parava quando encontrava um grupo desses, me fazia de desentendida e ficava escutando as histórias. de lá a Buenos Aires o porto tem uma pinta de aeroporto, mas é só a pinta porque para embarcar você vai percorrer um monte de corredor externo em fila até entrar no barco. compramos passagem um dia antes porque fica mais barato com antecedência, e compramos no Colonia Express porque a mocinha peruana das vendas era mais simpática. os preços das três empresas são mais ou menos os mesmos (710 pesos uruguaios; pouco mais de 71 reais pra passagem normal, que não é a mais turística e mais longa) e essa era um barco menor, um catamarã. 

pensar em ficar uma hora cruzando _o rio mais largo do mundo_ me deu uma certa agonia porque essa coisa de barco não é comigo, mas enfim alguns dramins e vamos lá.

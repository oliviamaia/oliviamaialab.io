---
title: " diário de um romance iniciado"
date: 2014-01-21 09:00:13
---

uma ideia na madrugada, fruto de uma insônia eufórica. título, epígrafes. as primeiras linhas. seis parágrafos curtos. meia página. súbito a sensação de familiaridade. uma citação num livro de Javier Marías. uma peça de Shakespeare? conheço Shakespeare por osmose, com essa sina de estudante de letras que já conhece o destino de Riobaldo e Diadorim antes de começar a ler o romance de Guimarães Rosa. por leituras transversais, tangentes. mas a familiaridade está lá. uma pesquisa e concluir que sim, Shakespeare já contou aquela história, com mais sangue do que eu pretendia contar (como convém). então a pausa na escrita para ler a peça, porque, enfim. voltas.

reincidências.

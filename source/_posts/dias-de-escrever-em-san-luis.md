---
title: " dias de escrever em san luís"
date: 2014-12-23 18:02:53
---

a estrada entre Merlo e San Luís, capital da província, é a coisa mais tediosa da história das estradas. no final fica um pouco mais interessante quando começam a aparecer umas montanhas no horizonte, nos últimos 50 quilômetros, mas até ali é uma planíce verde com eventuais ovelhas. ronc. tomei um táxi da rodoviária ao hostel e estava toda estragada da poltrona porcaria do ônibus. e com enxaqueca. viva. _y bueno_. San Luís era só pra ser uma transição entre Merlo e Mendoza, e assim foi: saquei dinheiro, comprei coisas de farmácia, aproveitei que o hostel tinha umas mesas grandes e confortáveis pra escrever newsletter, atualizar o blog e fazer backups que desde maio eu não fazia backup das minhas fotos, vai vendo.

a cidade também não convida muito pra passear. perto tem uns lugares mui lindos como Potrero de los Funes e Trapiche, com diques e montanhas e caminhadas, mas não se pode ver tudo e às vezes a gente precisa ser um pouco ermitão. se eu for visitar todos os lugares lindos por perto de onde passo eu vou enlouquecer. então dei uma volta pelo centro, entrei na catedral, passei pelas ruas de pedestre e... voltei pro hostel pra escrever. 

na segunda-feira saí pra almoçar um sanduba de lomo num restaurante boteco e... voltei pro hostel pra escrever.

ainda assim nunca é tempo suficiente pra escrever tudo o que eu quero escrever. eu precisava de uma semana à toa sem sair fazendo trilha por aí. não sei se consigo. resultado é que sempre tenho mil coisas pendentes. já tinha comprado a passagem pra Mendoza pro dia 23 pela manhã(zinha); também já feita a reserva em quarto privado de hostel até o dia 3 de janeiro, pra passar as festas com a mama. o ano de 2014 praticamente encerrado e o ano que vem uma incógnita gigante.

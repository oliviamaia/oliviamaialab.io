---
title: do estado atual da internet
date: 2022-02-03 11:43:17
---

eu lembro quando busca na internet era um negócio horrível.

eu lembro quando busca na internet virou um troço mágico e eficiente.

eu lembro quando os resultados de uma busca na internet eram relevantes: só tinha que aprender a falar a língua dos algoritmos.

eu lembro quando os professores falavam de como era preciso filtrar a informação na internet pra encontrar as fontes confiáveis.

eu lembro quando a gente ainda encontrava fontes confiáveis na internet.

vai fazer qualquer busca séria agora: pilhas de sites genéricos repetindo a mesma informação genérica e outros tantos de blogs do dr. fulano que furiosamente nega toda a informação genérica disponível numa argumentação vazia.

nada.

---
title: " do they have a choice?"
date: 2016-12-23 10:32:46
---

> I am often reminded of a scene in a film about Africa. A lion attacks a herd of zebras and kills one. The frightened zebras run for a while and then they stop and start to graze again. Do they have a choice?

Isaac Bashevis Singer, "Cafeteria"

---
title: " dois livros que me inspiraram e acompanharam quando escrevi TRÉGUA"
date: 2018-05-15 11:21:51
---

![](/img/posts/31739407_227209024706760_5643886240135643136_n.jpg) 

**1.** _62 modelo para armar_, do Julio Cortázar: fiz referência a ele na epígrafe, com um trecho do capítulo 62 de Jogo da Amarelinha. não sei exatamente por quê esse livro me marcou tanto, na verdade. é um livro bem maluco. mas deu uma luz: uma forma possível de escrever; os personagens que se trombam pra lá e pra cá, sem saber aonde estão indo. o ritmo do texto. aliás, bônus: o livro Os prêmios, também do Cortázar, que eu estava lendo quando comecei a escrever TRÉGUA. começava a ler e de repente tinha que parar de ler pra escrever. se não saía a escrita, bastava ler mais um pouquinho. nenhuma relação de temas, diga-se de passagem, mas funcionava. 

![](/img/posts/32359135_481490718932038_4425792785815175168_n.jpg) a primeira página de _62 modelo para armar_.

![](/img/posts/31433936_193091014662403_3270885692057583616_n.jpg) 

**2.** _Fever Pitch_ (Febre de bola), do Nick Hornby: existe talvez esse sentimento que só quem esteve no estádio de futebol vendo seu time jogar pode entender. talvez seja um exagero. mas é um sentimento que não se desfaz, não desaparece. esse livro do Hornby, nesse sentido, foi um espelho necessário. desastres à parte, existe uma mágica. esse livro é um manifesto de amor ao futebol. recomendo também a quem nunca entendeu qual a graça de um bando de marmanjo correndo atrás de uma bola. 

![](/img/posts/30830061_230995247481678_4267555680845561856_n.jpg) trechinho do livro.\[/caption\] 

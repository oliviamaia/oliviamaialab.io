---
title: " down down down. would the fall never come to an end?"
date: 2009-03-30 04:32:48
---

o mundo inteiro de repente convergindo, as palavras se ordenando para apreender o inapreensível. tudo. tudo o possível, feito uma casa distante em uma colina esquecida, trancada por fora, a chave perdida, e todo o mundo que um dia existiu ali dentro, todo o mundo que ainda existe. tudo. o espaço todo aberto, e o gesto desesperado de se buscar em que se agarrar, em um chão ou. talvez deixar-se cair, deixar-se ultrapassar a fronteira, caindo muito e caindo sempre.

---
title: " e eu com isso?"
date: 2019-04-26 10:58:36
---

talvez eu devesse estar mais preocupada com a crise política brasileira, a crise política mundial, as incertezas do futuro e o aquecimento global. lógico. mas é que não consigo abandonar a questão do tal _surveillance capitalism_ e tudo que vem junto disso. e convenhamos que todas as questões anteriores acabam enroladas nisso também.

nosso futuro distópico é logicamente mais parecido com _Admirável mundo novo_ do que com _1984_. não que autoritarismo seja coisa do passado; a questão é que autoritarismo deixou de ser a opção mais eficiente de dominar a população: só funciona quando as massas têm fome, e lhes são negados direitos básicos; funciona quando _as massas_ são pessoas que fariam de tudo por... enfim, um pouco de dignidade. de resto, autoritarismo é perda de tempo.

não: nosso futuro distópico é um em que não existem mais cidadãos. existem apenas _consumidores_﻿. nossos direitos como cidadãos se misturam aos nossos direitos como consumidores. mesmo a linguagem da cidadania se mistura à linguagem do consumo.

a internet, em algum momento perdido no passado não tão distante, deixou de ser um espaço de cidadania e se transformou num espaço de consumo. estamos sendo vigiados, claro, mas não é por nenhum motivo orwelliano. somos vigiados _pro seu próprio bem_. pra _personalizar sua experiência_. pra que você veja publicidade que é _relevante pra você_. e a gente deixa, também. mais artigos relevantes, mais vídeos relacionados com meus interesses na lista de vídeos sugeridos _pra você_.

no começo o conceito de experiência personalizada me parecia bastante prático. você descobre mais sites, mais pessoas, mais gente pra seguir no instagram. mas devo ser uma péssima usuária de redes sociais, pelo menos do ponto de vista do algoritmo que deduz personalidades, porque não demorou pra que todas essas personalizações perdessem completamente a relevância pra mim. de repente, tudo que o pocket me oferecia como sugestão, por exemplo, eram textos do tipo _5 coisas que as pessoas bem-sucedidas fazem durante a pausa para o almoço_ e _Como transformar estresse e ansiedade em produtividade_ (esse último é real).

quer dizer, talvez eu tenha uma cabeça meio esquisita, mas digamos que a mágica da timeline não funciona comigo. se olho duas atualizações no feed do facebook, fico logo entediada e vou fazer outra coisa. não porque eu não queira saber o que está acontecendo na vida dos meus amigos: é que não consigo me interessar pelo _formato_﻿. quando eu tenho que fazer uma atualização no facebook pra divulgar um projeto ou convidar as pessoas pra assinar a newsletter, me sinto toda errada. _quem lê essas coisas? quem clica nessas coisas? será que não existe outra solução?﻿_

cada publicação, cada hashtag, cada clique: tudo isso é usado pra definir o que você gosta, o que você faz, o que você compra. se você tem amigos que gostam de andar de bicicleta, talvez você também goste. ou, pelo menos, pode se sentir tentada a comprar uma bicicleta pra acompanhar aqueles amigos. não?

parece bem inofensivo: saber que você gosta de correr e tentar te vender um novo modelo de tênis com amortecimento especial. saber que você tem uma compulsão por gastar dinheiro quando tem liquidação de objetos de decoração. saber que você é alcoólatra e te oferecer... um uísque especial.

inofensivo.

quem não tem nada a esconder não deveria ter nada a temer etc. mas a gente não precisa ter cometido um crime pra não querer um parente chato lendo nossas mensagens do celular, por exemplo.

e essa nem é a questão.

a questão é o poder que você dá pra uma empresa. uma empresa que, como eu já escrevi em algum lugar (e como a gente bem sabe), funciona pra fazer dinheiro (por melhor que sejam suas intenções, se não fizer dinheiro, não sobrevive). e publicidade não é só pra te vender coisas. publicidade também vende ponto de vista, vende partido político, vende ideias. e com as estratégias desse _surveillance capitalism_, vende pras pessoas certas: vende pras pessoas que são mais suscetíveis (mais vulneráveis?) a comprar o que está sendo anunciado.

as eleições nos Estados Unidos foi só um exemplo do tipo de coisa bizarra que esse sistema pode gerar. a polarização política e os extremismos improdutivos que estão tanto lá como aqui é outra consequência de um mundo em que a gente é bombardeado (principalmente no facebook, claro, mas pra muita gente o facebook _é a internet_) com ideias com as quais a gente já concordava antes, e principalmente com as reações indignadas ao que há de pior no ponto de vista oposto (gente olha que absurdo esse imbecil que disso isso e aquilo etc).

e quem decide tudo isso? uma empresa? meia dúzia de empresas? um algoritmo? uma lógica voltada ao que "dá mais lucro" ou "mais resultado"? resultado pra quem?

eis o nosso futuro distópico. de que estamos reclamando? somos consumidores. temos o direito de simplesmente não comprar uma ideia que não nos interessa. temos o direito de não clicar em publicidade. temos todo o direito de sair do facebook, por exemplo. e ir morar nas montanhas.

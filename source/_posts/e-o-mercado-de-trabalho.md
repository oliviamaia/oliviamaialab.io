---
title: " e o mercado de trabalho?"
date: 2009-09-29 18:59:25
---

outro dia falei no colégio onde trabalho sobre o curso de letras. os alunos, do 2º ano do ensino médio, queriam saber qual era o mercado de trabalho.

          a pergunta a ser feita.

                                [trovões ressoando na distância.]

o que eu não podia dizer era que se alguém faz letras pela literatura, ou mesmo pelos estudos linguísticos, é porque está inevitavelmente fugindo do mercado de trabalho. existem cursos que formam profissionais. letras não é um deles. letras não é profissão. letras é carreira.

mercado de trabalho pra quem fez letras? além da docência? o mercado editorial. tradução. trabalho em editoras. jornalismo, talvez, agora. sim?

sim? mas -- pensei, depois, em casa -- não se precisa de um curso de letras pra trabalhar como revisor. não é isso que a gente aprende na faculdade de letras. tanta literatura pra revisar porcaria alheia? precisa um diploma em letras pra formatar texto alheio?

letras é carreira. repito: o curso de letras não forma profissional pra coisa nenhuma. pretenderia formar pesquisadores e potenciais professores. porque a docência, de certa forma, se encaixa talvez entre uma coisa e outra, mas necessariamente parte do mesmo princípio que norteia a carreira, e não a profissão. alguém que faz letras (ou é escritor, já que era disso que eu estava falando antes) pode, por exemplo, escrever sobre literatura pra uma revista ou jornal. mas isso exige que ele tenha construído uma carreira como pesquisador ou escritor. antes.

fugir do mercado de trabalho, pra alguns, é necessário. pra mim, é. fiz letras, escrevo e quero trabalhar com pesquisa ou ser professora, porque, como disse o professor com quem estou fazendo estágio: "prefiro fazer isso do que trabalhar num banco." e não?

e ouvi dos alunos: _eu queria fazer letras, mas não queria ser professor!_

claro. parece mais inverossímil ouvir: _eu queria fazer medicina, mas não queria ser médico!_ pelo tempo investido e exigido, acho que só uma meia dúzia de perdidos chegaria a essa conclusão. mas letras não é um curso que forma profissionais, e por isso ele atrai e assusta pelo mesmo motivo: é o curso, pelo curso. para o curso. pesquisa. como é química, física, matemática, com a diferença que esses tipos de curso podem permitir que a pesquisa seja voltada para uma questão prática, e, logicamente, voltada para o mercado de trabalho.

mas é pesquisa. é carreira.

tem gente que mira logo uma profissão. se jogam para o mercado de trabalho. eu desconfio sempre que muitos desses não sabem bem o que querem, ou têm medo da instabilidade financeira (ou porque já vivem em instabilidade financeira, que me parece um motivo justo).

  
ou.  
escreveu o Mirisola: Guimarães Rosa era um escritor que fazia bico no Itamaraty. e eu acredito nisso. a gente precisa às vezes de uma profissão pra sustentar a carreira. que é uma dura verdade.

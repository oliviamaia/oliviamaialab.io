---
title: " e palavras"
date: 2009-12-08 18:25:35
---

curioso que uma pomba que manca na avenida Pacaembu seja dessas cenas despropositadas que despertam palavras, e não tanto uma descrição mas súbito um amontoado de palavras escapando pelos cantos, saindo pelos bueiros da avenida (por onde já vi entrar um ratão de vinte centímetros, diga-se de passagem). e então interromper o treino na academia porque as palavras, e na recepção um pedaço de papel e uma caneta e agora e palavras, que nem tanto a pomba que manca e nem tanto por uma descrição e a patinha vermelha que devia doer e não tocava o chão mas a culpa toda da bendita pomba que manca.

---
title: " e que saiba abrir a porta para ir brincar"
date: 2010-10-20 15:09:25
---

há certos escritores que se lê como se escreve. que o cérebro registra a leitura como se escreve, como se reproduzisse em eco o texto alheio e se apropriasse dele. desses o prazer maior da leitura, dessa construção em conjunto, de pertencer a qualquer coisa externa que ao mesmo tempo também é você, também é algo seu, transformado seu.

o texto acaba mas jamais acaba, e continua pela ponta do lápis, pelos dedos no teclado em energia renovada e a certeza que sim, tudo já foi dito e tudo já foi escrito, mas há esse eco e por ele continua-se escrevendo e registrando e dizendo, milhares de pontos de vista sobre um mesmo objeto, sobre nosso único objeto, tudo sempre reflexos de um existente para além daquilo que jamais poderia ser pronunciado.

---
title: " a ilusão do real"
date: 2016-02-21 12:44:11
---

> It is not a good idea to interrupt the narrative too often, since storytelling works by lulling the reader or listener into a dreamlike state in which the time and space of the real world fade away, superseded by the time and space of the fiction. Breaking into the dream draws attention to the constructedness of the story, and plays havoc with the realist illusion. However, unless certain scenes are skipped over we will be here all afternoon. The skips are not part of the text, they are part of the performance.

_Elizabeth Costello_, J. M. Coetzee

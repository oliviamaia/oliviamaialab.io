---
title: " entre antofagasta de la sierra e fiambalá"
date: 2014-11-13 05:34:27
---

com os franceses nas 4x4. tinha espaço, cabia eu, cabia minha mochila. pensei que sim, queria ir ao vulcão Galán a maior cratera do mundo (argentino tem umas manias de o maior, o mais largo, o mais comprido), mas também não ia ter tão fácil outra chance de ir de Antofagasta a Fiambalá pelo caminho alternativo, pelas paisagens mais bonitas. então sete da manhã estava esperando os franceses na hostería municipal. saí na frente com um dos mais aceleradinhos, que tinha lugar na caminhoneta. esqueci o nome dele. ops. ele saiu logo depois das motos e fomos nos comunicando com um inglês macarrônico com muitos substantivos e verbos no infinitivo. ele apesar de curtir velocidade ia parando quando eu queria tirar foto então por mim estava tudo belezinha. e também que muitas vezes eu ficava olhando a paisagem com a boca aberta e esquecia de tirar foto, e só me dava conta porque o lábio começava a doer de seco. logo saímos da estrada e nos metemos num campo desolado de terra e areia com algo de marca de rodas no caminho, mas não muito. um vulcão se aproximava com seu gigantesco campo de lava petrificada; tanto se aproximou que nos metemos nesse entorno negro e pedregoso e fizemos toda a volta pelo campo de lava até sair do outro lado num salar esburacado e duro.

aí mais areia, ou cinzas vulcânicas, ou os dois, até chegar no famigerado campo de pedra pomes, gigantesco. me dei conta que era afinal o campo esbranquiçado que lá do vulcão Antofagasta se via na distância.

ao fundo umas montanhas cobertas de areia e que dali pareciam montanhas abandonadas, cheias de pó, esquecidas.

aí começou a complicação. e a subida. três mil e oitocentos, três mil e novecentos, quatro mil metros de altitude... e subindo. o carro foi cansando e a areia foi afofando. teve um momento ali que pareceu que a gente ia ficar ali parado no meio do deserto. mas tudo bem: vitória do 4x4. continuamos subindo enquanto o campo de pedra pomes se estreitava e se tornava mais esparso, e à direita uma montanha que mais parecia uma pintura, ou um cenário de ficção científica desenhado em história em quadrinhos. como eu posso descrever isso?

não parece uma montanha. não parece nada. parece coisa da cabeça de alguém com muita imaginação e tempo livre. a montanha se mostrou inteira quando alcançamos o ponto mais alto da travessia: quatro mil trezentos e pico metros de altitude; descemos do carro, tiramos fotos, voamos com o vento forte.

então voltamos pro carro e começamos a descer. pouco a pouco o cenário ia mudar pra algo mais... parecido com o planeta Terra. 

pouco a pouco. 

e depois pedras, vegetação, terra. Houston, we have landed. nos metemos numa encosta de uma montanha gigantesca e sem fim, com a cordilheira ao fundo, multicolorida e impossível. a estradinha estreita e o precipício. fomos devagar até alcançar um riachinho e parar pra almoçar. 

fiquei pensando que, putz, a puna, os vulcões, as paisagens incríveis. e no final o que a gente precisa pra viver é um riachinho, um vale com vegetação em volta. parecia que tinha acabado a descida mas depois de uma curva, a descida continuou até que alcançamos um povoado metido no meio do nada (do NADA). umas pessoas até meteram as caras pra fora em plena hora de siesta pra ver que era que estava acontecendo que tinha um carro passando pela estrada. 

aí a última parte interminável da travessia: a quebrada entre esse povoado impossível e a estrada principal, pelo leito do rio, que já estava ameaçando crescer, e que em alguns pontos me pareceu bem crescido. o cenário, mais terráqueo, me lembrou bastante Iruya e San Isidro. já estávamos a menos de dois mil metros de altitude e um calor violento. 

estrada enfim alcançada, seguimos até Palo Blanco onde era o ponto de encontro e... esperamos.

Fiambalá estava já uns 30 km de distância mas a dúvida era se iam ou não pelas dunas (as dunas mais altas do mundo etc), porque o tempo já estava mei avançado. o grupo chegou uns minutos depois e ficaram lá falando um monte de francês enquanto eu não entendia nada. conclusão metade do grupo ia pelas dunas e metade passava por Fiambalá (o destino deles era Cortaderas, já no rumo do Paso de San Francisco). troquei de caminhoneta porque meu piloto ia pelas dunas e não passaria por Fiambalá pra economizar tempo e segui com o grupo que tomaria a estrada normal. foi se aproximar de Fiambalá e um vento violento tipo tempestade de areia. 

Fiambalá está metida entre montanhas, aos pés da cordilheira dos Andes e feita oásis no meio de um enorme deserto de areia, com dunas e muito vento. pararam todos no posto de gasolina e nesse ponto juntei minhas mochilas e me despedi da turma. a praça de Fiambalá estava a três quadras de distância e me empurrou o vento (e a areia).

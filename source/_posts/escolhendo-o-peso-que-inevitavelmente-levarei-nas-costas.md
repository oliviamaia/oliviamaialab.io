---
title: " escolhendo o peso que inevitavelmente levarei nas costas"
date: 2013-07-20 15:06:16
---

porque ainda não encontrei um e-reader que me agrade, porque sou terrivelmente teimosa, porque gosto de sofrer. de qualquer forma é um sofrimento ainda maior fazer essa escolha, seja pelas minhas costas, seja pelos livros que ficam. (esses da foto são uma seleção preliminar para a primeira parte da viagem, para antes de passar por são paulo mais uma vez.)

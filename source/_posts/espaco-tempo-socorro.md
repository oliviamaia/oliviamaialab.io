---
title: " espaço-tempo socorro"
date: 2009-09-23 22:43:51
---

o problema de fazer a revisão do _segunda mão_, que é literatura policial, é se lembrar se isso ou aquilo acontece antes ou depois do momento em que estou na revisão. NENHUMA noção de tempo dentro do meu próprio romance. sem contar que eu preciso sempre conferir no resumo/plano em que dia da semana eles estão. porque, né, quarta-feira tem jogo de futebol, etc.

[aliás. quarta-feira tem jogo de futebol. etc. hip hip hip.]

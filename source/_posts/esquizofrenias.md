---
title: " esquizofrenias"
date: 2010-04-07 00:44:26
---

> (...) em seu segundo livro ele descreve um dispositivo mais perfeito, que pode ser usado enquanto ele se desloca: trata-se de um estetoscópio nos ouvidos ligado a um gravador portátil no qual pode recolocar os tubos ou retirá-los, baixar ou aumentar o volume, ou permutar com a leitura de uma revista estrangeira. (...) Se é exato que montou esse dispositivo já em 1976, bem antes do surgimento do _walkman_, podemos considerá-lo seu verdadeiro inventor, como ele o diz, e pela primeira vez na história uma bricolagem esquizofrênica está na origem de um aparelho que se espalhará por todo o planeta e que, por sua vez, esquizofrenizará povos e gerações inteiras.

_Deleuze_, "Louis Wolfson, ou o procedimento", em _Crítica e clínica_.

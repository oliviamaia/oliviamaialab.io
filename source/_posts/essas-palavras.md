---
title: " essas palavras"
date: 2009-12-13 03:45:52
---

> Eu as deixo dizer, minhas palavras, que não são minhas, eu, essas palavras, essa palavra que elas dizem, mas que dizem em vão. Sou QuaseWatt e em minha vida só houve três coisas: a impossibilidade de escrever, a possibilidade de fazê-lo e a solidão, física, evidentemente, que é com a qual agora sigo adiante.

_Bartleby e companhia_, Enrique Vila-Matas

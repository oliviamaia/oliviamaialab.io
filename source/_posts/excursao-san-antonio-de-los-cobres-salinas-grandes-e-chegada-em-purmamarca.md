---
title: " excursão: san antonio de los cobres, salinas grandes e chegada em purmamarca"
date: 2014-10-24 16:09:58
---

mais uma vez: wooosh. saí de San Lorenzo um fim de dia, parei num hostel em Salta pra uma noite e fui atrás da excursão. reservei no lugar em que a moça me tinha sido mais simpática, já que os preços são praticamente os mesmos e alguns mais baratos são um pouco suspeitos. no dia seguinte a excursão saía às sete da manhã (começam essa hora a buscar a turma nas hospedagens) e como me buscaram primeiro fiquei com um lugar na frente eba eba. nada mal porque muitas curvas. milhões de curvas.

porque San Antonio de los Cobres está a 3700m de altitude e aí que, né, tem que subir muita montanha no rumo à puna, esse espaço de altura entre a pré-cordilheira e a cordilheira que ocupa uma área enorme de Catamarca, Salta e Jujuy. por aí ficam um monte de salinas (nenhuma delas como Uyuni, na Bolívia, mas ok) e alguns povoados cinzentos de pessoas cinzentas com a pele maltratada pelo clima. o caminho que é lindo. porque o micro-ônibus se mete primeiro na quebrada del Toro, seguindo o caminho do trem, e quebrada é sempre esse vale extremo com um rio metido entre paredões ou encostas muito inclinadas cheias de _cardones_. 

os _cardones_, no caso, que existem até mais ou menos 3500m de altitude, e aí quando desaparecem a gente sabe que chegou à puna.

San Antonio, enfim, um _pueblo de puna_. plano, cinzento, frio, seco, meio triste. mulheres que te metem miniaturas de lhama na cara dizendo _llama llama llama_. almoçamos aí e eu provei uma empanada de carne de lhama. depois ainda comprei dois alfajores de um peso. 

entre San Antonio e as Salinas Grandes uma interminável estrada de terra e pedra (_rípio_) com direito a pneu estourado e parada pra troca com vista às salinas ao fundo. aí que as salinas mesmo, bueno, é lindo e tudo, mas acho que eu já fiquei marcada por Uyuni e não achei grande coisa. é uma parada de trinta minutos e bora seguir pela estrada. 

passamos pelo ponto mais alto do percurso, a 4200m de altitude, com parada pra foto e vaquinhas ao fundo (não eram lhamas dessa vez, ou eram lhamas disfarçadas de vaquinhas), e depois de algumas curvas uma última parada na estrada antes de começar a descer a _cuesta de Lipán_ que: wooosh. linda de ver do alto e infinita pra descer em zigue-zague. embora também: linda pra ver enquanto descendo em zigue-zague. como tem montanha nesses lados da Argentina. 

a última “reta” do caminho de muitas cores até chegar a Purmamarca, lar do _cerro de los siete colores_ e meu destino final. um povoadinho de uma dúzia de ruas metido entre montanhas coloridas por todos os lados (por TODOS OS LADOS) com casas de adobe e tudo (menos os morros) numa mesma cor de terra. pra fazer contraste, na praça, tem umas dezenas de banquinhas de roupas e panos muito muito coloridos. deixei a turma da excursão e fui procurar hospedagem.

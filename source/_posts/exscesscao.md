---
title: " exscessção"
date: 2009-05-11 17:51:01
---

> aluno de letras não pode cometer certos erros de gramática e ortografia. e eu não estou falando da palavra exceção, porque a ortografia da palavra exceção muda a cada semana, tem vez que é com x, tem vez que é com ss, tem vez que é com ss, x e ç ao mesmo tempo.
> 
> _professor Antonio Dimas_, Lit. Brasileira V

---
title: " filosofia de pasta de dente"
date: 2012-11-28 15:26:29
---

perceber que muito melhor é apertar o tubo de pasta de dente bem no meio, porque sempre mais interessante esse não saber quando se vai pôr pasta na escova e talvez seja necessário repensar toda a logística da coisa porque aparentemente acabou a pasta, mas aí apertar um pouco no final e descobrir que ainda há muita pasta, e essa alegriazinha estúpida de não precisar abrir outra embalagem ou vestir os chinelos para ir até a farmácia, e mais ainda essa alegriazinha estúpida quase toda semana, que só não é maior do que a alegriazinha de se pegar o tubo de pasta de dente e apertá-lo com a mão cheia bem no meio feito a gente ainda soubesse ser criança e pouco importasse o pragmatismo e a ordem.

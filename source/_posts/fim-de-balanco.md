---
title: " fim de balanço"
date: 2015-09-28 14:10:14
---

computadores com sistemas atualizados, backups feitos, fotos em processo de edição e seleção. agora arrumar as mochilas e escolher o que vai de primeira, o que fica numa mala esperando o momento em que a gente esteja parado num lugar pra minha mãe despachar na nossa direção. já fiz e desfiz mala tantas vezes nesses últimos dois anos e ainda assim parece que é a primeira vez. amanhã um voo pra Fortaleza e uma semana mais ou menos na casa do pai antes de seguir. (não pergunte pra onde. não sabemos.) que não é tanto o que assusta, o não saber. talvez um cansaço genérico depois de dois anos e uns quebrados de viajar, de não parar. e parar alguns dias faz pensar que não é má ideia, parar um pouco. mas, por ora, esse é o plano: buscar um lugar pra parar. pelo menos até aquele famigerado _travel bug_ se manifestar outra vez e fazer coçar o pé.

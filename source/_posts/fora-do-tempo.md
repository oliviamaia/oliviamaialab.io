---
title: " fora do tempo"
date: 2009-08-05 03:17:40
---

> ... Mas o que importa não é que os alvos ideais sejam ou não atingíveis concretamente na sua sonhada integridade. O essencial é que nos disponhamos a agir como se pudéssemos alcançá-los, porque isso pode impedir ou ao menos atenuar o afloramento do que há de pior em nós e em nossa sociedade.

do discurso do Antonio Candido, [quando recebeu o prêmio Juca Pato de intelectual do ano](http://terramagazine.terra.com.br/interna/0,,OI3118896-EI6581,00.html#), pela UBE. [o penúltimo parágrafo é pra ler três vezes.] _como se pudéssemos alcançá-los_ é, de certa forma, a contribuição da literatura na vida da gente. ou uma prova irrefutável da necessidade da arte no coração das pessoas.

---
title: " Guimarães Rosa me salvou de fazer publicidade"
date: 2016-09-13 11:29:11
---

numa troca de e-mails sobre a necessidade de escrever um amigo me passou esse link: [poema inédito de Angélica Freitas em homenagem a Ana Cristina Cesar](http://www1.folha.uol.com.br/serafina/2016/07/1785342-leia-poema-inedito-de-angelica-freitas-em-homenagem-a-ana-cristina-cesar.shtml). no que fiquei depois pensando que Guimarães Rosa _me salvou de fazer publicidade_. publicidade, esse caminho do artista cínico e pragmático, a saída dos criativos desesperados. mas as possibilidades da arte, a beleza do texto, de repente, paf. por dizer: existe um caminho sensato. já pensou?, publicidade! até o começo do último ano de colégio me parecia o único trabalho criativo com o qual eu não morreria de fome. mas aí Guimarães Rosa e aquele professor do terceiro ano e eu voltei a rabiscar cadernos com histórias mágicas sobre crianças que encontravam fantasmas em espelhos no porão da fazenda da avó ou que iam parar num circo de 1930 quando se perdiam dos pais num passeio pelo parque. assumir o _vamos ver se_ e _quem sabe_. Guimarães Rosa me fez acreditar que criatividade nada tinha a ver com pragmatismo.

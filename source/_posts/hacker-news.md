---
title: hacker news
date: 2022-03-09 10:15:05
---

primeira definição [no Urban Dictionary](https://www.urbandictionary.com/define.php?term=Hacker%20News): 

> A website where over enthusiastic programmers and entrepreneurs ("hackers", as they call it) gather and rediscover old ideas thinking they are novel and congratulate each other in the process. They have "curious" conversations, which is a euphemism for a contest of who can use the most convoluted sequence of words to describe terrible ideas that go nowhere. They share articles and news stories with each other, with the added flavor of grandiose commentary. There is ritual of babysitting new users and reminding them to be gentle and not commit heinous acts such as challenging their terrible ideas.

eu não costumo ler os comentários no hacker news, mas tenho usado um aplicativo no telefone que abre os comentários por padrão. aí:

![screenshot do aplicativo](/img/posts/Screenshot_20220302-141200.png)

parece adequado.

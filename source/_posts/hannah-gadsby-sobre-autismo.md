---
title: hannah gadsby sobre autismo
date: 2022-03-23 08:56:23
---

> I can be cold and not know it. I can be hungry and not know it. I can need to go to the bathroom and not know it. I can be sad and not know it. I can feel distressed and not know it. I can be unsafe and not know it. You know how sometimes you put your hand under running water and for a brief moment you don’t know if it is hot or cold? That is every minute of my life. Being perpetually potentially unsafe is a great recipe for anxiety. And – spoiler alert – anxiety is bad.

([daqui](https://www.theguardian.com/stage/2022/mar/19/hannah-gadsby-autism-diagnosis-little-out-of-whack).)

quer dizer. sim. mais ou menos isso.

um pouco também dar sentido a essa irritação que eu sempre senti quando alguém me pergunta o que eu quero, ou se estou me sentindo "melhor", se está tudo bem. não saber se estou cansada ou triste ou com fome. é tudo parte de um desconforto vago que só faz bagunçar as ideias e deixa crescer uma irritação sem foco.

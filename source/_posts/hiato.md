---
title: " hiato"
date: 2008-09-06 02:43:39
---

que me parece de repente é esgotamento de temas, quando o personagem não se basta, sozinho, naquele vazio que é só existir. sempre um vazio. tudo me vem como repetição de qualquer outra coisa, minha ou dos outros.

---
title: " in the dark and deep"
date: 2009-03-20 17:36:55
---

> A autópsia informou _that he died of unknown causes_, sentença a que acrescentei para mim: _in the dark and deep part of the night_.

W.G. Sebald, _Os anéis de Saturno_

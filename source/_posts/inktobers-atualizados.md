---
title: página de inktobers reformulada e atualizada
date: 2020-11-05 10:15:40
---

refiz e atualizei a [página de inktobers](/arte/inktober) no site e adicionei uma página com as [ilustrações de 2020](/arte/inktober/2020).

os inktobers não estão disponíveis em alta resolução porque são mais exercício do que qualquer outra coisa, mas se algum lhe interessar e quiser a arte digitalizada em alta resolução é só me pedir que eu envio (sem custo nenhum). por outro lado estão todos disponíveis na [página de originais disponíveis](/arte/originais) se quiser pedir o seu.

e mais uma vez não esquece de espiar minha [página de apoio recorrente no catarse](https://catarse.me/oliviamaia).

ufa ufa. missões de outubro concluídas e finalizadas. que venha o fim de ano que 2020 já vai tarde.

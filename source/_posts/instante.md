---
title: " instante"
date: 2010-12-24 21:25:00
---

> E quando lentamente soavam os toques do meio-dia e ribombava o canhão do forte Saint-Elme, um sentimento de plenitude, não um sentimento de felicidade, mas um sentimento de presença real e total, como se todas as fissuras do ser estivessem obstruídas, apoderava-se de mim e de tudo o que estava à muita volta. De todos os lados afluíam torrentes de luz e de alegria que, de fonte em fonte, caíam para se condensar num oceano sem limites. Neste momento (o único), eu me aceitava pela única adesão de meus pés ao solo, de meus olhos à luz. E no mesmo instante sobre todas as margens do Mediterrâneo, do alto de todos os terraços de Palermo, de Ravello, de Ragusa e de Amalfi, de Argel e de Alexandria, de Patras, de Istambul, de Esmirna e de Barcelona, milhares de homens estavam como eu, retendo seu fôlego e dizendo: Sim. E eu pensava que se o mundo sensível não fosse senão um leve tecido de aparências, um véu de quimeras volúveis, que à noite nós rasgamos e que nossa dor tenta inutilmente apagar, existem contudo homens, os primeiros a sofrer com isso, que refazem esse véu, reconstroem estas aparências e fazem retomar a vida universal que, sem este arrebatamento cotidiano, cessaria em algum lugar como uma fonte perdida no campo.

do conto "As ilhas afortunadas", no livro _As ilhas_, de Jean Grenier.

o fim do conto é melhor ainda, mas quem quiser que vá ler, que o livro é curtinho (tem mais introdução e prefácio que livro) e um troço muito lindo.

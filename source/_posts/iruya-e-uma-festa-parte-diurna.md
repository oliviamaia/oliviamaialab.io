---
title: " iruya é uma festa, parte diurna"
date: 2014-10-27 08:00:43
---

_em que eu me desperto às oito e minha companheira de quarto nem voltou_ manhã de silêncio e estar um pouco sozinha. acordei com uma dor monstro no joelho direito e dores musculares por todo o corpo. comi a maçã que tinha sobrado do dia anterior e fui caminhar um pouco, subi no mirante, escrevi, voltei pro quarto, li um pouco. desci mais uma vez à outra praça, comprei danoninho e oreo e fiz um _desayuno completo_. o rapaz do armazém ainda me deu uma colherzinha de sorvete pra comer o danoninho mas eu acabei usando as bolachas de colher porque _colher comestível_ é sempre mais interessante. comecei a achar que Juan e Barbara tinham se metido n’alguma quebrada pela noite e vai saber quando se tem amigos que se metem a escalar ilhas no meio do rio quase seco. toquei pra hospedagem em que estava Juan e perguntei por ele. numa janela apareceu Barbara, que não quis subir o morro às quatro da manhã e ficou com uma cama por lá mesmo, que era mais perto e mais pra baixo. enquanto ela enfim subia e Juan se preparava pra acordar, e o _intendente_ fazia outro discurso na praça e o bispo anunciava a missa das onze, fui ver se chegava a francesa num dos ônibus que vinha de Humahuaca, porque combinamos de nos encontrar mas zero internet. muito provável (como depois confirmamos) que ela nem sabia o nome do lugar em que estávamos. 

do segundo ônibus veio Marie e compartilhamos um pouco de folha de coca pra enfrentar a subida impossível àquela hora da manhã. antes do almoço estava o grupo reunido pra comprar coisas de comer e seguir ao rio, dessa vez sem escalar nada. 

mais friozinho e mais vento. depois do almoço e de estar à toa me meti adiante numa quebradinha mui linda pra confirmar que qualquer lado que se olhe existe uma paisagem incrível. deu pra meditar e fazer siesta, mas depois de um tempo vou cansando de brigar com ponta de pedra pra conseguir sentar confortavelmente. 

voltei com Marie e a ideia era seguir direto à hospedagem porque meu joelho doía demais, mas paramos na praça depois de comprar água saborizada de pomelo e ficamos vendo os meninos e meninas da escola dançando folclore. 

compramos números da rifa mas não ganhamos o chapéu nem a faixa e frustradas subimos a ladeira pra enfrentar a última parte da festa de Iruya.

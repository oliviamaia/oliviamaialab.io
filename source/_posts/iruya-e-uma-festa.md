---
title: " iruya é uma festa"
date: 2014-10-26 08:03:20
---

ou vira uma festa; porque era fim de semana do dia da padroeira _v__irgen del Rosário_, e vem gente de todos os povoados vizinhos, e montam a feira no leito do rio, e turistas de toda parte etc. missas e procissões e apresentação de folclore. 

chegamos e tentamos descobrir como se conectar à internet pra avisar ao mundo que estávamos vivas e essa coisa toda que a gente cisma em fazer hoje em dia como se alguém fosse dar por nossa falta em alguns dias sem comunicação. mas zero internet por todos os lados e por sorte o sinal normal de telefone da Barbara funcionava porque o meu nem isso. conseguiu mandar mensagem pro Juan e nos encontramos na igreja. juntos almoçamos e fomos visitar a tal da feira metida no rio, que segundo uma senhora de San Isidro (que Juan conheceu no dia anterior) eram bolivianos mas segundo _doña_ Asunta era gente de todo lado e povoados vizinhos que vinham pra festa e acampavam por lá. metade da feira era comida muito frita muito gordurosa de procedência duvidosa e a outra metade roupas e porcarias de todo tipo. a turma arma a barraca com metade pra servir de exposição de mercadoria e a outra metade pra servir de quarto, uma coisa ao lado da outra. 

tocamos pela quebrada no sentido do rio e fizemos uma siesta numa sombra de uma quebradinha que deve ser a saída de algum _arroyo_ seco. tempo matado seguimos adiante, nos metemos a escalar uma das ilhas do rio e fizemos de umas pedras um tobogã mei perigoso. eu por exemplo me lanhei toda.

ainda adiante rio abaixo pelas pedras se alcança um encontro do rio San Isidro (que vem de San Isidro) e o rio Iruya (em que estávamos). à esquerda o San Isidro é um pouco mais caudaloso mas nem tanto; o suficiente pra gente poder chegar, tirar as botas e matar mais um tanto de tempo com os pés na água. à direita o rio segue por outra quebrada num percurso que só encontra outro povoado com pelo menos um dia de caminhada.

a vista são essas montanhas enormes que se erguem nas laterais, de cores variadas e variantes, cheias de pontas. que incríveis são essas montanhas cheias de pontas. voltamos quando o sol se escondeu atrás de uma delas; ainda tinha pelo menos uma hora pra que fosse embora de verdade e parasse de iluminar o dia. voltamos pelo caminho de cima, passando por casinhas de adobe e senhoras de xale e chapéu levando o burro de carga. 

em algumas horas começava a festa que era procissão e serenata à virgem.

---
title: " julho"
date: 2008-07-02 18:16:25
---

Bolas de neve. Fico pensando em bolas de neve, caricaturas de avalanches em desenhos animados e seu uso exagerado em expressão de fala. Sempre preferi a imagem do efeito dominó porque as coisas desabam e tudo se acaba mas de forma tão ordenada e bela.

Fico pensando em bolas de neve porque nunca vi nada do gênero e me pergunto como seria estar presente em uma avalanche.

Não gosto do frio, mas bem que podia nevar. Daria algum propósito a esse exagero de agasalhos e cobertas e café quente. Nem isso.

Em São Paulo o clima é incerto, quatro estações em uma semana. E meus professores de geografia tentando dizer que no sudeste do país só existiam duas estações bem definidas. Nada é muito definido nessa cidade.

Mas o frio. Não gosto. Há uma certa melancolia em rostos pálidos e aquele céu branco de nuvens adormecidas depois que caiu a chuva.

Como se o mundo estivesse pronto para acabar.

[do meu "_Que os mortos enterrem_".]

---
title: " lembrete"
date: 2015-07-05 06:42:53
---

nessa onda de calor que tem feito sofrer os suíços mais sensíveis, antes que a gente pudesse reclamar da temperatura: uns minutos de papo com a baiana numa das banquinhas de bebida do festival latino "caliente!" que tá rolando aqui em zurique, e já lembramos por que a gente quer voltar logo e encontrar um lugarzinho pela costa do nordeste pra viver.

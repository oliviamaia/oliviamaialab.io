---
title: " literatura policial costumava ser engraçada"
date: 2010-06-08 22:05:11
---

> You could argue that crime is crime, and shouldn't be funny - but didn't it start out with a good and smart sense of humour? Holmes and Watson were a double act, Agatha Christie's Miss Marple and Dorothy L Sayers's Lord Peter Wimsey always played it for subtle laughs, and on the other side of the pond when "hardboiled" or "noir" fiction kicked in with Chandler and Hammett in the 20s and 30s, the one-liners flew faster than the bullets.

[artigo interessante no Guardian](http://www.guardian.co.uk/books/booksblog/2010/jun/03/colin-bateman-crime-funny-books) \[em inglês\] sobre o que diabos aconteceu com a literatura policial que de repente perdeu toda a graça. ainda bem que ainda temos Andrea Camilleri com o seu comissário mais rabugento da Sicília.

---
title: " livro imaginado"
date: 2010-11-26 14:56:53
---

esse livro imaginado de vidas que se enroscam para além do esperado desejado possível. confusão de nomes e pessoas que passam sem pedir licença ou acenar um até logo. a trama feito professor substituto que inventa matéria pra preencher os 50 minutos da hora-aula ou uma espécie de cola ruim que une vidas em torno de uma sequência quando as palavras sintagmas não sabem escapar ao tipo mais primevo da linearidade. trama policialesca ou nem tanto, também um disfarce, ilusionista de carreira que sabe nos fazer olhar para outros lados enquanto realiza o truque em plena vista dos que não se deixam enganar por cores e o movimento das mãos. escrever sem saber o final sem saber o meio sem saber reação deles personagens. essa ideia vaga, duas ou três cenas momentos e eles personagens que já começam sem permissão a andar conversar pensar sonhar dentro da minha cabeça, eles ainda sem nome, ainda sem rosto, eles um gesto um sorriso uma frase uma cidade natal um recorte de passado um desencontro.

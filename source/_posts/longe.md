---
title: " longe"
date: 2011-04-15 12:53:04
---

> Conhece-te a ti mesmo...É fácil dizê-lo e inclusive acreditar nisso; depois, nos momentos de ruptura, de implosão, de queda em si mesmo, o que se descobre é outra coisa. Cebolas infinitas, não acabaremos nunca de retirar as camadas que nos envolvem, desde os sete véus de Salomé até a prodigiosa espeleologia da psicanálise; embaixo, sempre mais embaixo, o centro recusa-se deixar-se ver tal como é. Estamos longe de muitas coisas, mas de nada estamos mais longe que de nós mesmos.

Julio Cortázar (sempre), em _Nicarágua tão violentamente doce_

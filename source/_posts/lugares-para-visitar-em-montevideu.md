---
title: " lugares para visitar em Montevidéu"
date: 2014-08-26 14:04:34
---

o que eu mais gostava de fazer em Montevidéu era ir atrás de uma praça ou parque (e tem bastante) pra ficar à toa lendo. a cidade é boa pra isso: tem o parque Rodó, que é bom pra ver o pôr do sol, e o parque del Prado, que fica mais longe do centro mas nada que um ônibus não resolva, que é uma lindezinha. sem contar o Jardim Botânico, do lado do parque do Prado. dá pra passar toda uma tarde ali à toa ou caminhando ou lendo ou desenhando ou escrevendo. fui durante a semana e imagino que fim de semana deve encher de gente.

em volta do estádio do centenário também tem um parque grande e bom de passear, ainda que também um pouco mais afastado do centro (fui caminhando e cansei).

depois disso caminhar, que era o que eu mais fazia. caminhar pela rambla toda é ótimo, mas te prepara que a rambla cobre toda a parte sul da cidade e é enorme. às vezes rola um vento violento. aliás, Montevidéu venta. o bairro sul que é antigo vale a pena recorrer, subir a rua Durazno, por exemplo, e todos os arredores. umas casinhas hermosas. parar em qualquer kiosko e comprar um alfajor.

feira de rua tem todos os dias, como tem em São Paulo, mas como a cidade é mais curta você sempre encontra uma feira em algum lugar. e tem verdura e frutas mas também tem queijo e outras coisinhas. a maior é a feira de Tristam Narvaja, perto da avenida 18 de Julio, que dá voltas e voltas por outras ruas e tem antiquidades, mate, artesanato e todo tipo de coisa. essa rua Tristam Narvaja, aliás, é uma rua cheia de sebos, de livrarias, uma coisa assustadora. nem entrem em nenhuma que era pra não ficar tentada, já que minha mochila vai nas minhas costas e minhas costas pobrezinhas não querem saber de livro. o centro velho na verdade não achei grande coisa; tem uma feirinha de artesanato no sábado, um monte de restaurante pra turista e o mercado do porto, que basicamente é um monte de restaurante pra turista. quem vai pra cidade no espírito restaurante pra turista tem que ir lá, sem dúvida. eu fiquei com um lugar na ponta do mercado que vende empanadas (fritas) mui baratas e ricas. aí sim recomendo.

outra que vale a pena é subir o prédio da intendência. você retira o ingresso numa casinha de informação turística ali em frente e sobe o elevador panorâmico. é de graça; ali em cima se vê toda a cidade, e tem toda a informação das construções e da história. 

do turismo brasileiro típico, que é comer e comprar, não posso dizer muito para além das feiras e da busca por empanadas boas e baratas. posso dizer que o tal do chivito é um sanduba de carne e grande coisa um sanduba de carne. no meu caso a boa era procurar as rotisserias que vendem comida semipronta, e tem umas tortas de verdura baratinhas e teoricamente saudáveis.

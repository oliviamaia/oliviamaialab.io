---
title: " Maigret e o fantasma"
date: 2010-05-26 20:29:12
---

é o penúltimo livro com o comissário Maigret publicado pela editora L&PM, e também um dos últimos que Simenon escreveu (é de 1963). há nele uma espécie de remanescência de todos os livros anteriores, de uma continuidade a qual não se pode escapar.

gostei demais do começo -- o começar pelo fim de um caso anterior, o tom de melancolia e uma descritividade que parece meio avessa ao estilo do Simenon. depois logo vem o dia e esse ritmo se dispersa, e voltamos ao território conhecido. a investigação toda acontece num intervalo de um dia, pra terminar quando chega a madrugada, e mais uma vez a continuidade marcada pelo caso anterior, mencionado nas primeiras páginas.

um inspetor meio azarado é vítima de um atentado, e tudo indica que estava investigando o colecionador holandês que morava no prédio em frente. o holandês parece um tipo mui correto e tudo, mas esconde alguma coisa. claro que Maigret fica cismado sem saber muito bem por quê. nada parece se encaixar. e pra quem lê o mistério continua até as últimas páginas, até as últimas horas.

vai pra minha lista de preferidos do Simenon.

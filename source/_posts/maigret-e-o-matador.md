---
title: " Maigret e o matador"
date: 2010-05-20 00:36:07
---

esse um dos últimos a ser publicado pela L&PM, dessa leva de inéditos. um jovem com uma mania estranha de gravar conversas alheias é assassinato numa noite de chuva. o gravador não foi mexido, e é a partir das últimas conversas gravadas que começam as investigações. há uma conversa mui suspeita, uma quadrilha de assaltantes de quadros... e Maigret acha que tem alguma coisa muito errada e esse bando de profissionais não parece o tipo que sai por aí matando a facadas qualquer um que apareça no caminho.

me veio qualquer semelhança com outro livro do Simenon, acho que _O homem que via o trem passar_, mas minha memória escapa. isso porque as cartas ao jornal. dessa vez em outra perspectiva: nesse outro livro, é o protagonista que envia cartas "de esclarecimento" aos jornais. em _Maigret e o matador_, é o matador, desconhecido ao leitor. aí impossível não pensar no _Demônio da perversidade_, do Poe. tem qualquer coisa nesse meio campo que atraía terrivelmente o Simenon, e por consequência perturbava o inspetor Maigret. por tentar compreender o que se mostra incompreensível. explicar pelo incompreensível. 

e o que continua me encantando é que mesmo na repetição de motivos/temas (enfim!) as histórias são sempre -- ainda -- novas. num vício de literatura policial americana com número de páginas padrão, estrutura padrão, conflitos padrão etc, esses livros do Simenon, de umas boas décadas atrás, são sempre novos. não acaba nunca.

  [e saiu esse mês mais um inédito: _[Maigret e os colegas americanos](http://www.lpm-editores.com.br/site/default.asp?Template=../livros/layout_produto.asp&CategoriaID=372905&ID=538063)_. vai colocando na lista.]

---
title: " mais do que um leitor"
date: 2017-05-07 20:51:42
---

> El oficio literario es de lo más paradójico: es verdad que escribes en primer lugar para ti mismo, para el lector que llevas dentro, o porque no lo puedes remediar, porque eres incapaz de soportar la vida sin entretenerla con fantasías; pero, al mismo tiempo, necesitas de manera indispensable que te lean; y no un solo lector, por muy exquisito e inteligente que éste sea, por mucho que confíes en su criterio, sino más personas, muchas más, a decir verdad muchísimas más, una nutrida horda, porque nuestra hambruna de lectores es una avidez profunda que nunca se sacia, una exigencia sin límites que roza la locura y que siempre me ha parecido de lo más curiosa.

Rosa Montero, _La loca de la casa_

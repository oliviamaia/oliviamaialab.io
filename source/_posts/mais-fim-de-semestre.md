---
title: " mais fim de semestre"
date: 2010-06-11 22:04:55
---

enquanto dou um pique pra terminar o trabalho de fim de semestre, aproveito o intervalo pra começar um esboço de lista das piores coisas do mundo:

2a. pior coisa do mundo: ressonância magnética do joelho.

**pior coisa do mundo**: ressonância magnética do punho.

![Media_httpimg687image_xaden](/img/2012/05/media_httpimg687image_xaden-scaled500.jpg?w=300)

bem vindo ao inferno.

[OK, talvez eu esteja exagerando. na verdade, a **PIOR** coisa de todas é [isso aqui](http://pt.wikipedia.org/wiki/Pun%C3%A7%C3%A3o_lombar). imagina uma **CANETA BIC** sendo enfiada na sua coluna. imaginou? pois é. é mais ou menos assim.]

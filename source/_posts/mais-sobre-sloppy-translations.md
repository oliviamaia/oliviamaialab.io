---
title: " mais sobre sloppy translations"
date: 2010-05-16 21:24:09
---

dessa vez com uma pitada de fúria.

acho certo um tradutor inglês-português ter bom conhecimento do inglês. mas melhor ainda seria um bom conhecimento do português.

será que as editoras não percebem? será que não há um revisor competente pro que existe de literatura de entretenimento traduzida? será que os bons revisores só trabalham com a "literatura de verdade"? será que um livro de entretenimento merece uma tradução que preposiciona o sujeito como se isso fosse a coisa mais normal do universo?

(vá dizer que é a fala do personagem: "hora dele chegar". não convence quando esse mesmo personagem, adiante, usa a regência corretinha do verbo "ir", por exemplo: "vou à padaria". convenhamos. coerência.)

e, sério, eu não culparia os tradutores. ninguém está livre de erros. mas existem revisores pra isso. é preciso dar ao texto coerência, consistência. a literatura de entretenimento não merece esse descaso só porque oferece uma "leitura rápida", e solta logo esse livro que precisamos te enfiar o próximo lançamento.

ooft.

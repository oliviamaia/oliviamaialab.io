---
title: " mais um domingo"
date: 2015-07-05 12:39:27
---

acordar dez da manhã depois de ir dormir passado meia-noite -- que é pra compensar as horas de sono perdido por ter acordado cinco da manhã no sábado pra visitar a parte italiana da Suíça --; café da manhã reforçado e de bicicleta até o centro de escalada do outro lado do morro a umas duas ou três vilas de distância parando pra tirar foto dum campode girassois; uma hora e meia de escalada e tomar o rumo do lago, encontrar um ponto cheio de argentinos e portugueses e a água quentinha; um pão recheado com chocolate e um mergulho com os patos e os cisnes enormes que te olham desconfiados; a volta que uma subida interminável e enfim a ladeira final cruzar o trilho de trem e estar de volta, e o marcador de temperatura gritando 40° ali antes da curva, e ainda são cinco da tarde e vai ter hambúrguer vegetariano pra fechar a tarde.

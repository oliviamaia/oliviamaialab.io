---
title: " mais uma atualização: internet del pueblo"
date: 2014-10-16 08:19:56
---

ops que ess não é mais um relato de viagem daqueles que eu quero programar. mas era pra dizer que finalmente tenho internet, ainda que não muito rápida e ainda que não no quarto: estou num povoadinho chamado Angastaco entre Molinos e Cafayate, aqui pelo meio dos valles Calchaquíes, e tem wifi grátis da municipalidade rolando por todos os lados. mais: estou numa _hostería_ top (top Angastaco) com café da manhã e quarto privado ao lado da praça no coração do povoado por menos de 30 real (90 pesos). retomarei minhas pendências internéticas e seguirei com minhas pendências computadorísticas (trabalhinho de revisão) aqui sentada na enorme varanda da _hostería_ escutando os passarinhos e vendo a gente passar.

---
title: " mais uma vez um começo"
date: 2013-05-30 22:52:18
---

comecei a escrever meu próximo livro e de repente me saíram quase duas páginas. não sei se presta, ainda mais porque o narrador me veio com

> Juro que não queria começar a contar essa história desse jeito.

e se eu achava que ainda sabia muito pouco sobre esses personagens, descobri logo que esse Bernardo é um tipo muito enrolado. se não tomar cuidado vão dez páginas sem que a história tenha começado de fato. isso que dá jogador de futebol metido a literato que com certeza leu mais Machado do que deveria. eu e meus narradores verborrágicos. os outros dois Tarciso e Emília por enquanto estão discutindo na distância e não se sabe muito bem por quê.

---
title: máquina atualizada
categories: "blog"
---

porque já que a depressão não me deixa fazer arte nem escrever eu vou é instalar um novo sistema operacional e passar três dias configurando todos os mínimos detalhes.

[![screenshot do terminal com resultado do comando neofetch](/img/neofetch2.png)](/img/neofetch2.png)

troquei o atom como editor de código pelo vscodium (versão sem telemetria do vscode). porque enfim, foda-se. vscode é microsoft mas atom é github e github hoje é... microsoft. até tentei me acostumar ao geany mas nele faltam algumas frescuras essenciais (visualização de cores a partir dos códigos HEX por exemplo).

enfim. máquina tá voando com esse manjaro edição openbox. tudo lindo.

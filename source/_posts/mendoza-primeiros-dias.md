---
title: " mendoza: primeiros dias"
date: 2014-12-26 09:33:53
---

então o ônibus pra Mendoza às oito da manhã e umas quatro horas de viagem por mais uma planície infinita até que ao fundo me aparecem umas nuvens bem estranhas no horizonte e eu percebi que não eram nuvens, e sim a cordilheira dos Andes com os picos todos nevados. acho que identifiquei o Aconcagua.

daí chegar, tomar um táxi, instalar-se no hostel. fui atrás de alguma coisa ora comer e me meti no mercado central pra uns pedaços de pizza e um paso de los toros. junkie pouco. por aí já era o centro da cidade e uma bagunça de compras de natal na última hora. socorro. a cidade seria simpática, com bastante árvores etc; se eu gostasse de cidade. já não sirvo pra cidade. encontrei a praça Independencia e segui pro escritório de informação turística pra descobrir como chegar no aeroporto de ônibus. a mocinha que me ajudou ao final me passou a informação do ônibus e outras miles de informações sobre Mendoza, San Rafael, Potrerillos, Maipú, Malargüe etc etc. saí de lá com um monte de folhetos e mapas. e o número do ônibus que tinha que tomar pra buscar a mama às 19h45 no aeroporto El Plumerillo. ao final saí do hostel quase duas horas antes do horário que tinha que estar lá e cheguei 19h35. o avião já tinha pousado cinco minutos antes mas minha mãe foi a última a sair.

y bueno. que dizer de cidade grande? em véspera de natal? no dia 24 fugimos no final do dia pra um parque perto do hostel depois de passear um pouco pela manhã. à noite teve festa de natal do hostel com muito vinho incluído e... bueno, não tanta carne. nunca tem carne suficiente. mas vinho, né, e no 25 tudo que eu pensava em fazer era dormir. nos juntamos com uma dinamarquesa e fomos ao parque San Martin levando na mochila o que tinha sobrado de uma pizza do dia 23.

e dormitamos no gramado. estou tão desacostumada com isso de cidade que fico meio perdida. tem bastante coisa pra fazer e lugares próximos pra visitar e trechos históricos da cidade pra conhecer mas fico mei bicho do mato sem pilha pra sair. quero ir ao parque do Aconcagua (tem que acordar cinco da madrugada e tomar um ônibus às seis) e uns povoados próximos mais tranquilos. dá pra ocupar os dias.

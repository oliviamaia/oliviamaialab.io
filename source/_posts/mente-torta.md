---
title: " mente torta"
date: 2009-01-09 17:36:50
---

eu acho muito estranha a minha incapacidade de usar qualquer tipo de agenda ou calendário no computador, ou mesmo uma agenda assim de mão, que eu esqueço sempre de abrir, e o melhor mesmo a se fazer é sempre ter um calendário que fique o tempo todo me encarando sobre a mesa e anotar lá mesmo o que houver para ser feito.

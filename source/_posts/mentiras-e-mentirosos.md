---
title: " mentiras e mentirosos"
date: 2014-01-19 14:06:30
---

como romancista, Alberto Manguel é um ótimo crítico literário (eu diria que o mesmo acontece com o Piglia, mas eu nunca consegui ler nenhum romance dele para além do primeiro capítulo). o que não impede que esse romance seja interessante, dentro desse lugar meio híbrido que ele ocupa, um romance com uma tese, a narrativa como argumento.

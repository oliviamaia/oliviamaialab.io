---
title: " mineirices"
date: 2014-02-14 19:36:42
---

interjeições em mineirês de acordo com o nível de mineirice (e suas respectivas traduções para o paulistês)

**6.** uai! → ué  
**5.** é mess? → sério?  
**4.** uai, sô! → ô, meu!  
**3.** noss! → nossa!  
**2.** nó! → nossa senhora!  
**1.** nu! → minha nossa senhora!  

favor encaminhar outras interjeições e/ou traduções, bem como o ranking no nível de mineirice, nos comentários deste post.

---
title: " minha família em valle hermoso"
date: 2014-12-12 14:37:06
---

não vou dizer que eu fugi de San Marcos Sierras mas é bem verdade que saí levando toda aquela raiva desnecessária no caminho pela _ruta 38_ até a parada do cemitério de Valle Hermoso. mas aí um abraço na amiga Dani e estava mais uma vez em casa, entre amigos, entre algo que eu posso chamar de família. fiquei o fim de semana escrevendo alguma coisa mas principalmente organizando as ideias e aproveitando as boas companhias.

no dia 3 quarta-feira era o aniversário de três anos da Emília e alonguei um pouco a estadia pra estar por ali. um dia antes fui caminhando até La Falda e comprei uma camiseta com uma zebra com botas de _regalito_. na verdade quando se viaja vamos ganhando famílias por todos os lados, e às vezes é uma família de hostel que se forma em dois dias. amizade de mochileiro etc. que é porque estamos todos na mesma situação. mas aqui por Córdoba tenho essa famílias de mais tempo que vão permanecer por mais tempo ainda e vão me fazer continuar voltando pra lá (e quem sabe vir viver pra esses lados). em dois dias já estava dissolvido o ódio e eu estava bem pronta pra Mina Clavero, minha próxima parada, no sul da província, no rumo de San Luís e Mendoza nesse começo de empreitada ao sul argentino.

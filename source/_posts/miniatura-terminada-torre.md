---
title: 'miniatura terminada: torre'
date: 2020-05-25 09:53:50
---

miniatura terminada! estou bem apaixonada por esse martelo minúsculo que eu consequi fazer.

![foto da torre 1](/img/min_torre/img_7314_IMGP.jpg)

![foto da torre 7](/img/min_torre/img_7320_IMGP.jpg)

![foto da torre 2](/img/min_torre/img_7315_IMGP.jpg)

![foto da torre 3](/img/min_torre/img_7316_IMGP.jpg)

![foto da torre 4](/img/min_torre/img_7317_IMGP.jpg)

![foto da torre 5](/img/min_torre/img_7318_IMGP.jpg)

![foto da torre 6](/img/min_torre/img_7319_IMGP.jpg)

![foto da torre 8](/img/min_torre/img_7321_IMGP.jpg)

logo devo começar um novo projeto. vai ter streaming no [twitch](https://twitch.tv/shadnyex) então fica de olho.

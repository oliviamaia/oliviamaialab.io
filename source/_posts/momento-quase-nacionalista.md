---
title: " momento quase nacionalista"
date: 2009-03-11 02:26:45
---

[e porque os mais espertos sabem compreender que não é nada disso. socorro.]

> Comparada às grandes, a nossa literatura é pobre e fraca. Mas é ela, não outra, que nos exprime. Se não for amada, não revelará a sua mensagem; e se não a amarmos, ninguém o fará por nós.
> 
> Antonio Candido, prefácio à 1ª edição da _Formação da Literatura Brasileira_

maior o grande conhecedor de cultura estrangeira quanto mais brasileiro for. isso, sim, é descolonizar-se. [prof. Joaquim Aguiar, departamento de teoria literária e literatura comparada da usp.] e que se discorde com civilidade.

---
title: narrativa como busca
date: 2022-03-11 10:24:49
---

> Other books—some of mine, I hope—are instead trying to map the surrounding territory and understand where we are. That is, such books are not linear, not built around a single chronology—in fact, they are often not structured around chronology at all. But just as a mushroom hunter is neither lost nor without purpose, these books are not without structure or direction. Why not understand by analogy, decenter the narrative, seek patterns of resemblance in parallel, explore the terrain rather than cutting a swathe through it? Why not meander and see what lies alongside? Such books are concerned not so much with what happens but with what it means; they are less about destination as resolution, and more about meaning revealed along the way.

-- [In Praise of the Meander: Rebecca Solnit on Letting Nonfiction Narrative Find Its Own Way](https://lithub.com/in-praise-of-the-meander-rebecca-solnit-on-letting-nonfiction-narrative-find-its-own-way/)

que é pouco pra lembrar por que eu escrevo, por que eu quero continuar escrevendo, por que eu não vou saber parar de escrever nunca.

bônus: o final do artigo; me peguei rindo sozinha.

aproveitei e comecei também a ler Orwell's Roses e vou dizer que nunca me interessei por rosas ou flores de um modo geral, mas o livro é mais um desses passeios da Rebecca Solnit que te levam junto sem você nem perceber o que está acontecendo.

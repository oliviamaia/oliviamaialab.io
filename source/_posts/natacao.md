---
title: " natação"
date: 2011-04-10 00:19:00
---

_De fato, a psicanálise e a literatura têm muito a ver com a natação. A psicanálise é em certo sentido uma arte da natação, uma arte de manter à tona no mar da linguagem pessoas que estão sempre fazendo força para afundar. E um artista é aquele que nunca sabe se vai poder nadar: pôde nadar antes, mas não sabe se vai poder nadar da próxima vez que entrar na linguagem._

Ricardo Piglia  
"Os sujeitos trágicos (literatura e psicanálise)"

no livro _Formas breves_
  
porque aí que terminei de escrever o livro que estava escrevendo  
                           e vai se chamar TRÉGUA, afinal

    e tenho outro que preciso escrever (o livro que ganhou o edital da petrobras) ainda que duas outras ideias para dois outros livros andem me rodeando, e fico nesse momento rondando a água, arriscando molhar o pé ainda sem muita certeza de que não vou me afundar no momento em que me atirar outra vez para voltar a nadar.

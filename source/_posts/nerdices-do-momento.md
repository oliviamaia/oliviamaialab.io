---
title: " nerdices do momento"
date: 2018-02-26 16:44:57
---

minha grande piração do momento é [isso aqui](https://en.wikipedia.org/wiki/Nothing_to_hide_argument), que resume um pouco o tipo de lógica que tem ocupado minha cabeça nas últimas semanas. e de carona também tudo que é [privacidade na internet](https://www.privacytools.io/), e essa loucura de entregar a vida pra meia dúzia de _empresas_ cujo objetivo maior é te fazer CLICAR EM PROPAGANDA.

é impressionante o quanto a internet pode se tornar um lugar esquisito quando você começa a prestar atenção nessas coisas. mas também é bem interessante descobrir o mundo online que existe pra além do rastreamento de hábitos e perfis de consumo.

a internet dentro do facebook e do instagram e do twitter é tão pequena.

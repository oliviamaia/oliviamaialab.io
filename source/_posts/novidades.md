---
title: algumas novidades
date: 2020-09-12 08:57:59
---

algumas das principais novidades dos últimos dias aqui no site:

- [página com originais disponíveis](/arte/originais)
- [novas ilustrações](/arte/rabiscos)
- [página /src atualizada](/src)
- [faq atualizado](/faq)
- [versão simplificada do site em inglês](/en) pra compartilhar cos amigos gringos

em breve:

- uma campanha recorrente no catarse pra quem quiser e puder apoiar meu trabalho pra eu continuar disponibilizando arte livre e gratuita pra todo mundo.
- um podcast com episódios curtinhos feito de pequenas alegrias literárias

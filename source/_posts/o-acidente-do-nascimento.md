---
title: " o acidente do nascimento"
date: 2016-04-23 10:48:29
---

> When one paused to think of who one might have been had the accident of birth not happened precisely as it did, then, well, one could be quite frankly appalled

Alexander McCall Smith, _Portuguese Irregular Verbs_

---
title: " o avesso e o direito"
date: 2014-03-12 20:38:43
---

(em inglês porque foi a edição que encontrei e de qualquer forma eu não sei francês.)

> From time to time I meet people who live among riches I cannot even imagine. I still have to make an effort to realize that others can feel envious of such wealth. A long time ago, I once lived a whole week luxuriating in all the goods of this world: we slept without a roof, on a beach, I lived on fruit, and spent half my days alone in the water. I learned something then that has always made me react to the signs of comfort or of a well-appointed house with irony, impatience, and sometimes anger.
> 
> [...]
> 
> Although I live without worrying about tomorrow now, and therefore count myself among the privileged, I don’t know how to own things. What I do have, which always comes to me without my asking for it, I can’t seem to keep. Less from extravagance, I think, than from another kind of parsimony: I cling like a miser to the freedom that disappears as soon as there is an excess of things. For me, the greatest luxury has always coincided with a certain bareness.
> 
> em "The Wrong Side and The Right Side", de Albert Camus.

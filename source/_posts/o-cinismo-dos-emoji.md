---
title: " o cinismo dos emoji"
date: 2018-02-28 14:54:05
---

porque tão mais fácil responder com um sorriso quando na verdade você está fazendo uma careta de desgosto e na verdade não está com vontade de entrar no mérito da discussão.

ou: não ter o que dizer em resposta a uma mensagem sentimental de parente e devolver um beijinho no coração.

e só posso concluir que emojis foram inventados por um de nós, seres socialmente incompetentes que nunca sabem o que dizer quando alguém diz que _vá com deus_ ou _manda lembranças pra tua mãe_ ou _que seu ano seja cheio de luz﻿_ etc.

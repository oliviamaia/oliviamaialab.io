---
title: " o dia de ontem no retiro Tao Tien"
date: 2014-03-08 12:44:28
---

um dia de trabalho wwoofer no [retiro Tao Tien](http://www.taotien.com.br): preparar o café da manhã, catar folhas secas, separar espinafres colhidos na horta, preparar o almoço, fazer geleia, meditar na pedra no ponto mais alto do sítio e ver o sol se pôr atrás das nuvens adivinhando nelas imagens e leões que cospem fogo. 

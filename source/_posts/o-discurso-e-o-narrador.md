---
title: " o discurso e o narrador"
date: 2009-06-04 22:03:26
---

e de insistências minhas com o discurso, e a necessidade de se montar e estruturar uma forma condizente com o que se pretende, deixei passar a minha própria falha, nesse sentido, de não perceber que o incômodo e descompasso entre o objetivo e o resultado no texto se dava justamente pelo descompasso entre discurso e objeto. a forma era o descompasso. isso desconfiava, nessa leitura insistente do mestre-açu Antonio Candido, e o brilhantismo que é o livro "O Discurso e a Cidade". que eu tenha compreendido o que os professores já disseram sobre ele, duvido. uma intuição me guiava.

> Ninguém sabe melhor do que tu, sábio Kublai, que nunca se deve confundir a cidade com o discurso que a descreve. No entanto, há uma relação entre ambos.
> 
> Italo Calvino, _Le città invisiblili_

a forma a linguagem. o discurso. eu havia enchido as paciências do Doni com esse papo, conversando sobre escritos dele. mas olhar para si é sempre outra tarefa, e em um email a um amigo que começou a ler esse meu último livro, _segunda mão_, comentei de uma revisão que se focou demais no temperamento do narrador, mas que ainda não foi capaz de solucionar algum problema maior. que não foi capaz de compreender o descompasso. ora! que não poderia, jamais. o temperamento do narrador não são as atitudes, tão somente. o temperamento do narrador é o discurso do narrador, e está aí o descompasso, o desencontro, o ponto central de todo o incômodo. essa gente que é texto, tão só, nunca um reflexo de imaginação sem mediador. tudo é o texto. e o texto é o discurso. a linguagem. tudo que o texto retrata vem mediado pela linguagem, inevitavelmente. nisso muito Antonio Candido insiste, e volta sempre ao texto. no que soube levar adiante a compulsividade obsessiva dos new critics para um patamar mais produtivo de análise.

> No romance [o estilo] não se reduz a um modo determinado de usar a língua, pois manifesta simultaneamente a posição do narrador, formando ambos uma realidade dupla. A língua dá vida ao enfoque, o enfoque dirige o estilo.
> 
> Antonio Candido, "O mundo provérbio", em _O Discurso e a Cidade_

inesperada revelação pela leitura da análise de um romance do naturalismo italiano. isso pouco importa. certo que ainda Antonio Candido não diria que se precisa existir a consciência do autor para que se alcance essa organização do discurso, e bem se pode tomar o ensaio do Schwarz sobre o Alencar para saber os efeitos possíveis (ou causas possíveis) de um tal descompasso, mas há um despropósito que se multiplica na literatura atual e é responsável por muitos insucessos e esquecimentos. deveria o autor buscar qualquer coerência interna e compreender o próprio discurso. ou. relevância, coerência. compreender certos mecanismos, para expandir possibilidades. assim como ninguém é obrigado a concordar com Antonio Candido, mas vale conhecê-lo. compreendê-lo. e depois a gente conversa.

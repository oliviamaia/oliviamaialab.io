---
title: " o humano segundo vampyroteuthis infernalis"
date: 2016-12-14 18:51:33
---

> Should Vampyroteuthis analyse objects informed by man, he will find that sex had a very small role in the elaboration of the stored information. And should he analyse human behaviour, he will find that man's sexual behaviour hardly changes over the course of human history. How can such an anomaly be explained, this predominance of digestion over sex? It can be explained by the fact that the human male is slightly larger than the human female. The male repressed the female during a considerable part of human history. This repression caused in the male a constant dread of a female revolt and with this he repressed the female dimension of the world. Thus he repressed the sexual dimension of the world and concentrated his reflection on the digestive apparatus. This lends human history its pathological character: human history is the history of a specifically human neurosis.

Vilém Flusser, _Vampyroteuthis infernalis_

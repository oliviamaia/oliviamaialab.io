---
title: " o início do fim"
date: 2016-12-26 10:00:00
---

> ​When it comes to technology, the brain still functions, but in everything else degeneration has begun. They are all insane: the Communists, the Fascists, the preachers of democracy, the writers, the painters, the clergy, the atheists. Soon technology, too, will disintegrate. Buildings will collapse, power plants will stop generating electricity. Generals will drop atomic bombs on their own populations. Mad revolutionaries will run in the streets, crying fantastic slogans.

Isaac Bashevis Singer, "Cafeteria"

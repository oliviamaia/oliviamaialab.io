---
title: " o outro Walser"
date: 2014-08-28 21:10:50
---

encontrar, no prefácio a _Berlin Stories_, do Robert Walser, outra versão do escritor, que nos últimos anos a crítica resolveu ressuscitar; porque não aquele Walser que morreu no sanatório durante uma caminhada pela neve numa noite de natal, não o que escrevia numa letra tão miúda que foram anos para desvendar suas historietas escritas em embalagens de pão. que é o outro lado da figura do _outsider_: o lado da criança que brinca, um jovem sem noção numa Berlim enorme, desastrado que falava sem pensar e nas festas comia para além do socialmente aceitável. e afinal uma versão que convive muito bem com a versão mais séria encontrada com mais frequência talvez porque afinal Walser era um tipo meio cronópio, exageradamente cronópio, com a solidão que convém aos cronópios em um mundo de famas e esperanças.

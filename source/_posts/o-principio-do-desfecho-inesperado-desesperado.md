---
title: " o princípio do desfecho inesperado desesperado"
date: 2008-09-26 21:07:54
---

#### ou: o princípio Agatha Christie //

porque então te conto uma história em que há quatro personagens e um deles morre. passa então o resto da história na dúvida entre qual dos outros três é o assassino, e as suspeitas e desconfianças, e então no final te jogo um quinto personagem e digo que na verdade o assassino é ele. uau. por essa você não esperava, hein?

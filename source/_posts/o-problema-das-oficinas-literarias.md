---
title: " o problema das oficinas literárias"
date: 2013-06-22 00:41:28
---

> In complex sentences with multiple clauses that relate in complex ways, the talented writer will organize those clauses in the chronological order in which the referents occur, despite the logical relation grammar imposes.

era o que eu tinha pra dizer. isso e mais todo o resto do artigo, que está aqui: "[Good writing vs. Talented writing](http://www.brainpickings.org/index.php/2013/05/20/good-writing-vs-talented-writing/?utm_source=feedly&utm_medium=feed&utm_campaign=Feed%3A+brainpickings%2Frss+(Brain+Pickings))"

---
title: " o problema do pessimismo"
date: 2016-02-02 21:01:36
---

> In raising problems without solutions, in posing questions without answers, in retreating to the hermetic, cavernous abode of complaint, pessimism is guilty of that most inexcusable of Occidental crimes - the crime of not pretending its for real. Pessimism fails to live up to the most basic tenet of philosophy - the 'as if'. Think as if it will be helpful, act as if it will make a difference, speak as if there is something to say, live as if you are not, in fact, being lived by some murmuring non-entity both shadowy and muddied.

Eugene Thacker, _Cosmic Pessimism_ ([via Spurious](http://spurious.typepad.com/spurious/2016/01/there-is-little-redemption-for-pessimism-and-no-consolation-prize-ultimately-pessimism-is-weary-of-everything-and-of-itsel.html))

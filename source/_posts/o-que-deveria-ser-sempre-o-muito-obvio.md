---
title: " o que deveria ser sempre o muito óbvio"
date: 2008-05-05 04:50:51
---

tudo que é dito em coro por mais de meia dúzia de pessoas é mentira;

o que foi dito em conjunto e concordado por mais de uma pessoa é mentira;

duas cabeças em concordância produzem melhor uma mentira;

duas cabeças em concordância estão mentindo;

etc.

---
title: "o suíço e os idiomas: uma coleção de tweets"
date: 2015-12-28 16:09:06
---

**suíço aprendendo português** 

"essa gata tá causando." - suíço, que antes de conjugar o verbo 'ter' no passado já tinha aprendido as expressões mais importantes. 

suíço lendo receitas em português:  
"geleias críticas?"  
"cítricas!"  
"ah, bom!" 

eu ensinando pro suíço o que é _meleca_.  
suíço: "mas os meninos que estão na rua também são meleca?"  
eu: ".......... ah, não! esses são _moleque_!" 

suíço tentando lembrar como se diz _tela_:  
suíço: "apagou a pantalla."  
eu: "apagou quê?" 
suíço: "apagou a.......... panela?"  

suíço: "é que minha musculação está meio tensa." 
eu: "sua musculatura!" 
 
eu: "árvore de manga se chama mangueira."  
suíço: "mangueira? mas como se chama isso que se usa pra jogar água?"  
eu: "mangueira."  
suíço: "......"  
eu: "não, nada a ver." 

suíço: "temos comida, saudade..."   
eu: "saudade? "   
suíço: "gesundheit...?"   
eu: "saúde!"   

suíço: "ainda tem mo... mar..."   
eu: "me..."   
suíço: "mera..."   
eu: "melan..."   
suíço: "melango?"   
eu: "melancia!" 

suíço: "se encontramos um cabeleiro..."   
eu: "ca-be-lei-rei-ro."   
suíço: "cabeleireiro... mas por que temos que fazer essa volta?" 

mãe estudando francês: "hm, acho que escrevi isso errado... ah, não! está certo!"   
suíço, do outro lado da casa: "isso me passa com o português também." 

suíço: "velha? como se chama isso?"   
eu: "vela. ve-la. não vai chegar no Paulinho e pedir um pacote de velhas."

* * *

**a gente tentando entender o polonês:** 

polonês 101:   
eu: "como se diz bom dia em polonês? dica: é parecido com tcheco."   
suíço indignado: "dica?!" 

aprendendo a dizer obrigado em polonês:  
eu: "djen - cu - ié."   
suíço: "djen cuié. isso parece um nome de chinês."

* * *

**eu aprendendo alemão** 

eu: "todas as bebidas alcoólicas são do gênero masculino, menos cerveja, que é neutro."   
suíço (sendo alemão): "cerveja não é uma bebida alcoólica."

* * *

**bônus** 

"não sei se a gente pode confiar nesse teto." - suíço no vão do MASP. 

"como se chama este tipo mesmo?" suíço apontando uma foto do Cristo Redentor. 

eu: "você lava a louça."   
suíço: "schon wieder louça laven?"   
eu: "....... droga, essa nem posso pôr no twitter que ninguém vai entender!"   
suíço: "vou ter que fazer um twitter pra te acompanhar por lá."   
eu: "twitter? que twitter? não tenho twitter."

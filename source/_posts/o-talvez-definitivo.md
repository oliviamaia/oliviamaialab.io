---
title: " o talvez definitivo"
date: 2014-01-25 22:26:56
---

> Os antigos escrevem -- diz Colombo no _Diálogo_ -- que os amantes infelizes jogavam-se no mar do penhasco de Leucádia, e se conseguissem sobreviver, ficavam livres da paixão amorosa pela graça de Apolo. Pois então "cada navegação é quase um salto do penhasco Leucade". Os navegadores estão sempre em perigo de morte, mas justamente por isso valorizam a vida mais que os outros. Distantes da terra, eles não têm maior desejo do que ver dela um "cantinho". Disso fazem fé aqueles que participam da presente expedição: incertos da viagem, acordam e adormecem pensando na terra; "e se uma só vez nos será revelado de longe o cume de um monte ou de uma floresta, ou coisa parecida, nossa felicidade será imensa."
> 
> - Piero Boitani, _A sombra de Ulisses_

e pensar no final do conto "[Perda e recuperação do cabelo](http://www.lainsignia.org/2002/abril/cul_004.htm)", do Cortázar. pensar nesse grande _talvez_ que nos empurra adiante numa vida que nunca poderia ter sentido nenhum senão aquele que damos a ela. por acreditar em alguma coisa ou procurar o que com certa frequência nos parece impossível alcançar*; _para lutar contra o pragmatismo e a horrível tendência à consecução de fins úteis_; por procurar um fio de cabelo com um nó no meio; por acreditar no novo mundo ou num mundo novo. é o mesmo _talvez_ que faz a literatura: 

"Os homens escrevem ficções porque estão encarnados, porque são imperfeitos. Um deus não escreve romances." (Enrique Vila-Matas) 

nesse livro _A sombra de Ulisses_, o autor conta como Leopardi comenta a glória da empreitada de Colombo apontando que "o conhecimento reduz, faz com que [o mundo] 'diminua' na consciência do homem", "achata a Terra tornando-a uniforme, transformando-a em _breve carta_", "arranca o 'caro imaginar' do menino e da humanidade criança, mata, com o 'real', o estupendo poder primigênio da imaginação", "faz desaparecer de repente, fixando-os em um ponto geográfico, os sonhos do outro mundo, da nova terra além do ocaso do sol". pois está posta a contradição desse _talvez_. é ele, afinal que impulsiona a ciência, tida por Leopardi como "inimiga das grandes ideias, apesar de ter desmedidamente ampliado as opiniões naturais", pois "uma pequeníssima ideia confusa é sempre maior do que uma grandíssima completamente clara". o _talvez_ é sempre maior do que o _sim_ e o _não_. o _possível_ é sempre infinito, enquanto o fato está limitado em suas dimensões medíveis. mas no devir da existência a grandeza do _talvez_ está condenada a se transformar a todo momento em _sim_ e _não_, em fatos, descobertas; mesmo na forma de ficção ele perde também um pouco da sua imensidão, dadas as devidas proporções e acreditando no que Aristóteles apontou sobre a diferença entre poesia e história: a poesia é "o que _poderia_ acontecer". um _talvez_ que cria a esperança mas também faz nascer a espera). 

de qualquer forma, não escapamos dele. 

*na terça-feira saindo do metrô fui abordada por uma moça mui simpática que _precisava dizer, ainda que aquilo soasse um pouco estranho_ que eu pensava que estava buscando alguma coisa impossível, mas não, era isso mesmo, não era uma busca impossível. eu sorri, ela pareceu um pouco constrangida, deu um _tchau_ meio sem jeito e subiu os degraus da escala rolante.

---
title: " objetivo"
date: 2016-09-26 10:24:54
---

> On one side there is luminosity, trust, faith, the beauty of the earth; on the other side, darkness, doubt, unbelief, the cruelty of the earth, the capacity of people to do evil. When I write, the first side is true; when I do not write, the second is. Thus I have to write, to save myself from disintegration. Not much philosophy in this statement, but at least it has been verified by experience.

- Czeslaw Milosz, _Road-Side Dog_

---
title: " os donos do mundo"
date: 2013-07-07 16:33:20
---

o mundo é das pessoas que escrevem títulos com todas as iniciais em maiúsculas; escrevem os nomes dos meses com maiúsculas; chamam o facebook de “fêice”; não sabem escolher as palavras-chave para fazer uma busca no google; clicam no primeiro resultado da busca do google sem ler do que se trata; chamam o google de “gâgol”; usam gerúndio sem sujeito; usam o verbo “possuir” com sujeitos inanimados; usam “o mesmo” no lugar do pronome pessoal; dizem “sou péssimo em português”; tomam decisões baseadas em retorno financeiro; trafegam pelo acostamento na volta da praia; usam expressões como “ser pró-ativo” e “agregar valor”; leem biografias de homens de negócio; se interessam por listas sobre os 100 homens mais poderosos do planeta; cobrem os olhos das crianças na exposição de arte ao deparar com um nu; dizem “dá pra pôr um fundo azul?”; dizem “eu não teria essa coragem”; um mundo em tons pastéis, um mundo classe média norte-americana, com respeito ao _personal space_ alheio e o medo de processo caso você ajude a se levantar uma senhora que tropeçou e caiu sozinha na rua.

---
title: 'oli joga videogames: estacionar é uma arte'
date: 2021-12-28 10:25:08
---

![screenshot de no man's sky mostrando nave estacionada de lado](/img/posts/games/screenshot_20210804-134140.png)

![screenshot de no man's sky mostrando nave estacionada de lado](/img/posts/games/screenshot_20210806-191201.png)

![screenshot de no man's sky mostrando veículo estacionado dentro de um buraco](/img/posts/games/screenshot_20210807-200011.png)

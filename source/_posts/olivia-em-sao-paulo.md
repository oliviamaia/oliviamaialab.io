---
title: " olivia em são paulo"
date: 2015-03-03 12:46:11
---

terminei minha volta patagônica, parei em Buenos Aires e voei pra São Paulo. mama e Oliver me esperavam no aeroporto. isso já uma semana, e já estou preparando a mochila pra seguir viagem. ou: "viagem". até quando a gente continua chamando de viagem o que é a vida? também esses dias fiz trinta anos. ainda não publiquei nada de fotos embora já tenha todas selecionadas. é que vou me enroscando com outras coisas e a verdade é que quase nunca tenho o computador ligado. nem posso dizer que a culpa é do teclado suíço-alemão. aí que resolvi umas pendências burocráticas e já estou pronta pra ir embora. faltaria teoricamente tomar vergonha na cara e atualizar fotos escrever newsletter etc. tenho uma alegria gigante morando dentro de mim que fica rindo sozinha até mesmo quando estou mei triste mei entediada mei ansiosa com o futuro. parece que o mundo fica enorme e ele é todo meu. pode ser o suíço, pode ser esses meus livros nas estantes, pode ser a pilha de roupas pra meter dentro da mochila. eu nem sei. com certeza não é a cidade de São Paulo.

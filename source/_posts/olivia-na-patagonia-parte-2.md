---
title: " olivia na patagônia, parte 2"
date: 2015-02-04 19:15:35
---

buenas. cheguei hoje a El Bolsón, que é mais um pedacinho de Patagônia argentina nesses lados da cordilheira dos Andes. Oliver tomou um ônibus pra Santiago de onde toma um voo pra Belo Horizonte e se os planos funcionarem nos vemos em mais ou menos um mês. desde San Martin de los Andes passamos ainda por Villa Traful, que foi uma belezinha (sem internet) e por fim Bariloche, onde, depois de duas noites numa _hostería_ de verdade (chamada La Casita Suiza, porque tudo por ali é _suíço_, aparentemente), com cama e café da manha e essas coisas todas, tomamos o rumo de Los Coihues (a uns 15 km de Bariloche), ao lado do lago Gutierrez, e paramos num camping ali por uma semana. já as paisagens se repetem um tanto, ainda que lindas e tudo mais. lagos, montanhas, uns pontos de neve nos picos, água fria, poeira, vento. mas às vezes tem algo assim: 

e você um pouco cai pra trás porque né. agora termino uma volta curta pela Patagônia: El Bolsón, Esquel e Puerto Madryn (pinguins!), com uma possível parada em Las Grutas antes de seguir a Buenos Aires de onde sai meu voo a São Paulo no dia 24 de fevereiro. e depois... depois... paf. depois a gente vê. estou aqui terminando de escrever e organizar a [newsletter](http://oliviamaia.net/newsletter/) (selecionando umas fotos e tudo) e publico ainda essa semana (agora vai). até o final do mês estamos de volta à programação normal.

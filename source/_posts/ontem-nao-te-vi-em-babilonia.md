---
title: " ontem não te vi em Babilónia"
date: 2009-10-05 03:04:44
---

do Antonio Lobo Antunes, que ganhei do marido no último natal e que até então tinha lido um primeiro capítulo, porque livros grandes me atraem mas me impedem de ler em qualquer posição esdrúxula e requerem sempre alguma classe e postura pra não se ter dores nos punhos já cheios de tendinite

            (sinal dos tempos)

mesmo com a academia diária; mas enfim, o livro, _ontem não te vi em Babilónia_, um dos últimos do portuga louco, agora engatei leitura e ainda que não se possa levá-lo em qualquer lugar

                      (senão a dor nas costas, além das tendinites)

por causa do peso e tamanho pouco conveniente, devo dizer que o livro é bom demais, que Antonio Lobo Antunes é um chato mas é bom demais, e que o formato todo do post é também minha modesta homenagem a esse portuga que sabe fazer uso das possibilidades de formatação do texto ao seu favor, ao contrário de outro portuga, também chato mas nunca tão bom;

estive também lendo alguns poemas do Herberto Helder, em _ou o poema contínuo_; e como disse o amigo Testa, esse livro bota em desespero, essa batalha com o hermético contra o hermético do mundo, poemas que se escapam, se desviam, por uma compreensão maior que está logo ali mais ou menos à esquerda ou à direita no campo de visão, mas que foge ao foco, escapa sempre ao foco.

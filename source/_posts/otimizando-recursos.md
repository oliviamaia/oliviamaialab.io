---
title: otimizando recursos
date: 2020-02-17 18:07:49
---


teoricamente as imagens em [rabiscos](https://oliviamaia.net/arte/rabiscos) e [inktober](https://oliviamaia.net/arte/inktober) agora devem carregar de forma **aceitável**. ainda não está 100%, mas acho que melhor. se tiver alguma sugestão quanto a isso, por favor me [grita](mailto:olivia@oliviamaia.net).

também dei umas otimizadas site afora, já que estava no processo, enquanto também aprendia a user _branches_ em git sem quebrar nada. acho que funcionou.

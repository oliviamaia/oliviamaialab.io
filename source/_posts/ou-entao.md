---
title: " ou então"
date: 2008-11-28 06:08:03
---

porque no futebol eu sempre fui lateral, ou ainda zagueira, mas nunca meia, nunca atacante, porque estar rodeada de campo por todos os lados me dá uma agonia terrível.

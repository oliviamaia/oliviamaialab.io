---
title: " Ouro Preto: uma cidade improvável"
date: 2014-02-05 14:48:50
---

Ouro Preto é uma cidade improvável. só mesmo muita ganância para fazer surgir uma vila entre tanto morro, tão íngremes, que foi também o que aconteceu em boa parte das cidades e vilas mais antigas da Chapada Diamantina. as ruas são todas irregulares, inclinadas para cima, para o lado, os paralelepípedos escorregadios assentados em ondulações impossíveis. um passo em falso e a gente rola morro abaixo. na melhor das hipóteses, escadas quase regulares. quase. todo lado que se olha uma torre de igreja. 

tento imaginar a cidade nos séculos XVIII, XIX. as construções não mudaram muita coisa. mas o barulho, os carros, enfim meus próprios passos e o peso da mochila me puxam de volta à realidade do presente antes mesmo que eu consiga me lembrar da descrição que um professor de literatura brasileira uma vez fez de Tomás Antônio Gonzaga. 

olho para o chão e uma bituca de cigarro, uma embalagem de bala, um recibo de cartão de crédito. parece impossível acreditar que tanto tempo atrás o céu fosse aquele mesmo céu, o calçamento das ruas; imaginar aquele muro velho e manchado quando havia acabado de ser construído.

nas imagens e fotos da exposição na estação ferroviária a cidade parece não ter mudado absolutamente nada.

para inveja dos amigos cariocas e paulistas, digo que está fresco, que à noite e pela manhã faz um friozinho, que durante o dia a temperatura vai aos 28, 29 graus (no máximo 30, e quem diria que isso poderia ser algo de que se gabar). o que mata são as ladeiras. cheguei ontem pela manhã, depois de uma viagem de muitas curvas e muitos dramins. estou no Buena Vista Hostel, que também fica no meio de um morro, mas o pessoal aqui é simpático, tem wifi, o preço é camarada e é pertinho da praça Tiradentes. que mais precisa? caminhei um pouco, dormi um pouco, li um pouco. inesperado foi encontrar o Kevin Johansen, gênio, filmando um clipe novo na frente da igreja Nossa Senhora do Carmo. quase não consegui me manter de pé quando fui falar com ele.

enfim. alguém esqueceu do meu inferno astral. talvez tenha sido eu. hoje caminhei mais um pouco e fui até a estação rodoviária, de onde agora sai um trem turístico até Mariana. amanhã vou passar o dia por lá.

---
title: " paciência tem limite"
date: 2008-10-04 02:44:51
---

a professora de teoria do texto II, que acha que é a única professora de toda aquela faculdade que ensina a matéria do jeito que deve ser ensinada (sim, ela disse isso) toda aula querendo analisar texto do Roberto Pompeu de Toledo, Bonassi e Mainardi, que é pra mostrar a fina ironia e a retórica mui bem desenvolvida dos elementos. tipo. morre. sério. se joga da ponte. enquanto isso eu uso o tempo da aula pra corrigir redações (porque, convenhamos, a retórica dos alunos é melhor do que a deles).

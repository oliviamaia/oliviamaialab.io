---
title: " palavras palavras palavras"
date: 2015-08-22 11:28:57
---

algumas palavras em tcheco que aprendemos em dois dias e meio de Praga:

*   cerveja
*   saída
*   cuidado/atenção
*   porta
*   cidade
*   por favor
*   obrigado
*   ponte
*   restaurante
*   pizza
*   sim
*   manteiga

... e amanhã já começa a parte polonesa dessa viagem.

---
title: " passeio em Juazeiro antes de ir embora"
date: 2013-09-25 19:13:25
---

hoje de manhã pegamos a barca para atravessar o rio São Francisco e conhecer a cidade de Juazeiro. o pessoal daqui fica dizendo que Petrolina é bem melhor e o lado de lá não tem nada de bom. a moça da biblioteca de Petrolina, por exemplo, mora em Juazeiro e disse que não tem nada para fazer lá. mas é que pelo menos a gente tirava foto cruzando o rio. caminhamos um pouco pelo centro e depois fomos conhecer o campus da Univasf (é longe, o povo dizia, mas a gente chegou em quinze minutos). aproveitamos para conhecer a biblioteca, usar a internet para resolver pendências bancárias (meu deus e essa greve que não acaba nunca) e almoçar barato na lanchonete de lá. 

na volta o sol estava gritando uns 36 graus. compramos numa banca um mapa rodoviário para parar de sofrer sem internet na hora de decidir para qual cidade ir nos meios de caminho entre um destino final e outro. aí cruzar o rio outra vez e voltar para o apartamento organizar as coisas e descansar um pouco do calor.

(essas fotos são do celular, que é de onde estou escrevendo. depois a gente põe no álbum as fotos da câmera de verdade.) daqui a pouco vamos comer alguma coisa, terminar de arrumar as malas e zarpar para a rodoviária. nosso ônibus sai quase meia-noite rumo à Seabra. dali de manhã pegamos outro ônibus para chegar em Lençóis, na Chapada Diamantina.

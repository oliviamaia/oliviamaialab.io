---
title: por aí internet afora
date: 2020-07-11 09:50:38
---

porque não apareço mais no twitter e não tenho facebook e instagram e de repente me distanciei, parece, da parte brasileira das internetz. parece que não tem mais ninguém ouvindo, ninguém interessado. parece que todo mundo está muito mais ocupado tentando se fazer ouvir.

no mastodon acabo conhecendo mais gente que se comunica em inglês e, olha, as pessoas _se comunicam_. não porque é o inglês, mas porque é o mastodon, o fediverso. o inglês é só um idioma em comum.

eu sinto falta de escrever e me comunicar em português, mas não encontrei esse lugar na internet em que as pessoas se _comunicam_ em português. alguns blogs, alguma gente falando sozinha. meu blog, eu falando sozinha. fico querendo perguntar se é alguma coisa do momento cultural brasileiro ou da minha cabeça. ou do twitter?

fico querendo perguntar, mas não pergunto.

não tem ninguém ouvindo.

---
title: " pôr do sol nas praias do Trairi"
date: 2013-09-07 09:56:05
---

cinco e quarenta da tarde todos os dias o ano inteiro o sol morre aqui no Ceará. nas praias do Trairi, principalmente em Flecheiras e na barra do Mundaú, é momento para parar um pouco e ficar olhando o horizonte. Flecheiras a gente ia quase todo fim de dia, que a Esther ia abrir o café e nós pegávamos uma carona com ela, para depois voltar só depois das dez da noite. 

na segunda-feira o café não abria e ela nos levou até Mundaú, onde fica a barra do rio, para ver o pôr do sol. tomamos uma cerveja na beira da praia e depois subimos de carro até as dunas. caminhamos por lá e esperamos o sol ir embora para voltar.

curioso como a gente começa a prestar atenção em umas coisas que antes nem passavam pela cabeça: a hora exata do pôr do sol, tábua de mares, direção do vento, horários do vento. em São Paulo a gente sabia a hora do rush. talvez sejam coisas análogas.

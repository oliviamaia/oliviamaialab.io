---
title: " por onde eu ando"
date: 2013-12-21 12:00:42
---

para uma resposta rápida, melhor mesmo é clicar aqui. e se ainda não está acompanhando o blog autonautas, corre lá, assina o feed ou inscreve o e-mail para receber as atualizações. tem um monte de fotos e relatos e essas coisas todas. agora estou em uberaba na casa da minha prima aproveitando as mordomias familiares. domingo vamos ver dinossauros. já foram quatro meses e meio viajando: começamos em Fortaleza, depois algumas praias cearenses (incluindo Jericoacoara); aí Serra da Capivara, no Piauí, Petrolina (PE) e Juazeiro (BA); Lençóis e Chapada Diamantina por umas duas semanas e rumo ao Goiás; Alto Paraíso de Goiás por quase dois meses trabalhando em uma fazenda orgânica com cultivo de shiitake e permacultura, uma visita rápida a Pirenópolis e enfim Minas Gerais, Uberaba, casa da prima. já dormi em barraca, a céu aberto, em cama, saco de dormir, casa na árvore. banho no rio e em piscina natural, e muito banho frio ou gelado. aprendi a fazer pizza, tapioca e cozinhar coisas vegetarianas diversas. escrevi pouco, tirei muitas fotos. desse percurso algumas conclusões:

*   a gente se acostuma a qualquer coisa, para o bem e para o mal;
*   quanto mais a gente sai do hábito mais estranhas parecem as manias das pessoas que continuam nele;
*   tem muita gente boa no mundo (se é que a gente pode falar em gente boa e má).

ou: no hábito a gente não consegue acreditar na nossa própria capacidade de mudar e se adaptar, mas sair disso vale a pena pelas pessoas boas que estão no mundo e valem a pena conhecer.

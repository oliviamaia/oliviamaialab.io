---
title: " por que eu não uso maiúsculas"
date: 2015-12-15 11:19:28
---

primeira resposta: hábito. 

segunda resposta: me parece mais bonitinho. 

você pergunta: mas por quê? nem lembro quando foi que parei com essa de botar maiúscula no começo da frase. quando escrevo meus romances, meus contos, sou uma pessoa comportada e faço tudo de acordo com a norma padrão da língua portuguesa etc (mais ou menos, às vezes eu como uma vírgula pra obrigar o leitor prender a respiração). na internet, nas mensagens do celular, parei (desabilitei o corretor ortográfico do celular porque não aguento ele capitalizando minhas pobres palavras). esse negócio de maiúscula e minúscula, afinal, é uma mania relativamente recente na história da escrita. antes, maiúsculas e minúsculas nem se misturavam. houve uma época em que se usava maiúscula só pra marcar importância (daí o uso com os nomes próprios) e mesmo hoje em dia algo desse hábito se mantém. tem muito brasileiro que acha que tem que capitalizar todas as palavras num título, por exemplo. faz parecer importante, né? a gramática do português diz que isso não existe. puro anglicismo. e se existe o ponto final, por exemplo, a maiúscula seguinte acaba sendo uma redundância. ou não? a internet não precisa de maiúsculas.

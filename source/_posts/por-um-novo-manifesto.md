---
title: " por um novo manifesto"
date: 2016-12-30 15:44:36
---

> ADORNO: If we let history go its own way and we just give it a little push, it will end up in a catastrophe for mankind. HORKHEIMER: Nothing can be done to prevent that except to bring in socialism. ADORNO: That’s what we say too.

tem algo de cômico no desespero dessa conversa (_Towards a New Manifesto_). a convicção de super-heróis de história em quadrinhos. a busca por uma forma de explicar o óbvio e fazer-se compreender? hoje, nenhuma novidade. as palavras se gastaram, todas.

---
title: " porque eu preciso escrever umas coisas e não é pra fazer sentido mesmo"
date: 2008-11-28 05:38:04
---

então o que é aquela coisa que de repente não se volta atrás nunca mais feito um doce que se deixou no forno e passou o tempo de tirar ou uma sucessão de ctrl-z que acaba no instante exato antes da mudança que você queria desfazer ou um HD que morre com todas as fotos e os textos e a vida. essa agonia estúpida do nunca mais da negação no infinito que dói nos olhos só de pensar. feito um despropósito, porque afinal, que importa o doce no forno. ou o absurdo da inconformação. sentar e esperar se alinharem os astros, que parece ser às vezes o rumo, quando. quando. quem ali quem em algum lugar quem quem alinha as coincidências? onde se meteu Apolo?

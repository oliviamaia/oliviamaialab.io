---
title: " porque tem dias que é bem assim que o mundo nos diz as coisas"
date: 2009-02-03 05:19:36
---

> Demitido a bem do serviço público, inscrevi-me numa maratona de danças e fui transportado semi-inconsciente para um hospital de tuberculosos, onde vim a falecer na madrugada de 15 de setembro de 1934. Mas o atestado de óbito fora passado um pouco às pressas e obtive alta dois meses depois, mais forte do que um touro da Pomerânia ou de qualquer outra parte do globo.

_A lua vem da Ásia_ (1956), Campos de Carvalho

---
title: " pra onde se cospe"
date: 2009-06-24 17:59:19
---

depois conversando com o Doni por email sobre meu último post, ele comentou do tgi que estava escrevendo e me mandou esse trecho do Adorno:

> Enquanto o cálculo egoísta é expandido ao extremo, a visão da totalidade dos fatores e, particularmente, dos efeitos de tal política sobre o todo, é estranhamente diminuída. A perspicácia excessivamente concentrada no interesse próprio resulta na deterioração da capacidade de enxergar além dele, o que resulta em prejuízo para o interesse próprio. A irracionalidade não é necessariamente uma força que opera em uma esfera externa à racionalidade: ela pode resultar do transtorno de processos racionais de autoconservação.

[que pode também ser uma maneira de _cuspir pra cima_.]

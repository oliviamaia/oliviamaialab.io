---
title: " praga é uma cidade fotogênica"
date: 2015-08-23 10:45:13
---

não me leva a mal: a cidade é bonita ao vivo também. mas depois de tantos spoilers fotográficos talvez fosse normal essa sensação de que, _ok, a cidade é linda, mas também não é a coisa mais fantástica que eu já vi._ afinal, é uma cidade. depois passei minhas fotos ao computador e fui reencontrando aquela cidade linda que eu tinha visto na internet antes de vir. e concluí que Praga é na verdade uma cidade fotogênica.

 exceção a tudo isso é o relógio astronômico, que é terrivelmente mais lindo ao vivo, mesmo com o mar de turistas que se amontoa sob ele, mesmo quando dá hora cheia e não é possível caminhar por ali enquanto todos esperam o momento do "espetáculo" do relógio (na verdade a caveirinha toca um sino, umas figuras se mexem e todos os turistas ficam decepcionados). 

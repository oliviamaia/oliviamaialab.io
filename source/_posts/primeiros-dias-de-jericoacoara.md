---
title: " primeiros dias de Jericoacoara"
date: 2013-09-08 12:25:48
---

chegamos em Jericoacoara no meio da tarde da terça-feira retrasada mas só começamos a curtir mesmo a vila no dia seguinte. passamos a primeira noite num camping muito ruim e fugimos de lá assim que nasceu o sol. estabelecidos enfim no camping do Natureza, num quartinho com banheiro, saímos para caminhar pela praia.

o dia amanheceu meio nublado. seguimos para o lado esquerdo pela praia principal, passando pela frente da famosa Duna do Pôr do Sol, e caminhamos até começar a sentir fome. voltamos para a vila para almoçar um PF, indicação de um mineiro que conhecemos no camping. à tarde aproveitamos a maré baixa para andar até a Pedra Furada. 

não deu tempo de voltar para o pôr do sol na duna: o Fabio topou o pé numa pedra e ficamos na praia mesmo, com um saquinho de gelo. no final das contas o sol se escondeu atrás das nuvens antes mesmo de se aproximar do horizonte.

logo no dia seguinte acordamos cedo para fazer um dos passeios de buggy. fomos direto para a Jumentur, agência do guia Tatuzinho, que tínhamos conhecido na Pedra Furada no dia anterior. os preços são os mesmos sempre (200 reais o buggy para dividir em até quatro pessoas, que na verdade são 160 se você insistir um pouquinho mais). fomos com uma dupla de pai e filho de Brasília para o percurso da parte Leste de Jericoacoara: Lagoa do Paraíso e Lagoa Azul. levamos uns sanduíches para o almoço porque a comida nas barracas é tudo muito caro. mais uma vez um dia nublado e meio frio, considerando o vento que nunca para. 

no final do dia ainda subimos a duna para ver o pôr do sol lá de cima. dizem que é uma das coisas mais bonitas do mundo. a verdade é que não pegamos nenhum pôr do sol com o céu muito limpo. sempre tinha uma nuvenzinha distante e cinzenta para engolir o sol antes que ele encostasse no mar. subir a duna é fácil. o difícil é ficar lá em cima sem engolir areia. o vento vem, como em qualquer ponto do litoral cearense, do leste, que no caso significa pelas costas. e vem trazendo areia de tudo que é duna que encontra pelo caminho.

nos outros dias ficamos um pouco mais à toa. conversamos com o pessoal do camping, lemos (desenhei um tanto também), mergulhamos no mar (tentamos; o mar de Jeri é desses que você anda três quilômetros e a água continua no seu joelho - se a maré estiver cheia). no domingo 1º de setembro fizemos o outro passeio de buggy, para o lado da vila de Tatajuba, também com o guia Tatuzinho. dessa vez a companhia foi um carioca e uma paulistana. 
o plano inicial era ficar até o dia 2 de setembro, quando o quarto em que estávamos deveria ser desocupado por causa de uma reserva. mas o dono do camping logo nos ofereceu um desconto camarada para o caso de querermos estender a estadia em barraca. a ideia pareceu bem razoável. tínhamos levado barraca e não custava nada usar um pouquinho. fizemos as contas e concluímos que o dinheiro ainda dava para mais uns cinco dias. na segunda-feira então acordamos, arrumamos as malas e nos mudamos para debaixo de uma sombra dos cajueiros. ![2013-09-04 15.27.47](/img/2013/09/2013-09-04-15.27.47-455x470.jpg)

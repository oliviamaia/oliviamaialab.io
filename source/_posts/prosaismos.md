---
title: " prosaísmos"
date: 2010-09-01 00:59:38
---

há qualquer coisa em certos escritores da língua espanhola que me espanta. uma simplicidade no dizer, um prosaísmo disfarçado de poesia ou mais provavelmente o contrário. um dizer desenfreado que não encontra fôlego digno de pausa na necessidade de preencher o vazio com palavras gritando no silêncio, enquanto ao leitor por trás das letras e da sonoridade ininterrupta grita ainda aquele silêncio, destacado de seu valor de pano de fundo porque as palavras, porque as frases e a impossibilidade de se dizer aquele silêncio.

digo da língua espanhola porque Cortázar, porque Bolaño, porque agora Javier Marias que comecei a ler hoje à tarde; e leio em tradução porque meu espanhol sofre de desnutrição. mas vê, está lá. a organização da sintaxe em fluxo que poderia não acabar nunca mais e sempre há algo a se dizer, algo pequeno e um detalhe do cenário ou na descrição de um rosto, de um olhar, de uma toalha de mesa, ou ainda um sentimento sem nome para cuja compreensão multiplicam-se comparações e metáforas e palavras e está ali o cotidiano todo, as coisas minúsculas. mas por trás grita aquele silêncio e tudo o que o silêncio nunca poderia nos dizer, e só ouvimos gritar o silêncio porque grita junto desse fluxo ininterrupto de palavras e frases e pequenezas.

algo nesses escritores que me espanta, que me encanta. que está no aproximar do prosaico ao poético ao ponto que os dois se misturam, e a vida parece que cresce, a vida fica enorme; enorme num mundo gigantesco.

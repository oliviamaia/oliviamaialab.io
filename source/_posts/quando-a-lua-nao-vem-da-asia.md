---
title: " quando a lua não vem da Ásia"
date: 2009-02-07 17:43:55
---

oy, mais Campos de Carvalho, agora um capítulo inteiro, levando meu pavor de baratas to a whole new level of freakiness:

> **Capítulo Negro** Tenho sido injusto para com a Noite. Amo a Noite e vivo a difamá-la, chegando mesmo ao crime de tomar narcótico para combater a insônia -- esse meu único bem. A Noite é a túnica que me assenta como uma luva, como sudário a um cadáver, ou -- já que estou mesmo no terreno das comparações -- como óculos escuros num cego de nascença, em pleno meio-dia. Só não amo, na Noite, as baratas e os escorpiões, estes felizmente mais raros de encontrar do que os fantasmas ou os assassinos embuçados nas esquinas sem luz, a desoras. As baratas, temo-as como aos seres fantásticos criados pela imaginação de Jerônimo Bosch, e preferiria ter que entrar na jaula dos leões a ter por um instante na mão um desses habitantes dos esgotos e das sarjetas, de antenas vibráteis e patas de caranguejo. Vou mesmo ao extremo de preferir uma sopa de escorpiões vivos ao simples contato de uma barata morta e já em parte devorada pelas formigas, de patas para o ar como uma prostituta. O meu inferno será por certo todo coalhado de baratas aos milhares e aos milhões, todas vivas e ágeis dentro das trevas eternas e úmidas -- a menos que falte ao meu punidor a imaginação necessária para punir-me até a loucura, ou não tenha ele maldade bastante para impor um tal castigo à minha humana inocência. Mas a Noite, excluídas as baratas e a ameaça dos escorpiões, é a minha musa e o meu túmulo bem-amado, aquele a que aspiro com todas as forças da minha alma, como deve aspirar ao seu todo ser lúcido e tocado de inviolável pureza. E aqui lhe rendo esta homenagem tardia mas veemente, no pleno silêncio deste quarto frio e povoado de trevas, tendo por quadro-negro esta parede onde a custo faço deslizar a ponta do meu lápis, já que a lua hoje não veio da Ásia e não consigo sequer enxergar o meu triste corpo ajoelhado na cama. ... _Non dormit diabolus_.

_A lua vem da Ásia_ (1956), Campos de Carvalho

quem é louco?

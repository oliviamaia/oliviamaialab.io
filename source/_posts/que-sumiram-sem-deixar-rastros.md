---
title: " que sumiram sem deixar rastros"
date: 2009-04-04 06:15:40
---

> Colocar o próprio nome em alguma obra não assegura a ninguém o direito de ser lembrado, pois quem sabe se não foram justamente os melhores que sumiram sem deixar rastros.

W.G. Sebald, _Os anéis de Saturno_

[gênio.]

---
title: " que."
date: 2012-01-13 20:20:31
---

> "Me costaría explicar la publicación, en un mismo livro, de poemas y de una denegación de la poesía, del diario de un muerto y de las notas de un prelado amigo mío..." -- Georges Battaile, _Haine de la poésie_. [Rayuela, 136.]

---
title: " reflexões necessárias sobre o futebol"
date: 2014-07-25 11:52:47
---

porque um dia talvez eu vá escrever um ensaio chamado "o futebol e a nossa humanidade", embora me pareça que tudo o que poderia ser dito sobre esse assunto já está muito bem dito no próprio título, nas entrelinhas (entreletrinhas).

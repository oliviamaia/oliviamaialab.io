---
title: " retiro Tao Tien: primeiras fotos"
date: 2014-02-28 12:00:12
---

antes de mais nada eu queria dizer que [aqui](http://www.taotien.com.br) tem seriemas e gatos muito peludos e cães e meditação duas vezes por semana e uma vista muito massa. 

![img_9069](/img/posts/img_9069.jpg)

![img_9095](/img/posts/img_9095.jpg)

![img_9122](/img/posts/img_9122.jpg) 

![img_9111](/img/posts/img_9111.jpg)

![img_9082](/img/posts/img_9082.jpg) 

![img_9113](/img/posts/img_9113.jpg) 

![img_9144](/img/posts/img_9144.jpg)

![img_9059](/img/posts/img_9059.jpg)

também tem aranhas, mas são mais simpáticas do que as tarântulas de Alto Paraíso. (ah, e teve bolo de cenoura de aniversário. com chá mate gelado.) 

![img_9236](/img/posts/img_9236.jpg)

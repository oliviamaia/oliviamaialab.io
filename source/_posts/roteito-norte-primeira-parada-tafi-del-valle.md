---
title: " roteito norte, primeira parada: tafí del valle"
date: 2014-10-03 17:32:23
---

ônibus mais top da viagem e a viagem mais curta: duas horas e meia de San Miguel de Tucumán a Tafí del Valle. mas o cenário GENIAL pra subir a montanha e descer a montanha e chegar ao vale em que está Tafí. não tirei foto. ops. quer dizer, é legal mas também é bem TENSO porque curvas impossíveis e precipícios. mas floresta verdinha como parece ser o caso nesses lados mais centrais tucumanos, uma bela mudança de paisagem em relação a La Rioja (e seria também em relação ao que viria adiante). aí Tafí del Valle que parece o condado dos hobbits. nos metemos no hostel Nomade, da rede HI, meio afastado do centro e com janta inclusa na diária de 120 pesos. os donos não estavam porque a mulher foi ter filho em San Miguel de Tucumán mas estava a irmã e a família que cuidavam de tudo por ali e viviam na casita ao lado. montanhas por todos os lados principalmente leste e oeste porque no sentido norte-sul corre o vale que começa ali no povoado El Mollar e o dique La Angostura e segue por uns 8 km até Tafí antes de se escapar por uma quebrada e se meter pelos vales Calchaquíes, onde ficam Amaicha del Valle e Cafayate.

depois mais tarde ir fazendo amizades com a turma do quarto compartilhado, umas belgas e um alemão grandote com a cara do Will Ferrell e com um amigo do casal que estava cuidando do hostel. pra isso também o jantar incluso na diária serve bastante, já que come todo mundo na mesma mesa.

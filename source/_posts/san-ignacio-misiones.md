---
title: " San Ignacio, Misiones"
date: 2014-05-21 09:26:56
---

de Puerto Iguazu até San Ignacio sai ônibus a cada uma ou duas horas. são mais ou menos quatro horas de viagem; é sempre um mesmo ônibus que vai para Posadas. o terminal de San Ignacio que fica mei longe de tudo, ali no lado esquerdo da pista enquanto a cidade toda está do lado direito, e mais ainda depois de uns seis quarteirões adentro; antes disso você ou entra pela avenida Sarmiento, a principal, ou por alguma das ruas de terra vermelha paralelas. o asfalto vai começar na altura do centro, depois desses seis quarteirões adentro. eu caminhei até o Hostel El Jesuíta: umas dez quadras com uma leve subida e 12 quilos nas costas. não foi o fim do mundo, mas cheguei mei morta. o albergue é simples, pequeno e simpático. tem um café da manhã bem bonzinho e sai 82 pesos, que hoje em dia está como 20,50 reais. não tá mal. cheguei umas quatro da tarde e achei melhor ficar quieta e descansando. caminhei um pouco, comi empanadas de queijo com cebola, comprei comida, conversei sobre futebol e a copa do mundo com a moça da recepção, tentei entender o espanhol da francesa da cama ao lado. no dia seguinte, que eu teria inteiro, fui de manhã visitar as ruínas de San Ignacio Mini, das missões jesuítas. no museu uma moça veio dizer que sairia um tour guiado em inglês, e se eu entendesse inglês podia acompanhar. acompanhei. ela explicou sobre o funcionamento da missão e sobre o tipo de coisa de que os índios tinham que abrir mão, e o que enfim ganhavam se submetendo aos jesuítas. o lugar em si é gigantesco, e o que sobrou da igreja dá uma noção da megalomania daqueles padres. 

![img_0266](/img/posts/img_0266.jpg)

depois do almoço fui caminhar até uma tal Playa del Sol, distante uns três quilômetros do centro. cheguei junto de um grupo de três francesas e uma cachorra que estava antes passeando pelas ruínas. a praia é demais tranquila e resolvi ficar por ali lendo um pouco depois de uma meia hora de conversa com o cara que toma conta do lugar — claro que falamos sobre futebol e a copa do mundo. o rio Paraná passa ali distribuindo umas ondinhas pela areia e enquanto batia o sol a brisa era fresca. eu era a única pessoa ali, à parte um suíço e um norueguês que estavam acampando mais pra cima. 

![img_0327](/img/posts/img_0327.jpg)

voltei antes que o sol começasse a ir embora e peguei uma carona com um agente de viagens de Puerto Iguazu até a casa do Horácio Quiroga, no caminho de volta. a casa em si não tem nada demais e não vale os 30 pesos cobrados pela entrada. me diverti mais com as duas meninas de uns 9 ou 8 anos que me levaram pra fazer o tour. 

![img_0361](/img/posts/img_0361.jpg)

no dia seguinte saí depois do café da manhã e tomei de volta o caminho de terra até o terminal. de lá saem ônibus pra Posadas de hora em hora, praticamente; aqueles mesmos que vêm de Puerto Iguazu. álbum de fotos vem com atraso quando eu tiver internet de verdade.

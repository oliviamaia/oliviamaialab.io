---
title: " san miguel de tucumán e a fuga da cidade grande"
date: 2014-10-01 07:00:19
---

cidade grande! tivemos que tomar um ônibus às nove da noite até La Rioja e de lá era pra ser o das duas até San Miguel de Tucumán, capital da província de Tucumán, mas esse estava lotado então esperamos o das quatro sentadas numas mesas dum kiosko e vendo televisão ruim. a cidade é... bueno, uma cidade. tem centro histórico com praça e prédios antigos e bonitos, e banco pra sacar dinheiro e nada mais. me canso fácil. o que fizemos foi tomar um ônibus até o cerro San Javier; o ônibus sobe por uma estradinha sinistra com miles de curvas impossíveis entre florestas e precipícios e altas emoções. descemos na parada do Cristo e de lá dá pra ver toda a cidade. 

(mais ou menos.) resolvemos caminhar pela estrada pra alcançar a cascada do rio Noque, com algumas paradas interessantes pelo caminho; a cascada mesmo não chegava nunca. acabamos pegando carona numa caminhoneta que nos deixou na entrada. deu pra ficar ali bem à toa e pôr os pés na água (gelada).

na volta também uma carona simpática até o Cristo e matar um tempinho pra tomar o ônibus de volta. acabamos descendo com um desses táxis coletivos que cobram o mesmo do ônibus e teoricamente vão mais rápido, mas o infeliz do carro quebrou antes de chegar no centro e tivemos que tomar um ônibus de qualquer jeito. cidade grande cidade grande. não tinha mais muita coisa pra fazer e saltar de parapente (teoricamente é o segundo melhor lugar do mundo pra saltar de parapente) custava muito caro: 750 pesos. também os montes de trekking mais montes de pesos e achamos melhor guardar dinheiro pro resto do caminho. nos restava ir embora. no dia seguinte tomaríamos o ônibus do meio-dia pra Tafí del Valle.

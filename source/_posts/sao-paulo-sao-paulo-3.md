---
title: " São Paulo, São Paulo"
date: 2013-12-26 12:00:04
---

estamos os dois aqui na terrinha desde segunda-feira, dia 23. me parece fácil demais se reacostumar à cidade grande, mas parece também que fica faltando alguma coisa. a roça pelo jeito não vai mais embora. no começo da viagem pensamos que chegaríamos entre novembro e dezembro, mas depois pareceu que a volta só aconteceria em março ou abril. no final das contas aproveitamos a desculpa do natal e ano novo (tudo fica tão mais caro) e voltamos no tempo (mais ou menos) previsto. ficamos em São Paulo para encontrar amigos e aproveitar para recarregar energias até meados de janeiro. quem estiver por aqui, avise!

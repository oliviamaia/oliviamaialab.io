---
title: " são paulo, são paulo"
date: 2011-07-30 23:08:00
---

dormir no aeroporto não é das coisas mais confortáveis. os pés em cima da mala a blusa fazendo de travesseiro o pescoço todo torto. dormir no avião pior ainda. porque retorne o assento da poltrona para a posição vertical e eu estava que não aguentava o peso da cabeça. mas três decolagens e pousos depois, chegamos em são paulo, enfim enfim. meu estômago agradece.

que foi mostrar fotos almoçar um monte ligar o computador mandar fazer as atualizações deitar na cama e capotar. acordei foi só pra dormir mais ainda depois. delicinha água da torneira não-gelada. e muito oxigênio, que beleza. adoro oxigênio, adoro adoro.

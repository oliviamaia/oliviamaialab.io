---
title: " se você fosse a última pessoa na terra"
date: 2015-12-19 10:00:28
---

> Do you work solely for your own pleasure? Or are you chasing the confirmation and likes of others, altering your photos towards what you think people will like? Do you take pictures just because you love the sound of the shutter or do you do it because you can’t wait to hear the notification sound on your phone, saying that someone ”liked” your shot?
> 
> em _[If you were the last person on earth, would you still take a photo?](https://medium.com/@jenslennartsson/if-you-were-the-last-person-on-earth-would-you-still-take-a-photo-7b338b36eaba#.q3qm90wy5)_

a premissa do texto é interessante: se você fosse a última pessoa na Terra, você ainda tiraria uma foto? o desenvolvimento e a conclusão, óbvios: porque importante é fazer a arte que queremos fazer; não faz sentido escrever, fotografar, pintar etc apenas pra ganhar _corações_ no instagram ou no medium. que hoje em dia a internet nos empurra nessa direção graças ao feedback instantâneo etc. estamos todos viciados em coraçõezinhos virtuais. e afinal devemos fazer a arte que amamos e inevitavelmente nos conectaremos com pessoas que estão interessadas nessa arte. fazer o que se ama é o único caminho possível para ganhar dinheiro com o que se ama. mas a resposta à pergunta fica incompleta. se você fosse, de fato, a última pessoa na Terra, você ainda tiraria uma foto? 

![img_3439](/img/posts/img_3439-660x432.jpg)
praia de Japaratinga, Alagoas.

pra quem fotografamos? pra nós mesmos? será? afinal se você fosse a última pessoa na Terra, e não há mais sequer aqueles que possam se interessar pelo que você faz por amor, e não pra agradar ou ganhar _coraçõezinhos_, ainda assim você tiraria a foto? dinheiro obviamente já não seria uma questão. sua arte seria tudo que te resta? quer dizer, ainda é possível fazer o que se ama, e provavelmente muito importante, já que você é a última pessoa na Terra e não há muito mais a fazer senão cuidar da sua sobrevivência e preencher o tempo livre. mas a pergunta continua ali: você ainda tiraria uma foto? você continuaria fazendo arte? a arte é mera expressão necessária do eu? a quem estaria direcionado esse esforço de expressão? por que sentimos essa estranha necessidade de expressar-se? ou a gente, no fim das contas, faz arte, até (e principalmente) a arte que realmente faz sentido para nós mesmos, pra se conectar, de alguma forma, com o outro?

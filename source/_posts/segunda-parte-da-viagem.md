---
title: " segunda parte da viagem"
date: 2014-02-03 11:10:11
---

ontem arrumei minha mochila para seguir rumo a Ouro Preto, dar uma volta pelas cidades históricas de Minas Gerais e trabalhar por mais ou menos um mês (fazendo WWOOF) num retiro espiritual no município de Extrema, também em Minas. 

cortei um pouco o peso diminuindo a quantidade de camisetas, principalmente. eliminei uma coisa ou outra, mas a verdade é que eu já estava economizando bastante na primeira vez que arrumei minha mochila.

o que fez mesmo diferença foram os livros. quando pus a mochila nas costas, nem acreditei. estava muito mais leve. até porque eu consegui pôr ela nas costas sem precisar me apoiar de algum jeito, sem quase cair no chão. vitória. uns livros a menos, um kobo a mais: 

viajar leve é muito mais fácil do que isso, na verdade. eu levo livros (bom, levava livros, agora só levo o kobo) e caderno para desenhar porque isso é importante na minha vida, e nesse momento minha vida é a viagem. levo o computador porque quero escrever, postar, editar as fotos, eventualmente trabalhar (ops). mas uma viagem mais curta não precisa nada disso. dá pra ficar um mês sem computador. aliás, qualquer viagem de mais de duas semanas só precisa de no máximo cinco camisetas, por exemplo. e mesmo isso é relativo: se você for para um lugar não muito úmido, camiseta seca de um dia para o outro. cinco é um número que leva em consideração dificuldades para a roupa secar. e é suficiente. enfim, malas prontas. embarco hoje à noite, e chego em Ouro Preto amanhã de manhã.

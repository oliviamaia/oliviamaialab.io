---
title: " ser adulto"
date: 2013-03-05 00:20:04
---

ser adulto é fazer compras no supermercado e entre a manteiga e o macarrão e papel higiênico e detergente você se dá conta de que pode, se quiser, comprar também dois pacotes de doritos. ser adulto é a possibilidade de dois pacotes de doritos no SEU carrinho de supermercado. ser adulto é não comprar aqueles dois pacotes de doritos porque enfim a manteiga e o macarrão e o papel higiênico e o detergente etc e afinal para que eu vou comprar essa porcaria. (eu ia dormir, mas aí estava aqui deitada e o celular e o tumblr e confesso que ser adulta não é uma das minhas especialidades.) (substituir doritos pelo elemento "mãe, compra?" de sua preferência.)

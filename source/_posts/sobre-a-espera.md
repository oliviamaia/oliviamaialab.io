---
title: " sobre a espera"
date: 2013-09-13 11:11:48
---

porque o que se tem de incrível e fantástico na vida e nas ações (quase sempre alheias) não passa de um momento ou da percepção de um momento, mas é que a gente tão rápido esquece que a vida se faz de muitos momentos, um atrás do outro, sem parar, nem mesmo quando a gente tenta capturar um deles com as lentes de uma câmera e guarda no computador com carinho para voltar a ele em momento posterior. ou porque a vida se faz na maior parte do tempo de espera e a espera a gente não registra, senão com comentários rabugentos ou engraçadinhos no twitter; a espera a gente ocupa. com internet, com joguinhos, com aborrecimento? mas como se pode ocupar a espera? ou então o objetivo mesmo seria minimizar a espera, transformá-la em qualquer outra coisa, usufruí-la como não-espera. que a vida seja esse esforço constante por afugentar a espera? a espera (como o suicídio) é desses momentos que duram muito mais do que permitiu (deveria ter permitido) o tempo.

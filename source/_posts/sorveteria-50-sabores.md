---
title: " tirando a prova: sorveteria 50 sabores"
date: 2013-08-20 11:23:14
---

ontem no final do dia depois de baixar o sol tomamos o ônibus para a Beira Mar mais uma vez. fomos à sorveteria 50 sabores para ver se ganhava da Juarez. a sorveteria fica enfiada no muro do clube Náutico, bem próxima à feirinha de artesanato. você passa pela porta de vidro e AR CONDICIONADO swoosh.

fomos atendido por um rapaz mui simpático que nos apresentou os sabores regionais, os mais pedidos, os seus preferidos, os sabores que ele achava que a gente podia gostar e os sabores que a gente não sabia o que queria dizer. enfim, não chegamos nem perto de experimentar todos os sabores disponíveis.

de fato o sorvete é menos doce do que o sorvete da Juarez, mas também mais cremoso. para alguns isso pode ser uma vantagem, mas para quem tem (um pouco de) intolerância à lactose e para quem tomar sorvete é um ato de rebeldia contra o próprio estômago (eu), sorvete ser cremoso não é necessariamente uma vantagem. por outro lado, eles têm algumas opções de sorvetes (de fruta, principalmente), sem lactose. o sorvete de cajá foi um bom exemplo disso. mas também era mui bom como todo sorvete de cajá que eu já tomei. acabei escolhendo um dos meus favoritos, romeu e julieta (sorvete de QUEIJO e goiabada, melhor invenção do universo), com um de iogurte com morango, sugestão do Zeck, o rapaz que nos atendeu. o Fabio ficou com uma combinação de castanha de caju com cajá.

o sorvete duplo, com dois sabores, custa 13 reais. quase o dobro da Juarez, mas também vem quase o triplo de sorvete. é para morrer tomando sorvete. 

o veredito (mais ou menos): o Fabio achou que o sorvete da 50 sabores é bem melhor do que o da Juarez. eu acho que foi sorvete demais para mim e fiquei com a sensação de que o da Juarez é um pouco mais leve (embora mais doce). talvez tenha sido a combinação com a tapioca que a gente comeu mais tarde. talvez a gente tenha que voltar lá outras vezes para ter certeza.

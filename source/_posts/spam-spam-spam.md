---
title: " spam spam spam"
date: 2012-01-25 23:21:00
---

> It's like I've eaten spam a few times from a few popular brands and in a few serving suggestions, and found I'm not really keen on spam, 'cos it's salty and slimy and looks like something you might find in the alien queen's litter box. But I've found myself in a world that's completely obsessed with spam. People spend their entire lives in pursuit of spam. Every single advert on TV sells their product by placing it alongside spam. Movies have to work in at least one spam scene to reach the broadest audience. People break up and get divorced because they don't exchange enough spam. Soldiers are given time out to go have some spam. Low-risk prisoners are given spam visiting rights. People die for spam. Entire economies have been based around spam. Selling spam is the world's oldest profession. The lack of spam has been linked to mental disorders. The only thing getting teenagers through difficult puberty is the thought of one day getting to have spam of their very own. And when I explain to people that I'm not that into spam they tell me I must be some kind of hopeless cissy girl, or that I just haven't found the right spam yet. It feels like when a theist says "I'll pray for you." Or when a parent of some hideous mewling womb dropping says "You'll understand when you have one of your own." Quite infuriating. It's just tinned meat, guys.

via [fullyramblomatic.com](http://www.fullyramblomatic.com/archive/20080719-20100626.htm)

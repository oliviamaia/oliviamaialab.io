---
title: " storytelling"
date: 2010-03-31 18:39:21
---

tenho lido muitos desses blogs de autores e editores americanos sobre o trabalho do escritor. vez ou outra, para além do óbvio, aparece qualquer texto mais interessante. mas curioso mesmo é justamente a repetição do óbvio, ainda que vez ou outra sustentada por qualquer ensinamento grego.

as fórmulas americanas para o bom _storytelling_ são quase nazistas. não pode exposição nas primeiras páginas, não pode descrever demais, o conflito deve surgir na página tal, os capítulos devem ser assim, o protagonista deve ser assado, primeira pessoa é quase sempre algo ruim.

mais ainda: os leitores americanos leem quase exclusivamente autores americanos. o mercado se autoalimenta. um mercado gigantesco. e como tem gente escrevendo! seguindo fórmulas, escrevendo sobre fórmulas, discutindo as melhores maneiras de aplicar as fórmulas. tem autor saindo pelo ladrão.

e tudo um óbvio. penso, às vezes: oras, vou escrever sobre escrever, também. porque afinal o assunto me interessa. mas para além do óbvio, não há tanto a dizer. e, convenhamos, já há gente o suficiente escrevendo o óbvio.

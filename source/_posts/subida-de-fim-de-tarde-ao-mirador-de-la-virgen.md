---
title: " subida de fim de tarde ao mirador de la vírgen"
date: 2014-11-11 07:48:16
---

não vale um álbum, mas umas fotos simpáticas da subida por mais uma via crucis (já subi tanta via crucis este ano que já devo ter pagado todos os meus pecados e quem sabe uns pecados futuros) até a virgem, que tem a mão enorme agarrando um moleque que deve ser o menino Jesus. (pela manhã caminhei até a ponte do rio Belén, mas o rio está seco e o caminho pro mirante de lá é mais ou menos inexistente.)

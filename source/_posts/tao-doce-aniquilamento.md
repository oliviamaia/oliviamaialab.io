---
title: " tão doce aniquilamento"
date: 2011-03-11 15:31:54
---

![P1050969](/img/posts/p1050969-scaled-1000.jpg)

Porque solidão é coisa: massa viscosa que ocupa e cria vazios, vazio maciço e volumoso que se alarga preenchendo feito líquido os espaços mais ocultos e esquecidos do ser. Que é então única maneira de se manter são preencher esses espaços com coisa outra por tentativa de se espantar o vazio, então trabalho e futebol e literatura e sair com os amigos e comprar toda a coleção de filmes do diretor favorito em edição especial com comentários e cenas cortadas mas nunca a capacidade de espantar de todo o vazio, senão para aqueles poucos que sabem usar a máscara e esquecem que a máscara, que dormem com a máscara e nunca percebem o nó da gravata apertando o pescoço ou se abandonam na loucura sem volta jamais lembrar como soletrar a solidão sequer compreender as palavras no dicionário. Porque não basta preencher com o trabalho e com o futebol e com a literatura e sair com os amigos, porque há aquela hora à noite a cabeça no travesseiro e tudo o que se tem, enfim. Porque para espantar de todo a solidão é necessário _ser_ o trabalho, é necessário _ser_ o futebol e a literatura e o mundo externo perder-se de todo esquecer-se de todo e tão doce aniquilamento.

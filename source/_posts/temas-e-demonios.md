---
title: " temas e demônios"
date: 2014-07-28 12:00:26
---

> Como afirma Vargas Llosa: "El por qué escribe un novelista está visceralmente mezclado com el sobre qué escribe: los demonios de su vida son los temas de su obra." Hay un poema hindú, el _Vijñana Bhairava_, que le gustava mucho a Cortázar, y que en cierta forma le explica: "En el momento en que se perciben dos cosas, tomando conciencia del intervalo entre ellas, hay que hincarse en ese intervalo. Si se eliminan simultáneamente las dos cosas, entonces, en ese intervalo, resplandece la realidad."
> 
> Ignacio Solares, _Imagen de Julio Cortázar_

pergunta aos amigos escritores: quais são os demônios de sua vida? como esses demônios se infiltram na sua obra? (pergunta a sério. proponho post-resposta nos blogs respectivos. alguém encara?)

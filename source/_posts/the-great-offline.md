---
title: the great offline
date: 2021-12-27 10:54:52
---

> True disconnection, like true wilderness, is an empty goal. Whether we have shunned social media or not, the internet does not cease to exist as a driving force in the world, any more than ecological systems cease to shape our lives the minute we reach the end of the forest trail and hop back in the car.

via _real life_ magazine: [the great offline](https://reallifemag.com/the-great-offline/).

sempre me pareceu estranha a ideia de separar vida dentro e fora da internet, como se a internet fosse um universo separado da realidade; como se as pessoas que existem ali não existissem de verdade, ou existissem _menos_ do que as pessoas que conhecemos fora dela. 

como se fosse mais importante tentar interagir com um vizinho com quem você não tem nada em comum do que passar horas conversando na internet com um _amigo virtual_ com quem você compartilha interesses e ideias.

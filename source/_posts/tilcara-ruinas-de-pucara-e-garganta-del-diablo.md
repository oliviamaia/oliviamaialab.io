---
title: " tilcara: ruínas de pucará e garganta del diablo"
date: 2014-10-25 08:00:06
---

a parada em Tilcara foi mais curta do que eu tinha planejado antes mas a verdade é que o tempo deu justo pra passear bastante, passar um dia na cachoeira, visitar ruína e ficar um pouco à toa enquanto lá fora no mundo chovia. porque primeiro dia chegamos (depois de já garantir nossas passagens pra Iruya dali dois dias) cedo e fomos conhecer o Pucará de Tilcara, ruínas pré-inca e pós-inca (incas faziam as casas quadradinhas, aprendi) num pedaço mui estratégico de morro ali no meio do caminho da quebrada de Humahuaca. foi um sítio arqueológico descoberto no tempo em que os arqueologistas eram meio sem noção, aparentemente, e o governo construiu um monumento bizarro em homenagem a eles bem no topo do Pucará; a parada não tem nada a ver com as pobres das ruínas. parte delas foram reconstruídas, mas se esticar o olhar pra além da parte do circuito turístico ainda se vê as linhas de pedras meio devoradas entre _cardones_ e cactos variados. 

e claro que uma linda vista, porque né, ponto estratégico.

o tempo foi fechando e quando estávamos ali pelo jardim botânico de altura (ou seja: cactos) começamos a ouvir trovoadas. quando voltávamos perguntamos a uma moça local se ia chover e não, nada, nunca chove antes de janeiro. começou a chover faltava uns 50 metros pra chegar no hostel. chuvinha, né, mas. acho que eu não via chuva desde Montevidéu. (provavelmente mentira. estou tentando lembrar qual foi a última chuva antes dessa e só consegui pensar em Montevidéu.) Tilcara por exemplo não via chuva desde março. aproveitamos a desculpa pra ficar no hostel, jantar e dormir cedo. no dia seguinte o plano foi a _garganta del diablo_, uma caminhada de teoricamente quatro quilômetros em subida eterna. encaramos, com a companhia do cão do hostel, que depois descobrimos se chamar _Amigo_, mas que eu chamava de Cacá. o cão era tão louco quando aquele de Tafí, o louco de pedra, e saía rolando morro abaixo encrencando com as milhões de pedras que haviam pelo caminho.

fizemos o caminho em menos de uma hora, e chegamos levemente mortas. eram 10 pesos pra entrar na garganta e um folhetinho sem vergonha indicava mais ou menos o caminho. perdemos um tempo tentando descobrir pra onde estaria a cachoeira, já que a garganta em si é um troço feio e cimentado num pedaço estreito da quebrada. decidimos seguir o rio à esquerda (o que passava pela garganta) e saltando pedras chegamos ao nosso destino do dia.

aí passar o dia ali, lendo, tomando sol, metendo os pés na água e contando pedrinhas coloridas. e piquenique. ainda voltamos cedo, sem nenhuma pressa (o tempo ameaçava chuva mais uma vez). demos umas voltas pela cidade. 

(choveu mais uma vez.) aliás, choveu e esfriou. fizemos uma janta à brasileira (ou seja, às oito da noite) sob olhares incrédulos de argentinos e uruguaios, e mesmo a Barbara não estava acreditando que estava jantando àquela hora e ia ficar com fome na hora de dormir. o ônibus pra Iruya no dia seguinte saía às oito da manhãzinha.

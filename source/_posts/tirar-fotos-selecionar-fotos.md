---
title: " tirar fotos, selecionar fotos"
date: 2014-04-24 11:36:39
---

não gosto de selecionar fotos. primeiro porque perco muito tempo e não consigo escolher entre duas fotos quase idênticas com uma variação sutil de expressão, posição, sorrisos — principalmente as fotos com pessoas. adoro fotos com pessoas.

![img_9736](/img/posts/img_9736.jpg)
uma foto de pôr do sol em Serra Negra. não tem nenhuma pessoa, pôr do sol é legal.

mas é que quando as fotos são muitas e sem seleção parecem um pedaço gigantesco de passado e experiências e momentos e a gente vai lá e mete o facão, enxuga, julga: esse momento serve pra compartilhar, esse não, aquele precisa ficar reduzido a uma só imagem, o sol só precisa nascer na foto mais bonita etc etc. quando olho o resultado final fico um pouco triste. as fotos são bonitas, e talvez sejam mesmo as melhores, mas. talvez a começar porque a experiência já fica reduzida a um punhado de fotografias, mesmo antes da seleção. reduzir ainda mais o que já estava reduzido faz parecer que está faltando alguma coisa. e eu gosto de tirar fotos. gosto das fotos como registro, como recordação, como forma de se reviver um bom momento. e é bom poder compartilhar tudo isso, mesmo que nem sempre essas imagens tenham aos outros o mesmo significado que tem pra você.

---
title: " todos os dias"
date: 2016-03-23 14:28:00
---

o mesmo cenário urbano pela janela todos os dias. imutável e cinzento como são os cenários urbanos. a poeira dos automóveis. a fuligem acumulada nas calçadas, na rua; restos de civilização esquecidos entre os paralelepípedos: guimbas de cigarro, chiclete e embalagens plásticas coloricas. o mesmo cenário às vezes um pouco mais sujo do que no dia anterior. o mesmo sapo atropelado no calçamento, cada dia mais cinza, cada dia mais rua, cada dia menos sapo. o urbano como um pintor inexperiente que voraz mistura cores até que reste apenas um mesmo marrom acinzentado e inexpressivo. o cenário que por alguns instantes quase esquecido quando no final do dia as nuvens se colorem com a luz rosada do entardecer. mas outra vez enfim a mesma moldura, as mesmas cores, os mesmos ângulos. que terrível.

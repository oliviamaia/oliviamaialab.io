---
title: " todos os títulos estavam ocupados"
date: 2011-08-17 00:14:00
---

às vezes eu penso que as pessoas não entendem -- na verdade, eu sempre penso que as pessoas não entendem --; essa gente que viaja sempre, que viaja todas as férias pra ver paisagem diferente e pega fila do check-in no aeroporto internacional. mas é que ainda não me vai embora a bolívia, o vento gelado, a água gelada, o ar seco, a poeira. deve ser essa dor de ouvido que começou em sucre e não foi embora até hoje. minha pedrinha do chacaltaya e o espinho de cacto e os pés sentindo a falta da bota que eu usei por quinze dias quase sem tirar e meus joelhos resmungando porque eu dei de correr ladeira abaixo rumo à rodoviária de sorocaba e ia dar duas e meia e eu nem queria esperar mais quinze minutos pelo ônibus das duas e quarenta e cinco.

sorocaba, pois. que tem dessas da CARREIRA pular na sua frente assim loucamente e dizer EI, ESCUTA AÍ. carreira, meu deus, que palavrinha. mas é isso: corretora de redações, editora do site do colégio e professora de redação e gramática noutro colégio. em sorocaba. viação cometa é minha segunda casa, opa opa.

e o livro: aos trancos. que nem sei bem se me agrada aquele começo (página seis ou sete? que eu deveria estar com esse arquivo aberto aqui mas eu acordei às 4h30 da mañana e meus olhos estão dizendo DEOS DO CÉU MENINA VAI DORMIR enquanto meu estômago diz VAI JANTAAAR CADÊ COMIDAAA NHAM*) ----- então, o começo do livro. não sei bem se me agrada esse começo, ainda que num segundo momento tenham me saído mais umas linhas e me pareceu um encaminhamento pertinente (sai de mim, professora de gramática).

*meu cérebro está numas de 'olha, saiu o firefox 6, acho que vou instalar aqui'.

é que tenho (tenho?) alguma fé no processo. ou no narrador. por isso gosto desse narrador: ele é um em quem se pode confiar e o texto não acaba nunca. só preciso é deixar ele sentar aqui na frente do computador comigo e mandar ver. parar de duvidar da capacidade dele nesse começo de livro e deixar que ele assuma o leme. não saber pra onde ele está me levando é parte da alegria de escrever.

mas também minha cabeça (além de interessada em instalar o firefox 6) ficou agora numa confusão danada. o site novo e reunião com diretor vice-diretora professores preparar aula da semana que vem e os alunos já têm prova na terça-feira. fica uma coisa assim um pouco claustrofóbica. e eu preciso um pouco do tempo de deitar no chão com os pés pra cima e um gato na barriga (eu até dispensava o gato na barriga, mas o gato na barriga é inevitável no momento em que se deita no chão) pra fazer vir o narrador e os personagens e ver o que acontece.

mas deve ser essa dor de ouvido que começou em sucre e não foi embora até hoje.

---
title: " traduções"
date: 2010-05-13 15:48:39
---

encrenco sempre com alguns tipos de tradução do inglês pro português, e não sei o que acontece com essa literatura de entretenimento, a tradução sempre me chega aos trancos. por isso acabo preferindo os livros em inglês mesmo. sem contar que costumam ser mais baratos e menores, o que é sempre mais conveniente.

imagino que esse problema de tradução seja qualquer coisa com o próprio inglês, e a proximidade que existe entre a língua escrita e a língua falada, coisa que não acontece no português. talvez por isso qualquer tradução do inglês que se pretenda fiel acaba pecando pela artificialidade.

uma boa tradução de, principalmente, diálogos em inglês exigiria inevitavelmente uma reinvenção do texto, uma adequação à nossa língua para fugir ao tom de filme mal dublado. mas reinventar texto não está sempre na função do tradutor. a fidelidade ao inglês se transforma em artificialidade no português.

mas esse é o tipo de problema que poucos vão notar -- geralmente uns tipos chatos feito eu -- e a maioria toma essa "linguagem de tradução" como normal. ou, ainda: mesmo para aqueles que não conseguem ler sem ouvir um eco do original em inglês no fundo do cérebro, o texto é fácil, a leitura flui com uma velocidade considerável. a fragilidade da tradução -- geralmente bem feitinha -- se dissolve na agilidade da narrativa.

solução é ler no original. toda tradução é imperfeita. e, aparentemente, a tradução que se pretende muito fiel falha justamente no momento em que tem a ambição de se livrar dessas imperfeições.

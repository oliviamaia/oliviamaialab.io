---
title: " travessia pelo salar de antofalla"
date: 2014-11-12 12:00:40
---

gente. acho que não serve muito escrever qualquer coisa. posso dizer que fui até a hospedagem dos franceses de manhã e me meti no carro com um que falava algum inglês: Philippe (a gente chuta como se escreve porque eu não perguntei).

então não vou escrever muito. deixa que as fotos gritem seu silêncio alienígena. na praça seguiram a oeste e eu fiquei tentando adivinhar pra onde estávamos indo porque Philippe não sabia, ele só seguia. até aí na verdade eu não me importava porque meu deus: essa infinidade de espaço vazio; as vicunhas, as lhamas; esses arbustos de altitude que parecem fosforescentes; essa paisagem vulcânica quase extraterrestre; esses _salares_ sem fim; essas montanhas coloridas; essas lagoas coloridas.

depois me dei conta de que aquele caminho era a volta completa do salar de Antofalla, mui indicada pelo meu amigo Seba, passando por entre miles de vulcões adormecidos há milhares de anos, mas que ainda conservam aos seus pés um véu de cinzas e lava petrificada, como as ruínas de um império morto. estar na puna de Catamarca é um pouco sentir-se como um ser em miniatura. tudo está fora de proporção, fora da realidade. tem cenários que parecem uma pintura, um desenho de história em quadrinhos, um filme de ficção científica. e no meio de tudo surge uma casinha de adobe e um monte de lhamas pastando e você se pergunta como é possível alguém viver nesse meio do nada. 

mais ainda porque nesse meio de nada está o povoadinho de Antofalla, que é meia dúzia de casas e está perdido ali ao norte da província, quase em Salta, aos pés do salar. e cercado de outros salares. muito sal por todos os lados. depois a gente sobe a 4600m de altitude e para pra tirar fotos.

e descer em zigue-zague pra alcançar a última quebradinha do dia, onde a turma foi parada por um caminhãozinho da _gendarmería_ e eu fui dar uma de intérprete. eles queriam saber quanto faltava pra chegar em Antofalla e já estavam cansados porque a estrada era muito ruim (chame de estrada se quiser). voltamos pra Antofagasta no começo da tarde. não eram nem três horas e o dia já valia inteiro.

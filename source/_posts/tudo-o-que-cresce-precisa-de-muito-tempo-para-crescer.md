---
title: " tudo o que cresce precisa de muito tempo para crescer"
date: 2011-03-10 13:51:00
---

> E eu sem fazer nada, raios partam, adianto estas crónicas para ter espaço, mas as crónicas são um galope diferente, que me seca a cadência do livro e me atrapalha o ritmo. O segredo de escrever é ser estrábico, ter um olho na bola e outro nos jogadores. Em miúdo espantava-me que os olhos dos lagartos fossem independentes um do outro, mas quando comecei nesta vida descobri-me lagarto numa pedra, à coca, muito quietinho, rodando as pupilas para sítios diferentes, guloso da mosca de uma frase. Descobri também que o passado é a coisa mais imprevisível do mundo, não pára de se transformar.

via [aeiou.visao.pt](http://aeiou.visao.pt/tudo-o-que-cresce-precisa-de-muito-tempo-para-crescer=f593538) da [crônica](http://aeiou.visao.pt/tudo-o-que-cresce-precisa-de-muito-tempo-para-crescer=f593538) do António Lobo Antunes (gênio). vão ler.

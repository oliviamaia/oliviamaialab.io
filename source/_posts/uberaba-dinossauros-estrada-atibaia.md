---
title: " Uberaba, dinossauros, estrada, Atibaia"
date: 2013-12-25 21:03:24
---

no domingo 22, antes de seguir para Atibaia, eu e minha prima fomos até o sítio paleontológico de Peirópolis, onde fica o museu dos dinossauros. não estava pondo muita fé nesses dinos mineiros, mas o museu é bem massa e os dinos também.

custa 3 reais a entrada e você pode visitar o museu e uma sala grande do centro de pesquisas com algumas réplicas de ossadas e outros animais descobertos na região (como a preguiça gigante, nosso querido eremotherium, que já foi encontrado em todos os estados brasileiros, geralmente em cavernas ou áreas que costumavam ser alagadas). começamos pelo museu. são duas salas, uma maior e outra menor, e um corredor com alguns pedaços "menores" (ossos com nomes estranhos do tamanho de um pneu).

caminhamos um pouco pelo jardim para tirar fotos engraçadinhas com os dinos e fomos visitar a outra parte. eu na verdade já estava achando tudo muito massa sem nem saber que tinha outra parte. vai vendo como é a criança (nerd) feliz.

e porque estamos em Minas não fomos embora sem passar numa loja de doces caseiros, comprar queijo, goiabada e um outro troço de leite que minha prima curte. voltamos para a casa dela, almoçamos e carregamos o carro para a viagem até Atibaia: as malas dela, a minha mochila, um aparelho de veterinária gigante, travesseiro, tartaruga de pelúcia, sacola de comidas, dois gatos numa casinha grande, uma pitbull babona e uma salsicha. quando pareceu que não ia sobrar espaço para a pitbull, minha prima fez tudo caber com uma manobra ninja que eu não entendi até agora. os viajantes:

minha prima pôs um pouco de gasolina antes de pegar estrada mas esperou cruzar a divisa para completar o tanque. gasolina em Minas está 3,19 reais em alguns postos! uns quilômetros São Paulo adentro já tinha posto a 2,69. vai vendo que passagem de ônibus vai ficar mais caro. a viagem foi tranquila mas pegamos um enrosco por causa de obras na Anhanguera na altura de Americana. nem estava sentindo falta desse tipo de coisa. chegamos na casa da minha tia em Atibaia antes de escurecer.

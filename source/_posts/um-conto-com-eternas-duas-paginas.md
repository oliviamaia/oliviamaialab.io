---
title: " um conto com eternas duas páginas"
date: 2015-06-28 19:51:30
---

o conto se chama _Mancha de café_ e já tem duas páginas. era um conto que teria talvez 20 ou 40 páginas. mas vou escrevendo parágrafo por dia, ou menos, e ele continua com duas páginas. começo a desconfiar que ele vai terminar com seis páginas. o que não é um problema em si, na verdade, mas também que me ocorreu na última vez que abri o arquivo que o conto não era grande coisa, e que a ideia grandiosa que tive quando comecei a escrever se reduziu a uma banalidade mais ou menos _policialesca_ com um _catch_ final pro leitor fazer _a-ha!_ ou seja: começo a desconfiar que não quero terminar de escrever esse conto. e como a ideia era pelo menos terminar esse conto pra voltar ao ritmo de escrita e então retomar a escrita de um dos romances iniciados, parece que tenho um problema. ou: hora de mudar os planos.

---
title: " um corpo que instável hesita"
date: 2010-12-06 00:13:18
---

> Então o artista de quem não mais de falava e uma morte por excesso de vida. _Afogo-me em minha vida_, ele teria escrito, porque olhava ao redor e vida, seus quadros o encaravam e vida, toda a arte dos colegas e vida, as resenhas positivas nos meios de comunicação e vida, uma criança que chora na rua e vida. _Afogo-me na vida, esse oceano indiferente interminável e meu deus onde encontro terra firme?_, escreveria, inconsequente e ainda tão inocente por jamais prever as interpretações em superfície de psiquiatras televisivos que em seus veleiros flutuam.  

  
final de um dos capítulos mais ou menos no começo quase na metade de A VIDA SECRETA DAS NUVENS. bueno. já falei que gosto muito desse livro? acho que está ficando belezinha.

até a página 23 ainda está aos trancos. vou precisar reescrever o começo. talvez o final. e um capítulo ali pro meio cheio de fotografias.

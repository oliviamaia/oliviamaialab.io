---
title: " uma explicação minimamente sensata"
date: 2009-12-08 04:59:55
---

meu ex-namorado costumava dizer  
          (e eis uma das poucas coisas minimamente interessantes que ele dizia, embora a frase não fosse dele, e sim de algum professor de cursinho)  
                                                     que o grande problema de Portugal foi uma sucessão absurda de péssimos governantes (basta dar uma conferida nos títulos que os infelizes tinham o hábito de receber). a afirmação me parece minimamente sensata, dado todo o potencial que nossos caros colonizadores chegaram a ter em algum momento perdido da história, antes de tudo desandar.

lembrando disso, me resta afirmar que o que desencadeou a tragédia palmeirense (e que nome melhor pra descrever o que aconteceu, uma tragédia clássica, com o destino que foi traçado desde o primeiro momento e nós espectadores estúpidos imaginando ainda que alguém seria capaz de enganar o deus Apólo) nesse maldito campeonato brasileiro foi, também, a sucessão absurda de péssimos comandantes. Luxemburgo e Muricy, do alto de seu brilhantismo, foram incapazes de montar um time com algum tipo de estratégia eficiente. os dois cabeça-dura que era o cão, cada um à sua maneira. o primeiro que insistia numa estratégia que logicamente não funcionava, e o segundo que sequer tinha uma estratégia, e não posso deixar de imaginá-lo no vestiário com um pep-talk bem sem vergonha, batendo no braço e tudo. yeah, right.

o Marcão como LATERAL ESQUERDO é só um dos mais BERRANTES erros que os dois chegaram a cometer. a falta do Pierre e do Cleiton (e do Maurício Ramos, o mergulhador) são casos à parte. a questão é que não posso deixar de pensar que o melhor mesmo era ter deixado o Jorginho trabalhar, que o cara sabia o que estava fazendo, e eu, de fora, temendo pela falta de experiência do homem, tive a sensação que ele fazia os jogadores se entenderem e jogarem do jeito que sabem jogar.

          (porque, convenhamos, time o Palmeiras tinha, ou teve em boa parte dos jogos que perdeu.)

e para me tirar qualquer resto de fé que ainda podia me restar no Muricy, ele conseguiu desmontar o time (manja castelo de cartas, wooosh) no intervalo do jogo mais importante do campeonato (a essa altura do campeonato, etc). o sujeito foi capaz de me tirar o Deyvid Sacconi na metade do jogo, e desmontar a droga do time inteirinho.

sem tirar o mérito dos adversários,  
         (vê que não estou dizendo que ah, o Palmeiras ia ganhar o campeonato e. não é bem isso. a questão é que levou um tombo, um tombo feio, um tombo vergonhoso, um tombo desesperador)  
                                                  envergonhada como estou, como estamos (chora não, Pierre; você, o Danilo e o Marcos foram muitas vezes o time inteiro), essa me parece, também, uma explicação minimamente sensata.

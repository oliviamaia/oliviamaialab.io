---
title: "uma nota sobre «ser nerd»"
date: 2015-05-26 19:13:07
---

essa alegria quando o kobo está finalmente configurado, os livros organizados por estante e por série, do jeito que tinha que ser, depois de quase um dia de transferir arquivo e fazer ajustes.

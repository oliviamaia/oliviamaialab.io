---
title: " uma semana e meia de Fortaleza"
date: 2013-08-18 18:13:20
---

essa primeira parada é muito mais descanso do que qualquer outra coisa: casa do (meu) pai, curtindo minha irmãzinha (embora ela tenha agora nos abandonado para curtir a avó que voltou de viagem), economizando na hospedagem e aproveitando para tomar cerveja e comer camarão, baião de dois e feijão verde. em três dias da semana meu pai dá aula (a Federal está de férias) e nos outros dois se sobrar pique a gente vai de carro até a Praia do Futuro e fica por lá até o sol ir embora. 

e haja sol: hoje amanheceu uma chuva violenta. nove horas já tinha parado, e por volta das dez as nuvens já estavam indo embora. onze horas, quando saímos para a praia, era só o sol. na sexta-feira eu e o Fabio esperamos o sol baixar e pegamos o ônibus Circular 1 (um trânsito do capeta) até o Dragão do Mar, e de lá nos enfiamos na biblioteca estadual. o prédio é bonito, mas por dentro é um esquecimento só. encontrei um livro de obras reunidas do Campos de Carvalho e com ele veio uma camada de pó preto. 

depois com o Circular 2 (enfim sete da noite e não pegamos mais trânsito; o motorista que corria feito fosse sair voando) voltamos e paramos na sorveteria Juarez, na av. Barão de Studart. duas bolas de sorvete sete reais. o sorvete estava mui bom (cajá com chocolate, porque para mim cajá é sorvete e pronto). dizem por aí que é o melhor sorvete da cidade, mas para ter certeza vamos precisar dar um pulo na 50 sabores (tem na Beira Mar) e confirmar. que sacrifício. ontem, sábado, meu pai deu aula de manhã e à tarde fomos à Praia do Futuro. (“que tal a gente fazer uma coisa diferente hoje? vamos em outra barraca.”) quando o sol se escondeu seguimos para o mercado dos peixes, na Beira Mar. meu pai comprou 1 kg de camarão e fomos às barracas de preparo, ali ao lado. devo ter comido metade. 

aí hoje praia de novo. a gente tinha considerado a praia do Cumbuco mas segundo meu pai todas as praias são meio iguais por aqui e até chegar lá ia quase uma hora. então era Praia do Futuro mesmo, noutra barraca que é para variar um pouco. o mar estava nervoso e tomei um monte de caldo das ondas.

saímos umas três da tarde e fomos a um restaurante chamado Docentes & Decentes para comer feijão verde, indicação do cearense-carioca Marcos VP; meu pai já conhecia e aprovou a ideia. rapaz, que comida boa.

falta: encontrar um lugar para comer tapioca (já tenho indicações), sorveteria 50 sabores e fazer as contas antes de ir embora do Ceará para ver se sobrou grana para ir ao Beach Park (porque sim). e continuamos atualizando o álbum de Fortaleza. vai lá ver as fotos novas! que eu vou me enroscar no sofá e ler mais um pouco de Sherlock Holmes.

---
title: " vero, ello e mais do mesmo"
date: 2018-02-27 13:53:50
---

agora todo mundo está irritado com as bizarrices de algoritmo do instagram e facebook, o que me parece bastante coerente; mas daí a correr pra outro aplicativo tão fechado quanto me parece um pouco tiro no pé.

ah, porque esse tal de vero não tem algoritmo (seja lá o que eles querem dizer com isso) e você é dono do seu material etc. mais ou menos. vá ler a [política de privacidade](https://www.vero.co/privacy-policy/) do aplicativo. a ladainha é a mesma. informações são coletadas o tempo todo: além de pedir seu número de telefone, eles coletam informações sobre o que você posta, sobre quem você é e o que você faz na internet. não respeitam pedidos de "do not track". informações são compartilhadas com _terceiros_ e nada impede que a empresa seja vendida, que os planos mudem e que, enfim, eles compartilhem também todas essas informações com quem quer que seja que compre a empresa.

etc etc etc.

![](/img/posts/qeqg7f9pfuu.jpg)

o ello, uma mistura de pinterest e instagram pra artistas, por exemplo, já está por aí há mais tempo do que o tal do vero, e tem uma [política de privacidade um pouquinho menos cretina](https://ello.co/wtf/policies/privacy/). mais ainda é mais do mesmo e, na real, o site é um desastre de usabilidade e o aplicativo pro celular é um pesadelo. é empresa, e por mais que não estejam eles diretamente vendendo seus usuários à empresas de propaganda, ainda assim eles vão coletar informações e, talvez, compartilhar com tudo isso com um possível futuro associado.

não: não é o fim do mundo. facebook e instagram fazem coisas piores, e a gente continua lá. mas também não é nenhuma novidade imperdível.

o que me deixa mei passada é que _já existem_ opções﻿ _sem algoritmo_ e livres que realmente não coletam coisa nenhuma. redes sociais em que, aí sim, seu conteúdo é seu. redes sociais que inclusive não te amarram num único servidor, numa única _empresa_. já imaginou se pra usar e-mail ou ter um blog a gente tivesse que se cadastrar com uma única empresa? pois é. mas a gente não acha estranho quando precisa estar cadastrado numa só empresa pra poder entrar em contato com os amigos e a família ou mesmo pra usar um sistema de microblogging e ficar escrevendo bobagem o dia todo.

opções abertas e livres como [diaspora*](https://diasporafoundation.org/) e [friendica](http://friendi.ca/), por exemplo, já estão por aí faz tempo. tem também o [mastodon](https://joinmastodon.org/), que é como o twitter, mas livre e descentralizado. todos eles funcionam de uma maneira parecida: você escolhe um servidor pra criar uma conta (ou monta o seu próprio servidor), mas pode se conectar com pessoas de qualquer outro servidor. é como com o e-mail: você pode ter uma conta no gmail e mandar mensagem pra alguém que tenha conta no yahoo. nada de estranho nisso.

![](/img/posts/haipj8pyel8.jpg)

a questão, logicamente, é que uma REDE SOCIAL só serve se você tem com quem se conectar. por isso a gente continua no facebook e no twitter e no instagram. mas quando um monte de gente começa de repente a dizer que vai migrar pra outro lugar, eu me pergunto por que a gente não pode todo mundo migrar pra uma rede melhor, com mais controle, mais segurança e mais liberdade?

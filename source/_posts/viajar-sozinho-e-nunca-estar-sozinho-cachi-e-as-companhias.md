---
title: " viajar sozinho é nunca estar sozinho: Cachi e as companhias"
date: 2014-10-31 20:18:31
---

mentira, claro, mas poderia não ser. ou: quem procura acha; e quem não procura às vezes acha também. vê: segundo dia em Cachi no café da manhã comentei com um rapaz do hostal que se quisesse podia usar a mesma mesa que eu porque eu tinha limpado e a outra estava meio empoeirada e aí a gente já estava conversando e ele ia passar o dia no camping El Agarrobal, a uns 16 km de Cachi, e ia pedindo carona e se eu quisesse podia ir junto. que era o tipo de lugar muito longe que se não fosse com companhia eu nem ia mesmo, então _ya está_. porque caminhar sem se preocupar com tempo distância e ter qualquer companhia pra conversar enquanto se espera que algum carro pare e nos leve um pouco mais perto do nosso destino final. depois ainda errar caminho e deixar-se levar um monte de quilômetros adiante do planejado, ter que voltar caminhando, conseguir outra carona e com essa última chegar aos pés do camping.

![tarde no rio.](/img/posts/img_2880.jpg)

confesso que cheguei em Cachi pensando que ufa, vou aproveitar pra ficar um pouco sozinha excesso de interação etc. mas topei com esse rapaz e depois com outra menina de Buenos Aires e mais ainda um casal mais velho também de Buenos Aires e a companhia toda tinha algo de tranquilizante. fiquei cinco noites em Cachi no hostel/casa de família chamado La Mamama, pagando 50 pesos por noite, sem internet senão a da árvore do outro lado da rua porque o 3g do celular funcionava mais ou menos em uma janela de tempo entre as duas e as quatro da manhã, e às vezes um pouco às sete, e depois nunca mais. 

![pedaço de Cachi](/img/posts/img_2904.jpg)

com a companhia da Mariela e do casal, Néstor e Ana (o primeiro com quem fui até o Algorrobal foi embora logo no dia seguinte), fiz a visita a Las Pailas, um sítio arqueológico a alguns quilômetros dali aos pés do Nevado de Cachi (ou nevados, porque é um pedaço de pré-cordilheira cheio de picos que alcançam cinco ou seis mil metros de altura.

![las pailas](/img/posts/img_2908.jpg)

![e as companhias](/img/posts/img_2930.jpg)

subimos uma noite até o mirador do cemitério pra ver as estrelas junto do Oso, o cachorro mais simpático de Cachi. até noite de teatro teve, com um grupo de Salta e com direito ao Oso saltando no palco todas as vezes que os atores se exaltavam por alguma razão. meus dias em Cachi foram mui tranquilos e um pouco por querer estar à toa encontrei um equilíbrio entre viajar sozinha e ter companhia; aproveitar a companhia e deixar-se ficar em silêncio. estava lendo _The Tao of Travel_, do Paul Theroux, e me ocorreu uma coisa importante: estou viajando pra ESTAR. não tanto por recorrer e ver novas paisagens e conhecer e tirar fotos, embora também claro por tudo isso. mas o principal tem que ser estar presente, não importa onde. (com ou sem internet etc.) então Cachi me serviu muito bem pra isso.

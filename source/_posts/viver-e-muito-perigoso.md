---
title: " viver é muito perigoso"
date: 2009-06-25 21:56:31
---

mestre:

> Viver é muito perigoso... Querer o bem com demais força, de incerto jeito, pode já estar sendo se querendo o mal, por principiar.

Guimarães Rosa, _Grande sertão: veredas_

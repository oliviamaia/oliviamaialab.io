---
title: inktober 2020
layout: inktober
inks:
  - ano: 2020
    tema: "humanos"
    desc: "exercícios pra treinar movimento, postura, equilíbrio etc."
    links:
    - /img/rabiscos/ink/2020/ink20-01
    - /img/rabiscos/ink/2020/ink20-02
    - /img/rabiscos/ink/2020/ink20-03
    - /img/rabiscos/ink/2020/ink20-04
    - /img/rabiscos/ink/2020/ink20-05
    - /img/rabiscos/ink/2020/ink20-06
    - /img/rabiscos/ink/2020/ink20-07
    - /img/rabiscos/ink/2020/ink20-08
    - /img/rabiscos/ink/2020/ink20-12
    - /img/rabiscos/ink/2020/ink20-13
    - /img/rabiscos/ink/2020/ink20-14
    - /img/rabiscos/ink/2020/ink20-16
    - /img/rabiscos/ink/2020/ink20-17
    - /img/rabiscos/ink/2020/ink20-18
    - /img/rabiscos/ink/2020/ink20-19
    - /img/rabiscos/ink/2020/ink20-20
    - /img/rabiscos/ink/2020/ink20-22
    - /img/rabiscos/ink/2020/ink20-26
    - /img/rabiscos/ink/2020/ink20-27
    - /img/rabiscos/ink/2020/ink20-31
---


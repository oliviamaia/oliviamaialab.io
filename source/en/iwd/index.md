---
layout: 'en'
category: 'eng'
---

# let's talk about flowers

another one of these international women's day and maybe I'm getting better at hiding from the real world, but these last few years I've actually encountered some not not so dumb opinions about this day: men talking about their own privileges and lists of powerful women that did this or that, or maybe a list of inspiring people, made by a programmer man, and everyone in the list were women.

i wish i could say we were learning, and not only that the market realized feminism can also make money and generate clicks. either way, messages of "happy women's day!" still show up in whatsapp, and i do my best to ignore them.

what i wanted to write about is actually something else. you could say i wanted to write about flowers*. because, they say, flowers are girlish stuff. no one likes girlish stuff.

we are taught girlish things are always bad. unimportant. futile.

which is probably why everywhere we take the opportunity of women's day to remind everyone of STRONG fearless women that conquered spaces, beat prejudices, got to places of power or uncovered some mystery of science. we say: look, gentlemen, women also can do this kind of thing. while taking care of the kids.

and while all this is certainly noble and important, something still bothers me: women, in order to *be someone*, have to BE LIKE MEN. or, better yet: have to occupy positions of respect inside this masculine universe that is society as we know it.

no need to go too far: when girls are kids and run around shoeless, get dirty and play soccer, people think that's cute. they say it's a victory against gender stereotypes. but when the boy is interested in girlish stuff the reaction is different, and the modern dad, the same one that teaches his daughter to fly a kite and climb trees, this dad can't understand why the boy is so damn sensitive and cries all the time, and is disgusted by worms and scared of birds.

or maybe: when a woman dresses in a masculine manner, she's defying prejudices and showing strength. when a man wears feminine clothes, he's probably gay, trans, cross-dresser (and I'm not saying it's bad if they are, but they aren't really allowed to be anything else). i mean: men's clothes are unisex, right?

these things are not new. they are the consequences of a sexist world etc etc. what bothers me is our response to all that.

because we decided equality means women getting their place in the market, in the work space, in power. market, work, power. the world made by men. the world of strength, of focus, of productivity.

what i really wanted was a space that wasn't the masculine one. a world in which it isn't necessary to be LIKE A MAN to have respect; in which i don't need to be a leader or a scientist or an activist; in which women can just be women, dressmakers, mothers, actresses, hairdressers, teachers, nurses -- and that none of that means LESS than the rest.

who decided fashion is futile while sports is important? who decided worrying about your nails is frivolous and worrying about your car's paint is a healthy hobby?

enough of this obligation to be strong; enough with the obligation to feel proud for being strong.

why does strength has to be the parameter or value? why is it that these characteristics associated with the masculine world are always the parameter of value?

why does the woman need to be strong to be valued?

why does the man needs to be strong to be valued? and to be honest I'm not even sure we can talk about masculine and feminine if not as a social construct; I'm suspicious of people that say men and women are the same, but also suspicious of those that are certain they are completely different. but we run into a mistake when we insist women can do everything man does, yes sir. not because they can't: they can, and do. the mistake is in thinking that only in doing so women can have any value: by occupying this space dominated by men in science and arts and politics and whatnot; these spaces that win awards and money and magazine covers. in thinking that women have to be important in a world made by men, this world in which men decided what it means to be important.

maybe discrimination isn't that most nurses are women and most doctors are men, but that being a doctor is considered MORE and BETTER than being a nurse (why, because of the amount of time they studied? who decided that's the most important thing?)

maybe gender inequality isn't so much that there aren't as much women CEO or state leaders, but the simple fact that being a CEO and a state leader is considered some kind of peak of success personally and collectively.

let's face it: the world that determined this measure of value is the masculine world. the world of work is the world of men. the world of power is the world of men.

so corporate world takes this opportunity to sell more hiking boots, tactical knives and video-games to *strong* women (while, of course, they are still making money by selling clothing and self-help books to the *weak* ones).

*revolution* cannot be switching the old power by new power. we already know this does not work. the problem is not that men occupy spaces of power and importance, but in the mere existence of this spaces in the way they are configured today.

gender equality, yes, of course. if we get rid of the idea of power, and the idea that some people are *better* and *stronger*. otherwise, it just means a couple more women will have the opportunity to step on weaker people, while wearing suits.



\* the title made more sense in Portuguese, because it references to a Brazilian song called "Pra não dizer que não falei das flores" (or something like, _So you don't say I didn't talk about the flowers_), a song of resistance against the Brazilian military dictatorship.{.small}

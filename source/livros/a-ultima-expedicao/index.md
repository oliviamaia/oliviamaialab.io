---
title: a última expedição
id_compra_impresso: A ULTIMA EXPEDICAO - impresso
layout: livro
photos:
  - /img/capas_livros/ultima.jpg
páginas: 224
editora: Editora Draco
isbn: 978-85-8243-013-2
formato: 14 x 21
peso: 235g
data: abr/2013
infoextra: A última expedição foi um dos projetos selecionados pelo Programa Petrobras Cultural de 2010. A produção tem patrocínio da Petrobras por meio da Lei de Incentivo à Cultura, do Ministério da Cultura.
goodreads: https://www.goodreads.com/book/show/22294376-a-ltima-expedi-o
preco_impresso: 'R$ 29.90'
---

## sinopse

Seis meses depois do fracasso de uma expedição à Antártica, Estevão não sabe por onde começar a resolver sua vida: a noiva o abandonou, o pai não fala com ele e o emprego improvisado não serve para pagar o condomínio do apartamento e as dívidas que se acumulam no banco. Uma expedição de busca na Bolívia não era bem sua ideia de solução, mas a proposta vem com a promessa de um bom adiantamento em dinheiro e já vai o tempo para a monotonia da cidade grande começar a enlouquecê-lo.

Talvez não fosse expedição para se levar a sério: cinco pessoas contratadas para procurar um médico de reputação decadente que desapareceu no altiplano boliviano enquanto pesquisava o potencial anestésico de uns cactos mutantes. Mas a inquietação de seis meses de inércia, a misteriosa argentina que desde o início parece segui-los, as informações desencontradas que surgem Bolívia adentro. Estevão descobrirá que há muito que não lhe contaram, e talvez seja um dos poucos interessados em encontrar esse médico com vida.

## como comprar

Você pode comprar seu exemplar assinado aqui, diretamente comigo. Se preferir, pode também adquirir o livro com [a própria editora](https://editoradraco.com/produto/a-ultima-expedicao-olivia-maia/), na [Livraria Cultura](http://www.livrariacultura.com.br/scripts/resenha/resenha.asp?nitem=42134115&sid=0164114681561864350291011), na [Travessa](http://www.travessa.com.br/A_ULTIMA_EXPEDICAO/artigo/73cc66aa-66d3-4218-8246-64b45e0aa6b5) e na [Amazon](https://www.amazon.com.br/ltima-Expedio-Olvia-Maia/dp/8582430132/ref=tmm_pap_swatch_0?_encoding=UTF8&qid=&sr=). O e-book está disponível nas lojas [Apple](https://itunes.apple.com/br/book/a-ultima-expedicao/id691121081), [Amazon](http://www.amazon.com.br/dp/B00EO30ZNG/) e [Kobo](http://ptbr.kobobooks.com/ebook/A-ltima-expedio/book-jMVGd1r-GkSyG3JzXXyewA/page1.html?s=_WjioSDHd0ihVmbxLCB2tA&r=1).

## prévia

Leia as primeiras 50 páginas do livro [no site da Editora Draco](https://editoradraco.com/produto/a-ultima-expedicao-olivia-maia/).

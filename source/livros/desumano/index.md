---
title: desumano
layout: livro
photos:
  - /img/capas_livros/desumano_capa.png
páginas: 134
editora: Edição da autora
data: dez/2006
goodreads: https://www.goodreads.com/book/show/21393991-desumano
nameyourprice: true
id_compra_digital: DESUMANO - ebook
link_download: https://dl.oliviamaia.net/desumano/
---

## sinopse

Desumano é uma novela policial na qual não há troca de tiros ou esquemas de corrupção entre investigadores e criminosos. Mas há a polícia – e ela está perseguindo um rapaz suspeito de assassinar a própria mãe.

Enquanto Márcio vagueia pelas ruas anônimas de São Paulo, empreende uma busca insana pela própria lucidez – para então descobrir o que é real e o que é imaginário em sua mente.

## comprar

Desumano foi originalmente publicado pela editora Brasiliense, mas deixou de ser comercializado por ela em 2009. Sua disponibilidade em livrarias depende exclusivamente de estoque: [busque a edição impressa](http://compare.buscape.com.br/prod_unico?idu=1851100096&pos=2&site_origem=1297061).

A edição digital (epub e PDF) está disponível aqui para download e você paga o quanto quiser.

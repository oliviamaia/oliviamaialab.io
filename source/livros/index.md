---
title: livros
---

clique nas capas para saber mais sobre cada livro e ver as opções para adquirir o seu exemplar.

# romances

- [![trégua](/img/capas_livros/tregua.jpg =150x)](/livros/tregua)
- [![a última expedição](/img/capas_livros/ultima.jpg =150x)](/livros/a-ultima-expedicao)
- [![segunda mão](/img/capas_livros/segmao.png =150x)](/livros/segunda-mao)
- [![operação p-2](/img/capas_livros/op2.png =150x)](/livros/operacao-p2)
- [![desumano](/img/capas_livros/desumano_capa.png =150x)](/livros/desumano)
{.livros-lista}

# contos

- [![aposta](/img/capas_livros/capa-aposta.jpg =150x)](/livros/aposta)
- [![epidemia](/img/capas_livros/capa-epidemia.jpg =150x)](/livros/epidemia)
- [![trajetória](/img/capas_livros/trajetoria.jpg =150x)](/livros/trajetoria)
- [![jamais o inexistente sorriso](/img/capas_livros/jamais.png =150x)](/livros/jamais-o-inexistente-sorriso)
- [![a casa no morro](/img/capas_livros/casanomorro.png =150x)](/livros/casa-no-morro)
{.livros-lista}

# participações em coletâneas

- [![sherlock holmes: o jogo continua](/img/capas_livros/sherlock.jpg =150x)](/livros/sherlock)
- [![são paulo noir](/img/capas_livros/spnoir.jpg =150x)](/livros/sp-noir)
{.livros-lista}

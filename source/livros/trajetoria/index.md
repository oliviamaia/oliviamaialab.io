---
title: trajetória
layout: livro
photos:
  - /img/capas_livros/trajetoria.jpg
páginas: 13
editora: Editora Draco
data: abr/2016
goodreads: https://www.goodreads.com/book/show/30461383-futebol---trajet-ria
infoextra: O conto Trajetória faz parte da antologia "Futebol - Histórias fantásticas de glória, paixão e vitórias", da Editora Draco.
---

## sinopse

Uma jogadora de futsal retorna à vila onde cresceu para descansar após um período frustrante em sua carreira. Durante uma pelada com os amigos, alguém chuta a bola longe demais. Tão longe que pode nem estar mais neste mundo.

## comprar

- [livraria cultura](http://www.livrariacultura.com.br/p/futebol-trajetoria-100889608)
- [e-book kindle](https://www.amazon.com.br/Futebol-Trajetria-Contos-do-Drago-ebook/dp/B01DUPKEP2/)
- [e-book kobo](https://store.kobobooks.com/pt-br/ebook/futebol-trajetoria)
- [google livros](https://play.google.com/store/books/details/Gerson_Lodi_Ribeiro_Futebol_Trajetria?id=lbLkCwAAQBAJ)

---
title: /src
---

## informações de produção aos curiosos

### site

este site é feito com [hexo](https://hexo.io), um gerador de sites estáticos em javascript, com [código fonte](https://codeberg.org/olivia/site) hospedado no [codeberg](https://codeberg.org/) e publicado via [netlify](https://app.netlify.com/).

o tema foi construído com base no tema [cactus](https://github.com/probberechts/hexo-theme-cactus) e modificado por mim.

### software

- linux + manjaro + bspwm // sistema operacional + distro + window manager <3
- novelwriter // editor de texto tipo markdown para escrever e organizar ficção
- micro // editor de texto de linha de comando
- scribus // diagramação (zines)
- sigil // criação de epub
- gimp // edição de imagens e preparação para impressão
- krita // pintura digital <3
- firefox // navegador web
- obs + ffmpeg // streaming + gravação de vídeos
- kdenlive // edição de vídeos
- alacritty + fish // emulador de terminal + shell

### hardware

- nanquim (talens)
- canetas técnicas (staedler, sakura, tombow, unipin)
- aquarela (winsor & newton etc)
- Wacom Cintiq 13HD <3
- Canon EOS 700D
- Canon SX280
- Ulefone Armor X5 Pro
- Dremel 4000

### máquinas

- desktop
  - CPU Intel i5-10400
  - GPU AMD Radeon RX 570 4GB
  - RAM 16GB DDR4
  - Monitor Dell 23.8" 2560x1440
  - Teclado Redragon Daksa K576
  - Mouse Redragon Cerberus M703
  - Webcam totalmente genérica chinesa pseudo 2K
  - Microfone Blue Snowball iCE

- laptop
  - Dell Inspiron 7460 14" 1920x1080
  - CPU Intel Core i5-7200U
  - GPU Intel HD Graphics 620 / NVIDIA GeForce 940MX
  - RAM 16GB DDR4

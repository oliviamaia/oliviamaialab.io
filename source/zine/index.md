---
title: zines
layout: zine
zines:
  - edicao: "rabiscologia #15"
    tema: Deixe tudo virar mato
    capa: /img/capas_zines/zine15.jpg
    páginas: 16
    formato: "A7 (minizine)"
    filename: "rabiscologia_015"
    preco: 'R$ 10.00'
  - edicao: "rabiscologia #14"
    tema: Palavras x coisas – Vol. II
    capa: /img/capas_zines/capa14.png
    páginas: 16
    formato: A6
    filename: "rabiscologia_014"
    indisp: true
  - edicao: "rabiscologia #13"
    tema: Palavras x coisas – Vol. I
    capa: /img/capas_zines/capa13.png
    páginas: 16
    formato: A6
    filename: "rabiscologia_013"
    indisp: true
  - edicao: "rabiscologia #11 e #12"
    tema: Sobre surtos empáticos (edição dupla)
    capa: /img/capas_zines/capacontracapa2_resize-min.png
    páginas: 24
    formato: A6
    filename: "rabiscologia_011-12"
    indisp: true
  - edicao: "rabiscologia #10"
    tema: Como começam os surtos empáticos
    capa: /img/capas_zines/capadi_resize-min.png
    páginas: 16
    formato: A6
    filename: "rabiscologia_010"
    indisp: true
  - edicao: "rabiscologia #9"
    tema: Folhas do meu quintal
    capa: /img/capas_zines/capacontracapa-09_resize-min.png
    páginas: 16
    formato: A6
    filename: "rabiscologia_009"
    preco: 'R$ 14.00'
  - edicao: "rabiscologia #8"
    tema: As palavras estão sempre em silêncio
    capa: /img/capas_zines/capaes_resize-min.png
    páginas: 16
    formato: A6
    filename: "rabiscologia_008"
    preco: 'R$ 14.00'
  - edicao: "rabiscologia #7"
    tema: Mergulhos involuntários
    capa: /img/capas_zines/capacontracapa-7_resize-min.png
    páginas: 16
    formato: A6
    filename: "rabiscologia_007"
    preco: 'R$ 14.00'
  - edicao: "rabiscologia #6"
    tema: Registro de processo e teimosias
    capa: /img/capas_zines/capa-di_resize.png
    páginas: 16
    formato: A6
    filename: "rabiscologia_006"
    preco: 'R$ 14.00'
  - edicao: "rabiscologia #5"
    tema: Diário de um livro inexistente
    capa: /img/capas_zines/capa-di5_resize-min.png
    páginas: 16
    formato: A6
    filename: "rabiscologia_005"
    preco: 'R$ 14.00'
  - edicao: "rabiscologia #4"
    tema: Humanidade em pedaços
    capa: /img/capas_zines/capa-di4_resize-min.png
    páginas: 16
    formato: A6
    filename: "rabiscologia_004"
    preco: 'R$ 14.00'
  - edicao: "rabiscologia #3"
    tema: Breve compêndio de monstros do ano novo
    capa: /img/capas_zines/capa-dir_resize.png
    páginas: 16
    formato: A6
    filename: "rabiscologia_003"
    preco: 'R$ 14.00'
  - edicao: "rabiscologia #2"
    tema: Tentativa de esgotamento de uma lenda grega
    capa: /img/capas_zines/capadir_resize-min.png
    páginas: 16
    formato: A6
    filename: "rabiscologia_002"
    preco: 'R$ 14.00'
  - edicao: "rabiscologia #1"
    tema: Sobre os princípios rabiscológicos
    capa: /img/capas_zines/capa_resize_resize-min.png
    páginas: 16
    formato: A6
    filename: "rabiscologia_001"
    preco: 'R$ 14.00'
  - edicao: "rabiscologia #0"
    tema: Especial INKTOBER 2017
    capa: /img/capas_zines/zine0.jpg
    páginas: 16
    formato: A6
    filename: "rabiscologia_000"
    indisp: true
  - edicao: "trégua: notas sobre a escrita"
    tema: Zine especial ilustrado sobre a escrita do romance Trégua.
    capa: /img/capas_zines/capatregua.png
    páginas: 20
    formato: A6
    filename: "tregua"
    preco: 'R$ 18.00'
---

o zine *RABISCOLOGIA* é uma revistinha de criações e invenções: uma pequena coleção de ilustrações, fotografias, colagens, textos, pedaços de ideias e às vezes um espaço pra você participar com seus próprios rabiscos.

rabiscologia é arte, é literatura, é rabisco: um zine com o que eu tenho de mais sincero pra oferecer aos amigos e leitores, como escritora e artista.

![pilha de zines](/img/zines-stack.jpg)

você pode fazer o **download gratuito** das edições em formato PDF. adições a esta página serão sempre anunciadas no [blog](/blog) ([assine o feed](/atom.xml)).
